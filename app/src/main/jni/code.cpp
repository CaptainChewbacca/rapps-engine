

#include <jni.h>
#include <android/log.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>

#define  LOG_TAG    "libgl2jni"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)




#include "Particles.h"
#include "engine/sound/MainSound.h"

Particles* Part;
MainSound* MS;

extern "C" {
    //JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNILib_init(JNIEnv * env, jobject obj,  jint width, jint height);


//Paticle
JNIEXPORT void JNICALL Java_com_engine_JNImanager_Init(JNIEnv * env, jobject obj, jint idp, jint idc);
JNIEXPORT int JNICALL Java_com_engine_JNImanager_AddParticleSprite(JNIEnv * env, jobject obj, jint m, jfloat t);
JNIEXPORT void JNICALL Java_com_engine_JNImanager_DeleteParticleSprite(JNIEnv * env, jobject obj, jint id);
JNIEXPORT void JNICALL Java_com_engine_JNImanager_Update(JNIEnv * env, jobject obj, jfloat time, jfloat dt, jint id);
JNIEXPORT float JNICALL Java_com_engine_JNImanager_getDebug(JNIEnv * env, jobject obj);


JNIEXPORT void JNICALL Java_com_engine_JNImanager_InitSound(JNIEnv * env, jobject obj);
};



//Paticle
JNIEXPORT void JNICALL Java_com_engine_JNImanager_Init(JNIEnv * env, jobject obj, jint idp, jint idc)
{
	Part = new Particles(idp, idc);
}

JNIEXPORT int JNICALL Java_com_engine_JNImanager_AddParticleSprite(JNIEnv * env, jobject obj, jint m, jfloat t)
{
	return Part->AddParticleSprite(m, t);
}

JNIEXPORT void JNICALL Java_com_engine_JNImanager_DeleteParticleSprite(JNIEnv * env, jobject obj, jint id)
{
	Part->DeleteParticleSprite(id);
}


JNIEXPORT void JNICALL Java_com_engine_JNImanager_Update(JNIEnv * env, jobject obj, jfloat time, jfloat dt, jint id)
{
	Part->Update(time, dt, id);
}

JNIEXPORT float JNICALL Java_com_engine_JNImanager_getDebug(JNIEnv * env, jobject obj){
	return Part->DEBUG;
}




JNIEXPORT void JNICALL Java_com_engine_JNImanager_InitSound(JNIEnv * env, jobject obj){
    MS = new MainSound();
}
