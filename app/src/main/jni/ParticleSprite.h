#ifndef PARTICLESPRITE_H
#define PARTICLESPRITE_H

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <math.h>



long RandomIteration = 1;

static float Rand(){
		long x = RandomIteration;
		++RandomIteration;
	    x = (x<<13) ^ x;
	    return ( 1.0f - ( (x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f);
}

static float Rand1(){
		long x = RandomIteration;
		++RandomIteration;
	    x = (x<<13) ^ x;
	    return 0.5f*( 2.0f - ( (x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f);
}


struct VecP2{
	float X, Y;
	VecP2(){
		X=0;Y=0;
	}
	VecP2(float x, float y){
		X=x;Y=y;
	}

	void set(float x, float y){
		X=x;Y=y;
	}
};

struct Particle{
	VecP2 Pos;

	VecP2 AngM;
	VecP2 Ang;
	bool Active;

	float Vel;
	VecP2 VelM;
	float Size;
	VecP2 SizeM;

	float r,g,b,t;
	VecP2 rM,gM,bM,tM;

	float LUpdate;
	float Tstart;
	float Tlife;

	Particle(){
		Active = false;
		Pos = VecP2();
		Ang = VecP2();

		AngM = VecP2();
		VelM = VecP2();
		SizeM = VecP2();

		rM = VecP2();
		gM = VecP2();
		bM = VecP2();
		tM = VecP2();

		LUpdate = -1;
		Tstart = 0;
		Tlife = 1.5;
	}




};


struct LineGraph{
	float Min, Min1, Min2, Max, Max1, Max2;
	int N;

	VecP2* Value;


	LineGraph(){
		N = 0;
		Min1 = 0; Max1 = 0;
		Min2 = 0; Max2 = 0;
		Value = 0;
	}

	LineGraph(float min, float max, int n, VecP2 *v){
		N = n;
		Min1 = min; Max1 = max;
		Min2 = min; Max2 = max;
		Value = new VecP2[n];
		for(int i = 0; i<n; ++i)
		 Value[i] = v[i];
	}

	LineGraph(float min1, float min2, float max1, float max2, int n, VecP2 *v){
		N = n;
		Min1 = min1;
		Min2 = min2;
		Max1 = max1;
		Max2 = max2;

		Min = min1+(min2-min1)*Rand1(); Max = max1+(max2-max1)*Rand1();
		Value = new VecP2[n];
		for(int i = 0; i<n; ++i)
		 Value[i] = v[i];
	}

	void NewMinMax(){
		Min = Min1+(Min2-Min1)*Rand1();
		Max = Max1+(Max2-Max1)*Rand1();
	}

	float getValue(float x){
		NewMinMax();

		float yout = 0;

		for(int i = 0; i<N; ++i){
			if(Value[i].X<=x){
				if(i+1 != N){
					if(Value[i+1].X>=x){
						float xn = x-Value[i].X;
						float xo = Value[i+1].X-Value[i].X;
						xn/=xo;
						yout = Min+(Max-Min)*(Value[i].Y+(Value[i+1].Y-Value[i].Y)*xn);
					}
				}else{
					yout = Min+(Max-Min)*Value[i].Y;
				}
			}
		}

		return yout;
	}

	float getValue(float x, float min, float max){
		Min = min;
		Max = max;

		float yout = 0;

		for(int i = 0; i<N; ++i){
			if(Value[i].X<=x){
				if(i+1 != N){
					if(Value[i+1].X>=x){
						float xn = x-Value[i].X;
						float xo = Value[i+1].X-Value[i].X;
						xn/=xo;
						yout = Min+(Max-Min)*(Value[i].Y+(Value[i+1].Y-Value[i].Y)*xn);
					}
				}else{
					yout = Min+(Max-Min)*Value[i].Y;
				}
			}
		}

		return yout;
	}

};




class ParticleSprite {

public:
    float X;
    float Y;
    float AOffset;
    float Angle;
    float Duration;

    int IDposition;
    int IDcolor;

    LineGraph Emission;
    LineGraph ParticleLife;
    LineGraph ParticleVel;
    LineGraph ParticleSize;
    LineGraph ParticleAngOffset;


    LineGraph ParticleCT;


    float NowDuration;
    float StartDuration;
    float NowMax;
    float OldEmissionP;
    float Direction;
    float Ax, Ay;
    int Max;

    Particle* P;


    float OffsetZ;
    //GLfloat* Position;
    //GLfloat* Color;

    GLfloat* PositionV;
    GLfloat* ColorV;

    float Width;
    float Height;

    float DEBUG;


    ParticleSprite(int idp, int idc, int m, float t)
	{
    	IDposition = idp;
    	IDcolor = idc;


        X = 0;
        Y = 0;
        AOffset = 45;
        Angle = 0;
        Duration = 3000;

        Ax = 1; Ay = 0;
        NowDuration = 0;//Duration;
        StartDuration = 0;
        NowMax = 0;
        OldEmissionP = 0;
        Direction = 0;
        Max = 10000;


        float OffsetZ = 0;

        //Max = m;
        P = new Particle[Max];
        for(int i = 0; i<Max; ++i){
        	P[i] = Particle();
        	//P[i].Tstart = t+i*Duration/(float)(1000*Max);
        	//P[i].Tend=1+i*(1f/m);;
			P[i].Active = true;
			ParticleReset(i, t+i*Duration/(float)(1000*Max));
        }



        VecP2* Em = new VecP2[1];
        Em[0] = VecP2(0,1.0);
        Emission = LineGraph(0,10000, 1, Em);



        VecP2* PL = new VecP2[1];
        PL[0] = VecP2(0,1.0f);
        ParticleLife = LineGraph(0,0,500,1000, 1, PL);



        VecP2* PV = new VecP2[2];
        PV[0] = VecP2(0,1.0f);
        PV[1] = VecP2(1,0.0f);
        ParticleVel = LineGraph(0,0,2,20, 2, PV);

        VecP2* PS = new VecP2[2];
        PS[0] = VecP2(0,1.0f);
        PS[1] = VecP2(1,0.2f);
        ParticleSize = LineGraph(0,0,1,2, 2, PS);



        VecP2* PAO = new VecP2[2];
        PAO[0] = VecP2(0,1.0f);
        PAO[1] = VecP2(0.5f,0.0f);
        ParticleAngOffset = LineGraph(0,0,-45,45, 2, PAO);



        VecP2* PCT = new VecP2[4];
        PCT[0] = VecP2(0,0.0f);
        PCT[1] = VecP2(0.2f,1.0f);
        PCT[2] = VecP2(0.8f,0.75f);
        PCT[3] = VecP2(1.0f,0.0f);
        ParticleCT = LineGraph(0,1, 4, PCT);




        PositionV = new GLfloat[Max*4];


        ColorV = new GLfloat[Max*4];

        for(int i = 0; i<Max*4; ++i){
        	PositionV[i] = 1;
        	ColorV[i] = 1;
        }

        //Position = &PositionV[0];
    	//Color = &ColorV[0];
	}


	void setAxis(float x, float y){Ax = x; Ay = y;}

	void setDirection(float d)
	{
		Angle=d;
		Direction = 0.017453*d;
		Ax = std::cos(d); Ay = std::sin(d);
	}

	/////////////////////////////////////////////////////////////////////////
	VecP2 getRandomVector(float aoffset){
		float d = Direction+0.017453*aoffset;
		return VecP2(std::cos(d),std::sin(d));
	}



	void Draw(float time, float dt)
	{
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);

        if(time*1000>StartDuration+Duration){
        	StartDuration=time*1000;
        }
        NowDuration = time*1000-StartDuration;

        NowMax = Emission.getValue(NowDuration/Duration);

        float PerSecondP = 1.0/NowMax;

        int act = 0;

		for(int i =0; i<Max; ++i){
			if(P[i].Active){
			    act++;
				Update(i, time, dt);

		        PositionV[i*4]   = P[i].Pos.X;
		        PositionV[i*4+1] = P[i].Pos.Y;
		        PositionV[i*4+2] = 0;
		        PositionV[i*4+3] = P[i].Size;

		        ColorV[i*4]   = P[i].r;
		        ColorV[i*4+1] = P[i].g;
		        ColorV[i*4+2] = P[i].b;
		        ColorV[i*4+3] = P[i].t;


			}else{
		        PositionV[i*4]   = -10000000;
		        PositionV[i*4+1] = -10000000;
		        PositionV[i*4+2] = 0;
		        PositionV[i*4+3] = 0;


				if(time-OldEmissionP>PerSecondP){
					OldEmissionP+=PerSecondP;
					P[i].Active = true;
					ParticleReset(i, time);
				}
			}
		}

		DEBUG = act;



        glVertexAttribPointer(IDposition, 4, GL_FLOAT, false, 16, PositionV);
        glEnableVertexAttribArray(IDposition);


        glVertexAttribPointer(IDcolor, 4, GL_FLOAT, false, 16, ColorV);
        glEnableVertexAttribArray(IDcolor);

		glDrawArrays(GL_POINTS, 0, Max);


		glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	}


	void Update(int i, float t, float dt){
		//if(t-P[i].LUpdate>0.08f){
			float PROCENT = (t-P[i].Tstart)/P[i].Tlife;

			P[i].Ang = getRandomVector(ParticleAngOffset.getValue(PROCENT, P[i].AngM.X, P[i].AngM.Y));

			P[i].Vel = ParticleVel.getValue(PROCENT, P[i].VelM.X, P[i].VelM.Y);
			P[i].Size = ParticleSize.getValue(PROCENT, P[i].SizeM.X, P[i].SizeM.Y);

			P[i].t = ParticleCT.getValue(PROCENT, P[i].tM.X, P[i].tM.Y);
			//P[i].LUpdate = t;
		//}

		P[i].Pos.X += P[i].Vel*dt*P[i].Ang.X;
		P[i].Pos.Y += P[i].Vel*dt*P[i].Ang.Y;

		if(t>P[i].Tstart+P[i].Tlife){
			P[i].Active = false;
		}
	}

	void ParticleReset(int i, float t){
		P[i].Pos.set(X,Y);

		P[i].Ang = getRandomVector(ParticleAngOffset.getValue(0));
		P[i].AngM.set(ParticleAngOffset.Min, ParticleAngOffset.Max);

		P[i].Vel = ParticleVel.getValue(0);
		P[i].VelM.set(ParticleVel.Min, ParticleVel.Max);

		P[i].Size = ParticleSize.getValue(0);
		P[i].SizeM.set(ParticleSize.Min, ParticleSize.Max);

		P[i].Tstart=t;
		P[i].Tlife = ParticleLife.getValue(NowDuration/Duration)/1000.0;

		P[i].r = 1;
		P[i].g = 0.12156863;
		P[i].b = 0.04705882;

		P[i].t = ParticleCT.getValue(0);
		P[i].tM.set(ParticleCT.Min, ParticleCT.Max);

	}

    void setPosition(float x, float y){
        X = x;
        Y = y;
    }

};

#endif