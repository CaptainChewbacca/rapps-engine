#ifndef PARTICLES_H
#define PARTICLES_H


#include "ParticleSprite.h"

class Particles{
public:


	std::vector<ParticleSprite*> PS;
    int Size;
    int IDposition;
    int IDcolor;

    float DEBUG;

	Particles(int idp, int idc){
    	IDposition = idp;
    	IDcolor = idc;
		Size = 0;
	}

	int AddParticleSprite(int m, float t){
		ParticleSprite* p = new ParticleSprite(IDposition, IDcolor, m, t);
        PS.push_back(p);
        return Size++;
	}

	void DeleteParticleSprite(int id){
		PS.erase(PS.begin()+id);
	}

	void Update(float time, float dt, int id){
		PS[id]->Draw(time, dt);
		DEBUG = PS[id]->DEBUG;
	}
};

#endif