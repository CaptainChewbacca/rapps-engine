package com.game;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;

import com.engine.Graphics.Drawing;
import com.engine.Graphics.Shader;
import com.engine.QuickMath;
import com.engine.Graphics.RenderToTexture;
import com.engine.Graphics.Sprite;
import com.engine.Graphics.SpriteDrawing;


public class PostProcessing {
	int W, H;
	RenderToTexture RT;

	private FloatBuffer mTriangleVertices;

    Sprite Render;

	int[] lastTex;


	public PostProcessing(int w, int h){
		W = w; H = h;


		float quadv[] =
            { -w*0.5f, -h*0.5f,
              -w*0.5f, h*0.5f,
               w*0.5f, h*0.5f,
               w*0.5f, -h*0.5f};

		Render = new Sprite(w/2,w/2);
		Render.mTriangleVertices = ByteBuffer.allocateDirect(quadv.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		Render.mTriangleVertices.put(quadv).position(0);
		Render.SetPosition(0, 0);


		RT = new RenderToTexture(w/2,h/2);


		float quadv1[] =
				{ 0, 0,
				  0, 1,
				  1, 1,
				  1, 0};

		mTriangleVertices = ByteBuffer.allocateDirect(quadv.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mTriangleVertices.put(quadv1).position(0);
	}

	
	public void begin(){
		RT.Begin();
	}


	public void Draw(float[] Mat, float dt){

		//GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE);


        //GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE_MINUS_SRC_ALPHA);
	}
	
	
	public void end(){
		RT.End();
	}	
}
