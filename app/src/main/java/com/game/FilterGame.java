package com.game;

public class FilterGame {
	final public static short CATEGORY_GROUND = 0x0001;
	  
	final public static short CATEGORY_PLAYER = 0x0002; 

	final public static short CATEGORY_CARS = 0x0004; 

	final public static short CATEGORY_HOUSES = 0x0008; 
	
	
	final public static short MASK_PLAYER = CATEGORY_CARS | CATEGORY_GROUND | CATEGORY_HOUSES; // или ~MASK_PLAYER
	final public static short MASK_CARS =  CATEGORY_PLAYER | CATEGORY_GROUND; 
	final public static short MASK_HOUSES = CATEGORY_PLAYER; 
	final public static short MASK_GROUND = -1;
	
}
