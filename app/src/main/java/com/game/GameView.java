package com.game;


import android.opengl.GLES20;

import com.engine.Graphics.*;
import com.engine.GUI.*;
import com.engine.*;
import com.phys.Deb;
import com.phys.callbacks.ContactImpulse;
import com.phys.callbacks.ContactListener;
import com.phys.collision.Manifold;
import com.phys.collision.shapes.PolygonShape;
import com.phys.common.Rot;
import com.phys.common.Transform;
import com.phys.common.Vec2;
import com.phys.dynamics.Body;
import com.phys.dynamics.BodyDef;
import com.phys.dynamics.FixtureDef;
import com.phys.dynamics.World;
import com.phys.dynamics.contacts.Contact;




public class GameView implements GLRenderView {
	
	public static Info AppInfo;


	Camera cam;
	float CamX, CamY;
	float CamZ = 0;  //Zoom
	
    int W;
    int H;
	
    SpriteDrawing SpriteDraw;
    PrimDrawing PrimDraw;
    GuiDrawing GuiDraw;


    Text TextSpr;


	GameButton GameB;
    MainGUI MGUI;

    public boolean iPause = false;
    boolean iResume = false;
    
    
    World world;
    Deb debugDrawer;
    

    
    Settings Setting;
    public GLRender App;
    public GameView(GLRender main) {

    	AppInfo = main.AppInfo;
    	App = main;

    }
	


    boolean Game = false;
    PostProcessing PostPR;
    float DEB = 0;
    


	ParticleDrawing PARTD;
	ParticleSprite PARTSP;
	Sprite Test;


    ContactListener ContactL = new ContactListener(){
		@Override
		public void beginContact(Contact contact) {

		}

		@Override
		public void endContact(Contact contact) {

		}

		@Override
		public void preSolve(Contact contact, Manifold oldManifold) {
		}

		@Override
		public void postSolve(Contact contact, ContactImpulse impulse) {

		}

    };
 	
	@Override
	public void Create(int w, int h) {
		W = w;              
		H = h;   
		GLES20.glEnable(GLES20.GL_BLEND);
		GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE_MINUS_SRC_ALPHA);	

		
        GLES20.glViewport(0, 0, W, H);
        GLES20.glLineWidth(1);

		
    	cam = new Camera(W, H, 5f);
    	AppInfo.SetCamera(cam);
    	cam.SetX(0);
    	cam.SetY(0);
    	
    	CamX = 0; CamY = 0;

    	
    	
		SpriteDraw = new SpriteDrawing();
		PrimDraw = new PrimDrawing();	       
		MGUI = new MainGUI();


		GameB = new GameButton(this);

		
        GuiDraw = new GuiDrawing(W, H);

        TextSpr = new Text("1.fnt", "1");
		TextSpr.SetPosition(-W/2, 0);
		TextSpr.SetSize(H/15f);
		TextSpr.SetText(" ");
        
        
        world = new World(new Vec2(0, -10f));
        debugDrawer = new Deb(null);
        world.setDebugDraw(debugDrawer);
		world.setContinuousPhysics(false);
		world.setAutoClearForces(false);
		world.setContactListener(ContactL);
		debugDrawer.setFlags(debugDrawer.e_shapeBit); 




        PostPR = new PostProcessing(W,H);


		PARTD = new ParticleDrawing();
		PARTSP = new ParticleSprite("sky", 1000, App.Time);


		Test = new Sprite("pickles");
		Test.SetPosition(0,2);
		Test.SetSize(1f);

	}


	@Override
	public void Render() {

        GLES20.glClearColor(0.1f, 0.1f, 0.1f,1.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);



    	fixedTimestepAccumulator += App.DeltaTime;
    	int nSteps = (int)Math.floor(fixedTimestepAccumulator / FIXED_TIMESTEP);
    	if (nSteps > 0){fixedTimestepAccumulator -= nSteps * FIXED_TIMESTEP;}
    	fixedTimestepAccumulatorRatio = fixedTimestepAccumulator / FIXED_TIMESTEP;
    	int nStepsClamped = Math.min(nSteps, MAX_STEPS);
    	for (int i = 0; i < nStepsClamped; ++ i){
    		world.step(FIXED_TIMESTEP, 6, 2);
    	}
    	world.clearForces();
    	smoothStates();


		CamX+=GameB.Joy1.Xf * App.DeltaTime * 15;
		CamY+=GameB.Joy1.Yf * App.DeltaTime * 15;

        cam.SetRotation(cam.A);
        cam.SetX(CamX);
        cam.SetY(CamY);
        cam.SetZoom((GameB.SL.Value+1));
        cam.Update();

		SpriteDraw.Begin(cam.ViewProjectionMatrix());

			SpriteDraw.SetColor(0, 0, 0, 1);

			SpriteDraw.SetColor(1, 1, 1, 1);
			SpriteDraw.BeginSprite();


			Test.Draw(SpriteDraw);
		SpriteDraw.End();



		/*PARTD.Begin(cam.ViewProjectionMatrix());
			PARTD.SetCamPixelInUnit(H/(2*cam.Zoom));
			//PARTSP.Draw(PARTD, App.Time, App.DeltaTime);
		PARTD.End();*/

    	
    	PrimDraw.Begin(cam.ViewProjectionMatrix());
    		world.drawDebugData();
    	PrimDraw.End();


		if(App.DeltaTime<0.5f)
			GameB.Update(App.DeltaTime, App.Time);


        GuiDraw.Begin();
        	GuiDraw.BeginGui();
        	GuiDraw.SetColor(1, 1, 1, 1);

			GameB.Draw(GuiDraw);
        	MGUI.Draw(GuiDraw);

		GuiDraw.SetColor(0, 0, 0, 1);
		TextSpr.Draw(SpriteDraw);


		GuiDraw.End();


		PrimDraw.Begin(GuiDraw.Mat);

		//PrimDraw.Circle(0,0, 0.5f*0.987f*H);

		PrimDraw.End();


		TextSpr.SetText("WORK!");
        //TextSpr.SetText("FPS: "+App.FPS+'\n'+"DeltaTime(ms): "+(int)(App.DeltaTime*1000)+'\n'+GameB.SL.Value);//+'\n'+JNImanager.getDebug()
	}

	

	
	float X,Y;
  	float DrX,DrY,CDrX,CDrY;
  	int DrI = -20;
  	
  	
	@Override
	public void TouchDown(float x, float y, int i) {
	    if(MGUI.TouchDown(x, y, i)){
	    	
	    }
	    else
	    {
       	    X = (x+W*0.5f)/(float)(H);
       	    Y = 1.0f+(y-H*0.5f)/(float)(H);
	    }
	}

	@Override
	public void TouchUp(int i) {
    	MGUI.TouchUp(i);
	}


	@Override
	public void TouchDrag(float x, float y, int i) {
		MGUI.TouchDrag(x, y, i);
		
   	    X = (x+W*0.5f)/(float)(H);
   	    Y = 1.0f+(y-H*0.5f)/(float)(H);    
	}
	

	@Override
	public void Destroy() {
		
	}	
	
	

    float FIXED_TIMESTEP = 1.f/60.f;
    int MAX_STEPS = 5;
	float fixedTimestepAccumulator = 0;
	float fixedTimestepAccumulatorRatio = 0;	
	void smoothStates()
	{
		float dt = fixedTimestepAccumulatorRatio * FIXED_TIMESTEP;
		float oneMinusRatio = 1.f - fixedTimestepAccumulatorRatio;
	 
		for (Body b = world.getBodyList(); b != null; b = b.getNext())
		{
			if (b.getType() != b.getType().STATIC)
			{
				b.setUserData(new Transform(new Vec2(b.getPosition().x + dt * b.getLinearVelocity().x, 
						b.getPosition().y + dt * b.getLinearVelocity().y),
						new Rot(b.getAngle()+dt*b.getAngularVelocity())));
				/*b.setUserData(new Transform(new Vec2(
						fixedTimestepAccumulatorRatio*b.getPosition().x + oneMinusRatio * ((Transform)b.getUserData()).p.x, 
						fixedTimestepAccumulatorRatio*b.getPosition().y + oneMinusRatio * ((Transform)b.getUserData()).p.x),
						new Rot(b.getAngle()+dt*b.getAngularVelocity())));*/
			}
		}
	}
 
	void resetSmoothStates()
	{
		for (Body b = world.getBodyList(); b != null; b = b.getNext())
		{
			if (b.getType()!= b.getType().STATIC)
			{
				b.setUserData(new Transform(b.getPosition(), new Rot(b.getAngle())));
			}
		}
	}	
	
}
