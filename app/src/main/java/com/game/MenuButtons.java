package com.game;

import com.engine.GUI.Button;
import com.engine.GUI.GuiDrawing;
import com.engine.GUI.Slider;
import com.engine.GUI.TouchListener;
import com.engine.Graphics.Sprite;
import com.engine.Graphics.SpriteDrawing;

public class MenuButtons {

	public static MenuView MV;
	
	Button Start;
	Button Settings;	
	Button BSettings;


	//Slider SL;


	boolean ToGame = false;
	boolean ToSettings = false;
	
	
	public MenuButtons(MenuView mv){
		MV= mv;

		//19.333x x
		//2.4525x x
		float Size = MV.H/20f;


		Button Exit = new Button("mexit", MV.W/2 - Size, MV.H/2 - Size, 90*MV.H/2000f, 3);
		MV.MGUI.Add(Exit);
		Exit.SetTouchListener(new TouchListener(){
			public void TouchDown(float x, float y){
				int pid = android.os.Process.myPid();
				android.os.Process.killProcess(pid);
			}
		});



		Start = new Button("mstart", -MV.W/2f + Size, 0 + Size, 1.5f*Size, 2);
        MV.MGUI.Add(Start);
        Start.SetTouchListener(new TouchListener(){
        	public void TouchDown(float x, float y){
        		ToGame = true;
        		//MV.App.toGame(0);
        	}	
        });
        
		
        Settings = new Button("msettings", -MV.W/2f + Size, -MV.H/2f + Size, 1.35f*Size, 1);
        MV.MGUI.Add(Settings);
        Settings.SetTouchListener(new TouchListener(){
        	public void TouchDown(float x, float y){
        		ToSettings = true;
        		//MV.App.toGame(0);
        	}	
        });	
    
        
        BSettings = new Button("mup", MV.W/2f - Size, -1.5f*MV.H + Size, 1.35f*Size, 4);
        MV.MGUI.Add(BSettings);
        BSettings.SetTouchListener(new TouchListener(){
        	public void TouchDown(float x, float y){
        		ToSettings = false;
        		//MV.App.toGame(0);
        	}	
        });



		//SL = new Slider(-MV.W/2f + Size*6,-MV.H/2f - Size*10f, Size*1.5f, 5, 50, MV.W,MV.H);
		//MV.MGUI.Add(SL);
	}
	
	
	public void Draw(GuiDrawing dr){

	}
	
	
	public void Update(float delta, float time){

		/* Variant 2
		if(Rainbow.Y>5*MV.H/12f+MV.H/17f){
			//Start.setButtonPosition(Start.X, Start.Y-(Start.Y-MV.H/6f)*delta);
			Rainbow.setPosition(Rainbow.X, Rainbow.Y-(Rainbow.Y-5*MV.H/12f-MV.H/17f)*delta);
		}
		 */
		
		if(ToGame == true){
			MV.cam.SetX(MV.cam.X-(MV.cam.X+2*MV.W)*delta);
			if(MV.cam.X<-MV.W*1.1f){
				MV.App.ToGame(0);
			}
		}else{
		
			if(ToSettings == true){
				MV.cam.SetY(MV.cam.Y-(MV.cam.Y+MV.H)*delta*5);
			}
			else{
				MV.cam.SetY(MV.cam.Y-MV.cam.Y*delta*5);
			}
		
		}
	}
	
	
	
}
