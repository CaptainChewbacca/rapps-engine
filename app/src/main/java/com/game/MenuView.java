package com.game;

import com.engine.*;
import com.engine.Graphics.*;
import com.engine.GUI.*;

import android.opengl.GLES20;


public class MenuView implements GLRenderView {
	
	public static Info AppInfo;
	
    int W;
    int H;
    
    
	Camera cam;
	
    SpriteDrawing SpriteDraw;
    GuiDrawing GuiDraw;

    MainGUI MGUI;
	
	MenuButtons MenuB;

	Text TX;
	
	public GLRender App;
    public MenuView(GLRender main){
    	AppInfo = main.AppInfo;
    	App = main;	
    }
	
    
	@Override
	public void Create(int w, int h) {
		W = w;              
		H = h;   
		GLES20.glEnable(GLES20.GL_BLEND);
		GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        GLES20.glViewport(0, 0, W, H);
        GLES20.glLineWidth(1);
	
		
    	cam = new Camera(W, H, H/2f);
    	AppInfo.SetCamera(cam);
    	
    	
		SpriteDraw = new SpriteDrawing();
		GuiDraw = new GuiDrawing(w,h);
		MGUI = new MainGUI();
		
		
		MenuB = new MenuButtons(this);

		TX = new Text("3.fnt", "3");
		TX.SetSize(H/10,H/10);
		TX.SetPosition(0, H/4);
		TX.SetText("Hello world!");
	}


	
	
	@Override
	public void Render() {
		
		
        GLES20.glClearColor(.91f, .91f, .91f,1.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

		cam.Update();

		
		SpriteDraw.Begin(cam.ViewProjectionMatrix);
		    SpriteDraw.BeginSprite();
		    float OX = cam.X/2f;
		    float OY = cam.Y/2f;

		TX.Draw(SpriteDraw);
	    SpriteDraw.End();

	    if(App.DeltaTime<0.5f)
	    	MenuB.Update(App.DeltaTime, App.Time);

        GuiDraw.Begin(cam.ViewProjectionMatrix());
           GuiDraw.BeginGui();
           MenuB.Draw(GuiDraw);
           MGUI.Draw(GuiDraw);	
        GuiDraw.End();
	}

	@Override
	public void Destroy() {

	}

	@Override
	public void TouchDown(float x, float y, int i) {

		MGUI.TouchDown(x+cam.X, y+cam.Y, i);	
	}

	@Override
	public void TouchDrag(float x, float y, int i) {
		MGUI.TouchDrag(x+cam.X, y+cam.Y, i);
	}

	@Override
	public void TouchUp(int i) {
		MGUI.TouchUp(i);
	}

}
