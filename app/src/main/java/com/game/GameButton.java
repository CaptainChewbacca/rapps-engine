package com.game;

import com.engine.GUI.Button;
import com.engine.GUI.GuiDrawing;
import com.engine.GUI.Joystick;
import com.engine.GUI.Slider;
import com.engine.GUI.TouchListener;
import com.engine.Graphics.Sprite;


public class GameButton {


    static GameView GV;


    public Slider SL;

    Button SideBarBut;
    Button MenuBut;

    boolean SideBarOpen;
    Sprite SiderBarBack;

    float Size;

    Joystick Joy1;


    public GameButton(GameView gv){
        GV = gv;


        Size = GV.H/20f;
        SideBarBut = new Button("pause", GV.W/2, GV.H/2-Size, Size, 3);
        GV.MGUI.Add(SideBarBut);
        SideBarBut.SetTouchListener(new TouchListener(){
            public void TouchDown(float x, float y){
                SideBarOpen = !SideBarOpen;
            }
        });

        MenuBut = new Button("menu", GV.W/2 + GV.H/3 - Size*0.5f, -GV.H/2+Size*10, 1.2f*Size, 3);
        GV.MGUI.Add(MenuBut);
        MenuBut.SetTouchListener(new TouchListener(){
            public void TouchDown(float x, float y){
                GV.App.ToMenu();
            }
        });


        SL = new Slider(GV.H/3f,-GV.H/3f, GV.H/15f, 10, 50, GV.W,GV.H);
        GV.MGUI.Add(SL);

        SideBarOpen = false;

        SiderBarBack = new Sprite("sidebarback");
        SiderBarBack.SetSize(GV.H/3f, GV.H);
        SiderBarBack.SetPosition(SideBarBut.X+GV.H/6f, 0);

        Joy1 = new Joystick("joyf", -GV.W/2f + GV.H/6f, -GV.H/2f + GV.H/6f, GV.H/14f);
        GV.MGUI.Add(Joy1);
    }

    public void Draw(GuiDrawing dr){
        SiderBarBack.Draw(dr);
        //Joy1.Draw(dr);
    }

    public void Update(float delta, float time){
        if(SideBarOpen){
            if(SideBarBut.X>GV.W/2f-GV.H/3f){
                SideBarBut.SetButtonPosition(SideBarBut.X - (SideBarBut.X - GV.W/2f + GV.H/3f)*delta*5, SideBarBut.Y);

                MenuBut.SetButtonPosition(SideBarBut.X + GV.H/3f - Size*0.5f, 0);

                SiderBarBack.SetPosition(SideBarBut.X + GV.H/6f, 0);
            }
        }else{
            if(SideBarBut.X<GV.W/2f){
                SideBarBut.SetButtonPosition(SideBarBut.X - (SideBarBut.X - GV.W/2f)*delta*5, SideBarBut.Y);

                MenuBut.SetButtonPosition(SideBarBut.X + GV.H/3f - Size*0.5f, 0);

                SiderBarBack.SetPosition(SideBarBut.X + GV.H/6f, 0);
            }

        }

    }

}
