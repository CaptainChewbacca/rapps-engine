
package com.phys.dynamics.contacts;

import com.phys.collision.Manifold;
import com.phys.collision.shapes.ChainShape;
import com.phys.collision.shapes.EdgeShape;
import com.phys.collision.shapes.PolygonShape;
import com.phys.collision.shapes.ShapeType;
import com.phys.common.Transform;
import com.phys.dynamics.Fixture;
import com.phys.pooling.IWorldPool;

public class ChainAndPolygonContact extends Contact {

  public ChainAndPolygonContact(IWorldPool argPool) {
    super(argPool);
  }

  @Override
  public void init(Fixture fA, int indexA, Fixture fB, int indexB) {
    super.init(fA, indexA, fB, indexB);
    assert (m_fixtureA.getType() == ShapeType.CHAIN);
    assert (m_fixtureB.getType() == ShapeType.POLYGON);
  }

  private final EdgeShape edge = new EdgeShape();

  @Override
  public void evaluate(Manifold manifold, Transform xfA, Transform xfB) {
    ChainShape chain = (ChainShape) m_fixtureA.getShape();
    chain.getChildEdge(edge, m_indexA);
    pool.getCollision().collideEdgeAndPolygon(manifold, edge, xfA,
        (PolygonShape) m_fixtureB.getShape(), xfB);
  }
}
