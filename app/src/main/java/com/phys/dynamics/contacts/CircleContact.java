
package com.phys.dynamics.contacts;

import com.phys.collision.Manifold;
import com.phys.collision.shapes.CircleShape;
import com.phys.collision.shapes.ShapeType;
import com.phys.common.Transform;
import com.phys.dynamics.Fixture;
import com.phys.pooling.IWorldPool;

public class CircleContact extends Contact {

  public CircleContact(IWorldPool argPool) {
    super(argPool);
  }

  public void init(Fixture fixtureA, Fixture fixtureB) {
    super.init(fixtureA, 0, fixtureB, 0);
    assert (m_fixtureA.getType() == ShapeType.CIRCLE);
    assert (m_fixtureB.getType() == ShapeType.CIRCLE);
  }

  @Override
  public void evaluate(Manifold manifold, Transform xfA, Transform xfB) {
    pool.getCollision().collideCircles(manifold, (CircleShape) m_fixtureA.getShape(), xfA,
        (CircleShape) m_fixtureB.getShape(), xfB);
  }
}
