
package com.phys.dynamics.contacts;

import com.phys.dynamics.Body;

public class ContactEdge {

  /**
   * provides quick access to the other body attached.
   */
  public Body other = null;

  /**
   * the contact
   */
  public Contact contact = null;

  /**
   * the previous contact edge in the body's contact list
   */
  public ContactEdge prev = null;

  /**
   * the next contact edge in the body's contact list
   */
  public ContactEdge next = null;
}
