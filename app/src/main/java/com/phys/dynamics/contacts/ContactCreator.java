
package com.phys.dynamics.contacts;

import com.phys.dynamics.Fixture;
import com.phys.pooling.IWorldPool;

// updated to rev 100
public interface ContactCreator {

	public Contact contactCreateFcn(IWorldPool argPool, Fixture fixtureA, Fixture fixtureB);
	
	public void contactDestroyFcn(IWorldPool argPool, Contact contact);
}
