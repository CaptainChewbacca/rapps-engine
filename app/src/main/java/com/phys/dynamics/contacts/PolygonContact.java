
package com.phys.dynamics.contacts;

import com.phys.collision.Manifold;
import com.phys.collision.shapes.PolygonShape;
import com.phys.collision.shapes.ShapeType;
import com.phys.common.Transform;
import com.phys.dynamics.Fixture;
import com.phys.pooling.IWorldPool;

public class PolygonContact extends Contact {

  public PolygonContact(IWorldPool argPool) {
    super(argPool);
  }

  public void init(Fixture fixtureA, Fixture fixtureB) {
    super.init(fixtureA, 0, fixtureB, 0);
    assert (m_fixtureA.getType() == ShapeType.POLYGON);
    assert (m_fixtureB.getType() == ShapeType.POLYGON);
  }

  @Override
  public void evaluate(Manifold manifold, Transform xfA, Transform xfB) {
    pool.getCollision().collidePolygons(manifold, (PolygonShape) m_fixtureA.getShape(), xfA,
        (PolygonShape) m_fixtureB.getShape(), xfB);
  }

}
