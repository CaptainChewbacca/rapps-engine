
package com.phys.dynamics.contacts;

import com.phys.collision.Manifold;
import com.phys.collision.shapes.EdgeShape;
import com.phys.collision.shapes.PolygonShape;
import com.phys.collision.shapes.ShapeType;
import com.phys.common.Transform;
import com.phys.dynamics.Fixture;
import com.phys.pooling.IWorldPool;

public class EdgeAndPolygonContact extends Contact {

  public EdgeAndPolygonContact(IWorldPool argPool) {
    super(argPool);
  }

  @Override
  public void init(Fixture fA, int indexA, Fixture fB, int indexB) {
    super.init(fA, indexA, fB, indexB);
    assert (m_fixtureA.getType() == ShapeType.EDGE);
    assert (m_fixtureB.getType() == ShapeType.POLYGON);
  }

  @Override
  public void evaluate(Manifold manifold, Transform xfA, Transform xfB) {
    pool.getCollision().collideEdgeAndPolygon(manifold, (EdgeShape) m_fixtureA.getShape(), xfA,
        (PolygonShape) m_fixtureB.getShape(), xfB);
  }
}
