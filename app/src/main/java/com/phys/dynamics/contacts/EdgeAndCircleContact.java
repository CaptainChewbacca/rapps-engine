
package com.phys.dynamics.contacts;

import com.phys.collision.Manifold;
import com.phys.collision.shapes.CircleShape;
import com.phys.collision.shapes.EdgeShape;
import com.phys.collision.shapes.ShapeType;
import com.phys.common.Transform;
import com.phys.dynamics.Fixture;
import com.phys.pooling.IWorldPool;

public class EdgeAndCircleContact extends Contact {

  public EdgeAndCircleContact(IWorldPool argPool) {
    super(argPool);
  }

  @Override
  public void init(Fixture fA, int indexA, Fixture fB, int indexB) {
    super.init(fA, indexA, fB, indexB);
    assert (m_fixtureA.getType() == ShapeType.EDGE);
    assert (m_fixtureB.getType() == ShapeType.CIRCLE);
  }

  @Override
  public void evaluate(Manifold manifold, Transform xfA, Transform xfB) {
    pool.getCollision().collideEdgeAndCircle(manifold, (EdgeShape) m_fixtureA.getShape(), xfA,
        (CircleShape) m_fixtureB.getShape(), xfB);
  }
}
