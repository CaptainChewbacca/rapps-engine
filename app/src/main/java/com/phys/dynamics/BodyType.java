
package com.phys.dynamics;


public enum BodyType {
	STATIC, KINEMATIC, DYNAMIC
}
