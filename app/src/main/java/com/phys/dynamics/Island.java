
package com.phys.dynamics;

import com.phys.callbacks.ContactImpulse;
import com.phys.callbacks.ContactListener;
import com.phys.common.MathUtils;
import com.phys.common.Settings;
import com.phys.common.Timer;
import com.phys.common.Vec2;
import com.phys.dynamics.contacts.Contact;
import com.phys.dynamics.contacts.ContactSolver;
import com.phys.dynamics.contacts.ContactSolver.ContactSolverDef;
import com.phys.dynamics.contacts.ContactVelocityConstraint;
import com.phys.dynamics.contacts.Position;
import com.phys.dynamics.contacts.Velocity;
import com.phys.dynamics.joints.Joint;


public class Island {

  public ContactListener m_listener;

  public Body[] m_bodies;
  public Contact[] m_contacts;
  public Joint[] m_joints;

  public Position[] m_positions;
  public Velocity[] m_velocities;

  public int m_bodyCount;
  public int m_jointCount;
  public int m_contactCount;

  public int m_bodyCapacity;
  public int m_contactCapacity;
  public int m_jointCapacity;

  public Island() {

  }

  public void init(int bodyCapacity, int contactCapacity, int jointCapacity,
      ContactListener listener) {
    // System.out.println("Initializing Island");
    m_bodyCapacity = bodyCapacity;
    m_contactCapacity = contactCapacity;
    m_jointCapacity = jointCapacity;
    m_bodyCount = 0;
    m_contactCount = 0;
    m_jointCount = 0;

    m_listener = listener;

    if (m_bodies == null || m_bodyCapacity > m_bodies.length) {
      m_bodies = new Body[m_bodyCapacity];
    }
    if (m_joints == null || m_jointCapacity > m_joints.length) {
      m_joints = new Joint[m_jointCapacity];
    }
    if (m_contacts == null || m_contactCapacity > m_contacts.length) {
      m_contacts = new Contact[m_contactCapacity];
    }

    // dynamic array
    if (m_velocities == null || m_bodyCapacity > m_velocities.length) {
      final Velocity[] old = m_velocities == null ? new Velocity[0] : m_velocities;
      m_velocities = new Velocity[m_bodyCapacity];
      System.arraycopy(old, 0, m_velocities, 0, old.length);
      for (int i = old.length; i < m_velocities.length; i++) {
        m_velocities[i] = new Velocity();
      }
    }

    // dynamic array
    if (m_positions == null || m_bodyCapacity > m_positions.length) {
      final Position[] old = m_positions == null ? new Position[0] : m_positions;
      m_positions = new Position[m_bodyCapacity];
      System.arraycopy(old, 0, m_positions, 0, old.length);
      for (int i = old.length; i < m_positions.length; i++) {
        m_positions[i] = new Position();
      }
    }
  }

  public void clear() {
    m_bodyCount = 0;
    m_contactCount = 0;
    m_jointCount = 0;
  }

  private final ContactSolver contactSolver = new ContactSolver();
  private final Timer timer = new Timer();
  private final SolverData solverData = new SolverData();
  private final ContactSolverDef solverDef = new ContactSolverDef();

  public void solve(Profile profile, TimeStep step, Vec2 gravity, boolean allowSleep) {

    // System.out.println("Solving Island");
    float h = step.dt;

    // Integrate velocities and apply damping. Initialize the body state.
    for (int i = 0; i < m_bodyCount; ++i) {
      final Body b = m_bodies[i];
      final Vec2 c = b.m_sweep.c;
      float a = b.m_sweep.a;
      final Vec2 v = b.m_linearVelocity;
      float w = b.m_angularVelocity;

      // Store positions for continuous collision.
      b.m_sweep.c0.set(b.m_sweep.c);
      b.m_sweep.a0 = b.m_sweep.a;

      if (b.m_type == BodyType.DYNAMIC) {
        // Integrate velocities.
        // v += h * (b.m_gravityScale * gravity + b.m_invMass * b.m_force);
        v.x += h * (b.m_gravityScale * gravity.x + b.m_invMass * b.m_force.x);
        v.y += h * (b.m_gravityScale * gravity.y + b.m_invMass * b.m_force.y);
        w += h * b.m_invI * b.m_torque;

        // Apply damping.
        // ODE: dv/dt + c * v = 0
        // Solution: v(t) = v0 * exp(-c * t)
        // Time step: v(t + dt) = v0 * exp(-c * (t + dt)) = v0 * exp(-c * t) * exp(-c * dt) = v *
        // exp(-c * dt)
        // v2 = exp(-c * dt) * v1
        // Taylor expansion:
        // v2 = (1.0f - c * dt) * v1
        float a1 = MathUtils.clamp(1.0f - h * b.m_linearDamping, 0.0f, 1.0f);
        v.x *= a1;
        v.y *= a1;
        w *= MathUtils.clamp(1.0f - h * b.m_angularDamping, 0.0f, 1.0f);
      }

      m_positions[i].c.x = c.x;
      m_positions[i].c.y = c.y;
      m_positions[i].a = a;
      m_velocities[i].v.x = v.x;
      m_velocities[i].v.y = v.y;
      m_velocities[i].w = w;
    }

    timer.reset();

    // Solver data
    solverData.step = step;
    solverData.positions = m_positions;
    solverData.velocities = m_velocities;

    // Initialize velocity constraints.
    solverDef.step = step;
    solverDef.contacts = m_contacts;
    solverDef.count = m_contactCount;
    solverDef.positions = m_positions;
    solverDef.velocities = m_velocities;

    contactSolver.init(solverDef);
    // System.out.println("island init vel");
    contactSolver.initializeVelocityConstraints();

    if (step.warmStarting) {
      // System.out.println("island warm start");
      contactSolver.warmStart();
    }

    for (int i = 0; i < m_jointCount; ++i) {
      m_joints[i].initVelocityConstraints(solverData);
    }

    profile.solveInit = timer.getMilliseconds();

    // Solve velocity constraints
    timer.reset();
    // System.out.println("island solving velocities");
    for (int i = 0; i < step.velocityIterations; ++i) {
      for (int j = 0; j < m_jointCount; ++j) {
        m_joints[j].solveVelocityConstraints(solverData);
      }

      contactSolver.solveVelocityConstraints();
    }

    // Store impulses for warm starting
    contactSolver.storeImpulses();
    profile.solveVelocity = timer.getMilliseconds();

    // Integrate positions
    for (int i = 0; i < m_bodyCount; ++i) {
      final Vec2 c = m_positions[i].c;
      float a = m_positions[i].a;
      final Vec2 v = m_velocities[i].v;
      float w = m_velocities[i].w;

      // Check for large velocities
      float translationx = v.x * h;
      float translationy = v.y * h;

      if (translationx * translationx + translationy * translationy > Settings.maxTranslationSquared) {
        float ratio = Settings.maxTranslation
            / MathUtils.sqrt(translationx * translationx + translationy * translationy);
        v.x *= ratio;
        v.y *= ratio;
      }

      float rotation = h * w;
      if (rotation * rotation > Settings.maxRotationSquared) {
        float ratio = Settings.maxRotation / MathUtils.abs(rotation);
        w *= ratio;
      }

      // Integrate
      c.x += h * v.x;
      c.y += h * v.y;
      a += h * w;

      m_positions[i].a = a;
      m_velocities[i].w = w;
    }

    // Solve position constraints
    timer.reset();
    boolean positionSolved = false;
    for (int i = 0; i < step.positionIterations; ++i) {
      boolean contactsOkay = contactSolver.solvePositionConstraints();

      boolean jointsOkay = true;
      for (int j = 0; j < m_jointCount; ++j) {
        boolean jointOkay = m_joints[j].solvePositionConstraints(solverData);
        jointsOkay = jointsOkay && jointOkay;
      }

      if (contactsOkay && jointsOkay) {
        // Exit early if the position errors are small.
        positionSolved = true;
        break;
      }
    }

    // Copy state buffers back to the bodies
    for (int i = 0; i < m_bodyCount; ++i) {
      Body body = m_bodies[i];
      body.m_sweep.c.x = m_positions[i].c.x;
      body.m_sweep.c.y = m_positions[i].c.y;
      body.m_sweep.a = m_positions[i].a;
      body.m_linearVelocity.x = m_velocities[i].v.x;
      body.m_linearVelocity.y = m_velocities[i].v.y;
      body.m_angularVelocity = m_velocities[i].w;
      body.synchronizeTransform();
    }

    profile.solvePosition = timer.getMilliseconds();

    report(contactSolver.m_velocityConstraints);

    if (allowSleep) {
      float minSleepTime = Float.MAX_VALUE;

      final float linTolSqr = Settings.linearSleepTolerance * Settings.linearSleepTolerance;
      final float angTolSqr = Settings.angularSleepTolerance * Settings.angularSleepTolerance;

      for (int i = 0; i < m_bodyCount; ++i) {
        Body b = m_bodies[i];
        if (b.getType() == BodyType.STATIC) {
          continue;
        }

        if ((b.m_flags & Body.e_autoSleepFlag) == 0
            || b.m_angularVelocity * b.m_angularVelocity > angTolSqr
            || Vec2.dot(b.m_linearVelocity, b.m_linearVelocity) > linTolSqr) {
          b.m_sleepTime = 0.0f;
          minSleepTime = 0.0f;
        } else {
          b.m_sleepTime += h;
          minSleepTime = MathUtils.min(minSleepTime, b.m_sleepTime);
        }
      }

      if (minSleepTime >= Settings.timeToSleep && positionSolved) {
        for (int i = 0; i < m_bodyCount; ++i) {
          Body b = m_bodies[i];
          b.setAwake(false);
        }
      }
    }
  }

  private final ContactSolver toiContactSolver = new ContactSolver();
  private final ContactSolverDef toiSolverDef = new ContactSolverDef();

  public void solveTOI(TimeStep subStep, int toiIndexA, int toiIndexB) {
    assert (toiIndexA < m_bodyCount);
    assert (toiIndexB < m_bodyCount);

    // Initialize the body state.
    for (int i = 0; i < m_bodyCount; ++i) {
      m_positions[i].c.x = m_bodies[i].m_sweep.c.x;
      m_positions[i].c.y = m_bodies[i].m_sweep.c.y;
      m_positions[i].a = m_bodies[i].m_sweep.a;
      m_velocities[i].v.x = m_bodies[i].m_linearVelocity.x;
      m_velocities[i].v.y = m_bodies[i].m_linearVelocity.y;
      m_velocities[i].w = m_bodies[i].m_angularVelocity;
    }

    toiSolverDef.contacts = m_contacts;
    toiSolverDef.count = m_contactCount;
    toiSolverDef.step = subStep;
    toiSolverDef.positions = m_positions;
    toiSolverDef.velocities = m_velocities;
    toiContactSolver.init(toiSolverDef);

    // Solve position constraints.
    for (int i = 0; i < subStep.positionIterations; ++i) {
      boolean contactsOkay = toiContactSolver.solveTOIPositionConstraints(toiIndexA, toiIndexB);
      if (contactsOkay) {
        break;
      }
    }
    // #if 0
    // // Is the new position really safe?
    // for (int i = 0; i < m_contactCount; ++i)
    // {
    // Contact* c = m_contacts[i];
    // Fixture* fA = c.GetFixtureA();
    // Fixture* fB = c.GetFixtureB();
    //
    // Body bA = fA.GetBody();
    // Body bB = fB.GetBody();
    //
    // int indexA = c.GetChildIndexA();
    // int indexB = c.GetChildIndexB();
    //
    // DistanceInput input;
    // input.proxyA.Set(fA.GetShape(), indexA);
    // input.proxyB.Set(fB.GetShape(), indexB);
    // input.transformA = bA.GetTransform();
    // input.transformB = bB.GetTransform();
    // input.useRadii = false;
    //
    // DistanceOutput output;
    // SimplexCache cache;
    // cache.count = 0;
    // Distance(&output, &cache, &input);
    //
    // if (output.distance == 0 || cache.count == 3)
    // {
    // cache.count += 0;
    // }
    // }
    // #endif

    // Leap of faith to new safe state.
    m_bodies[toiIndexA].m_sweep.c0.x = m_positions[toiIndexA].c.x;
    m_bodies[toiIndexA].m_sweep.c0.y = m_positions[toiIndexA].c.y;
    m_bodies[toiIndexA].m_sweep.a0 = m_positions[toiIndexA].a;
    m_bodies[toiIndexB].m_sweep.c0.set(m_positions[toiIndexB].c);
    m_bodies[toiIndexB].m_sweep.a0 = m_positions[toiIndexB].a;

    // No warm starting is needed for TOI events because warm
    // starting impulses were applied in the discrete solver.
    toiContactSolver.initializeVelocityConstraints();

    // Solve velocity constraints.
    for (int i = 0; i < subStep.velocityIterations; ++i) {
      toiContactSolver.solveVelocityConstraints();
    }

    // Don't store the TOI contact forces for warm starting
    // because they can be quite large.

    float h = subStep.dt;

    // Integrate positions
    for (int i = 0; i < m_bodyCount; ++i) {
      Vec2 c = m_positions[i].c;
      float a = m_positions[i].a;
      Vec2 v = m_velocities[i].v;
      float w = m_velocities[i].w;

      // Check for large velocities
      float translationx = v.x * h;
      float translationy = v.y * h;
      if (translationx * translationx + translationy * translationy > Settings.maxTranslationSquared) {
        float ratio = Settings.maxTranslation
            / MathUtils.sqrt(translationx * translationx + translationy * translationy);
        v.mulLocal(ratio);
      }

      float rotation = h * w;
      if (rotation * rotation > Settings.maxRotationSquared) {
        float ratio = Settings.maxRotation / MathUtils.abs(rotation);
        w *= ratio;
      }

      // Integrate
      c.x += v.x * h;
      c.y += v.y * h;
      a += h * w;

      m_positions[i].c.x = c.x;
      m_positions[i].c.y = c.y;
      m_positions[i].a = a;
      m_velocities[i].v.x = v.x;
      m_velocities[i].v.y = v.y;
      m_velocities[i].w = w;

      // Sync bodies
      Body body = m_bodies[i];
      body.m_sweep.c.x = c.x;
      body.m_sweep.c.y = c.y;
      body.m_sweep.a = a;
      body.m_linearVelocity.x = v.x;
      body.m_linearVelocity.y = v.y;
      body.m_angularVelocity = w;
      body.synchronizeTransform();
    }

    report(toiContactSolver.m_velocityConstraints);
  }

  public void add(Body body) {
    assert (m_bodyCount < m_bodyCapacity);
    body.m_islandIndex = m_bodyCount;
    m_bodies[m_bodyCount] = body;
    ++m_bodyCount;
  }

  public void add(Contact contact) {
    assert (m_contactCount < m_contactCapacity);
    m_contacts[m_contactCount++] = contact;
  }

  public void add(Joint joint) {
    assert (m_jointCount < m_jointCapacity);
    m_joints[m_jointCount++] = joint;
  }

  private final ContactImpulse impulse = new ContactImpulse();

  public void report(ContactVelocityConstraint[] constraints) {
    if (m_listener == null) {
      return;
    }

    for (int i = 0; i < m_contactCount; ++i) {
      Contact c = m_contacts[i];

      ContactVelocityConstraint vc = constraints[i];
      impulse.count = vc.pointCount;
      for (int j = 0; j < vc.pointCount; ++j) {
        impulse.normalImpulses[j] = vc.points[j].normalImpulse;
        impulse.tangentImpulses[j] = vc.points[j].tangentImpulse;
      }

      m_listener.postSolve(c, impulse);
    }
  }
}
