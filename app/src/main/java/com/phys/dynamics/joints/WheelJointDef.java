
package com.phys.dynamics.joints;

import com.phys.common.Vec2;
import com.phys.dynamics.Body;

public class WheelJointDef extends JointDef {
	
	/**
	 * The local anchor point relative to body1's origin.
	 */
	public final Vec2 localAnchorA = new Vec2();
	
	/**
	 * The local anchor point relative to body2's origin.
	 */
	public final Vec2 localAnchorB = new Vec2();
	
	/**
	 * The local translation axis in body1.
	 */
	public final Vec2 localAxisA = new Vec2();

	/**
	 * Enable/disable the joint motor.
	 */
	public boolean enableMotor;
	
	/**
	 * The maximum motor torque, usually in N-m.
	 */
	public float maxMotorTorque;
	
	/**
	 * The desired motor speed in radians per second.
	 */
	public float motorSpeed;
	
	/**
	 * Suspension frequency, zero indicates no suspension
	 */
	public float frequencyHz;
	
	/**
	 * Suspension damping ratio, one indicates critical damping
	 */
	public float dampingRatio;
	
	public WheelJointDef() {
		type = JointType.WHEEL;
		localAxisA.set(1, 0);
		enableMotor = false;
		maxMotorTorque = 0f;
		motorSpeed = 0f;
	}
	
	public void initialize(Body b1, Body b2, Vec2 anchor, Vec2 axis) {
		bodyA = b1;
		bodyB = b2;
		b1.getLocalPointToOut(anchor, localAnchorA);
		b2.getLocalPointToOut(anchor, localAnchorB);
		bodyA.getLocalVectorToOut(axis, localAxisA);
	}
}
