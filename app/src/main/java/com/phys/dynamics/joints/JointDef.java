
package com.phys.dynamics.joints;

import com.phys.dynamics.Body;


public class JointDef {

	public JointDef(){
		type = JointType.UNKNOWN;
		userData = null;
		bodyA = null;
		bodyB = null;
		collideConnected = false;
	}
	/**
	 * The joint type is set automatically for concrete joint types.
	 */
	public JointType type;
	
	/**
	 * Use this to attach application specific data to your joints.
	 */
	public Object userData;
	
	/**
	 * The first attached body.
	 */
	public Body bodyA;
	
	/**
	 * The second attached body.
	 */
	public Body bodyB;
	
	/**
	 * Set this flag to true if the attached bodies should collide.
	 */
	public boolean collideConnected;
}
