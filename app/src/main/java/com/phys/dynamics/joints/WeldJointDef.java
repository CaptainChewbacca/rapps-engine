
package com.phys.dynamics.joints;
import com.phys.common.Vec2;
import com.phys.dynamics.Body;
import com.phys.dynamics.joints.JointDef;
import com.phys.dynamics.joints.JointType;


public class WeldJointDef extends JointDef {
	/**
	 * The local anchor point relative to body1's origin.
	 */
	public final Vec2 localAnchorA;

	/**
	 * The local anchor point relative to body2's origin.
	 */
	public final Vec2  localAnchorB;

	/**
	 * The body2 angle minus body1 angle in the reference state (radians).
	 */
	public float referenceAngle;
	
	/**
	 * The mass-spring-damper frequency in Hertz. Rotation only.
	 * Disable softness with a value of 0.
	 */
	public float frequencyHz;
	
	/**
	 * The damping ratio. 0 = no damping, 1 = critical damping.
	 */
	public float dampingRatio;
	
	public WeldJointDef(){
		type = JointType.WELD;
		localAnchorA = new Vec2();
		localAnchorB = new Vec2();
		referenceAngle = 0.0f;
	}
	
	/**
	 * Initialize the bodies, anchors, and reference angle using a world
	 * anchor point.
	 * @param bA
	 * @param bB
	 * @param anchor
	 */
	public void initialize(Body bA, Body bB, Vec2 anchor){
		bodyA = bA;
		bodyB = bB;
		bodyA.getLocalPointToOut(anchor, localAnchorA);
		bodyB.getLocalPointToOut(anchor, localAnchorB);
		referenceAngle = bodyB.getAngle() - bodyA.getAngle();
	}
}
