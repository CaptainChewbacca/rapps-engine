
package com.phys.dynamics.joints;

public enum LimitState {
	INACTIVE, AT_LOWER, AT_UPPER, EQUAL
}
