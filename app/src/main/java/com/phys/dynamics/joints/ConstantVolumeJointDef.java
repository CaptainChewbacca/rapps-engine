package com.phys.dynamics.joints;


import java.util.ArrayList;

import com.phys.dynamics.Body;

public class ConstantVolumeJointDef extends JointDef {
  public float frequencyHz;
  public float dampingRatio;

  ArrayList<Body> bodies;
  ArrayList<DistanceJoint> joints;

  public ConstantVolumeJointDef() {
    type = JointType.CONSTANT_VOLUME;
    bodies = new ArrayList<Body>();
    joints = null;
    collideConnected = false;
    frequencyHz = 0.0f;
    dampingRatio = 0.0f;
  }

  /**
   * Adds a body to the group
   * 
   * @param argBody
   */
  public void addBody(Body argBody) {
    bodies.add(argBody);
    if (bodies.size() == 1) {
      bodyA = argBody;
    }
    if (bodies.size() == 2) {
      bodyB = argBody;
    }
  }

  /**
   * Adds a body and the pre-made distance joint. Should only be used for deserialization.
   */
  public void addBodyAndJoint(Body argBody, DistanceJoint argJoint) {
    addBody(argBody);
    if (joints == null) {
      joints = new ArrayList<DistanceJoint>();
    }
    joints.add(argJoint);
  }
}
