
package com.phys.dynamics.joints;

import com.phys.common.Vec2;
import com.phys.dynamics.Body;


public class FrictionJointDef extends JointDef {


	/**
	 * The local anchor point relative to bodyA's origin.
	 */
	public final Vec2 localAnchorA;

	/**
	 * The local anchor point relative to bodyB's origin.
	 */
	public final Vec2 localAnchorB;

	/**
	 * The maximum friction force in N.
	 */
	public float maxForce;

	/**
	 * The maximum friction torque in N-m.
	 */
	public float maxTorque;
	
	public FrictionJointDef(){
		type = JointType.FRICTION;
		localAnchorA = new Vec2();
		localAnchorB = new Vec2();
		maxForce = 0f;
		maxTorque = 0f;
	}
	/**
	 * Initialize the bodies, anchors, axis, and reference angle using the world
	 * anchor and world axis.
	 */
	public void initialize(Body bA, Body bB, Vec2 anchor){
		bodyA = bA;
		bodyB = bB;
		bA.getLocalPointToOut(anchor, localAnchorA);
		bB.getLocalPointToOut(anchor, localAnchorB);
	}
}
