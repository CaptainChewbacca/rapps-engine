
package com.phys.dynamics.joints;

public enum JointType {
	UNKNOWN, REVOLUTE, PRISMATIC, DISTANCE, PULLEY,
	MOUSE, GEAR, WHEEL, WELD, FRICTION, ROPE, CONSTANT_VOLUME
}
