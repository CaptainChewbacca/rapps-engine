
package com.phys.dynamics.joints;

import com.phys.dynamics.Body;


public class JointEdge {
	
	/**
	 * Provides quick access to the other body attached
	 */
	public Body other = null;
	
	/**
	 * the joint
	 */
	public Joint joint = null;
	
	/**
	 * the previous joint edge in the body's joint list
	 */
	public JointEdge prev = null;
	
	/**
	 * the next joint edge in the body's joint list
	 */
	public JointEdge next = null;
}
