package com.phys.dynamics.joints;


public class GearJointDef extends JointDef {
	/**
	 * The first revolute/prismatic joint attached to the gear joint.
	 */
	public Joint joint1;

	/**
	 * The second revolute/prismatic joint attached to the gear joint.
	 */
	public Joint joint2;

	/**
	 * Gear ratio.
	 * @see GearJoint
	 */
	public float ratio;
	
	public GearJointDef(){
		type = JointType.GEAR;
		joint1 = null;
		joint2 = null;
	}	
}
