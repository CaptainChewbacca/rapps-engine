
package com.phys.dynamics.joints;

import com.phys.common.Vec2;

public class Jacobian {
	public final Vec2 linearA = new Vec2();
	public float angularA;
	public float angularB;
}
