package com.phys.dynamics;

import com.phys.dynamics.contacts.Position;
import com.phys.dynamics.contacts.Velocity;

public class SolverData {
  public TimeStep step;
  public Position[] positions;
  public Velocity[] velocities;
}
