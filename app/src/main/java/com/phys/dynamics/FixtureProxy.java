
package com.phys.dynamics;

import com.phys.collision.AABB;

public class FixtureProxy {
  public final AABB aabb = new AABB();
  Fixture fixture;
  int childIndex;
  int proxyId;
}
