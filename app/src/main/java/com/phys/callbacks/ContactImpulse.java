package com.phys.callbacks;

import com.phys.common.Settings;


public class ContactImpulse {
  public float[] normalImpulses = new float[Settings.maxManifoldPoints];
  public float[] tangentImpulses = new float[Settings.maxManifoldPoints];
  public int count;
}
