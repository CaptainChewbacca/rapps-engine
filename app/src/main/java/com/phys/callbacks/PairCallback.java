package com.phys.callbacks;

public interface PairCallback {
	public void addPair(Object userDataA, Object userDataB);
}
