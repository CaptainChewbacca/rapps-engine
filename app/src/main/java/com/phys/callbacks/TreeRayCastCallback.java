package com.phys.callbacks;

import com.phys.collision.RayCastInput;

public interface TreeRayCastCallback {

	public float raycastCallback( RayCastInput input, int nodeId);
}
