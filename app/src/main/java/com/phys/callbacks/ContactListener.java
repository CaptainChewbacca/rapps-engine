package com.phys.callbacks;

import com.phys.collision.Manifold;
import com.phys.dynamics.contacts.Contact;

public interface ContactListener {

	public void beginContact(Contact contact);

	public void endContact(Contact contact);

	public void preSolve(Contact contact, Manifold oldManifold);
	

	public void postSolve(Contact contact, ContactImpulse impulse);
}
