package com.phys.callbacks;

import com.phys.dynamics.Fixture;
import com.phys.dynamics.joints.Joint;


public interface DestructionListener {

	public void sayGoodbye(Joint joint);
	public void sayGoodbye(Fixture fixture);
}
