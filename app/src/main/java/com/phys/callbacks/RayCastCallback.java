package com.phys.callbacks;

import com.phys.common.Vec2;
import com.phys.dynamics.Fixture;


public interface RayCastCallback {


	public float reportFixture(Fixture fixture, Vec2 point, Vec2 normal, float fraction);
}
