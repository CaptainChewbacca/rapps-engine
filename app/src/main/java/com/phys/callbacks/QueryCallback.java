package com.phys.callbacks;

import com.phys.dynamics.Fixture;


public interface QueryCallback {

	public boolean reportFixture(Fixture fixture);
}
