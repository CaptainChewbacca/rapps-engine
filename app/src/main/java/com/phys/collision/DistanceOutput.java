
package com.phys.collision;

import com.phys.common.Vec2;

public class DistanceOutput {
	/** Closest point on shapeA */
	public final Vec2 pointA = new Vec2();
	
	/** Closest point on shapeB */
	public final Vec2 pointB = new Vec2();
	
	public float distance;
	
	/** number of gjk iterations used */
	public int iterations;
}
