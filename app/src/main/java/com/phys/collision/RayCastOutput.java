
package com.phys.collision;

import com.phys.common.Vec2;

public class RayCastOutput{
	public final Vec2 normal;
	public float fraction;

	public RayCastOutput(){
		normal = new Vec2();
		fraction = 0;
	}

	public void set(final RayCastOutput rco){
		normal.set(rco.normal);
		fraction = rco.fraction;
	}
};
