package com.phys.collision.broadphase;

import com.phys.callbacks.DebugDraw;
import com.phys.callbacks.TreeCallback;
import com.phys.callbacks.TreeRayCastCallback;
import com.phys.collision.AABB;
import com.phys.collision.RayCastInput;
import com.phys.common.Vec2;

public interface BroadPhaseStrategy {

  int createProxy(AABB aabb, Object userData);


  void destroyProxy(int proxyId);


  boolean moveProxy(int proxyId, AABB aabb, Vec2 displacement);

  Object getUserData(int proxyId);

  AABB getFatAABB(int proxyId);

  /**
   * Query an AABB for overlapping proxies. The callback class is called for each proxy that
   * overlaps the supplied AABB.
   * 
   * @param callback
   * @param araabbgAABB
   */
  void query(TreeCallback callback, AABB aabb);

  /**
   * Ray-cast against the proxies in the tree. This relies on the callback to perform a exact
   * ray-cast in the case were the proxy contains a shape. The callback also performs the any
   * collision filtering. This has performance roughly equal to k * log(n), where k is the number of
   * collisions and n is the number of proxies in the tree.
   * 
   * @param input the ray-cast input data. The ray extends from p1 to p1 + maxFraction * (p2 - p1).
   * @param callback a callback class that is called for each proxy that is hit by the ray.
   */
  void raycast(TreeRayCastCallback callback, RayCastInput input);

  /**
   * Compute the height of the tree.
   */
  int computeHeight();

  /**
   * Compute the height of the binary tree in O(N) time. Should not be called often.
   * 
   * @return
   */
  int getHeight();

  /**
   * Get the maximum balance of an node in the tree. The balance is the difference in height of the
   * two children of a node.
   * 
   * @return
   */
  int getMaxBalance();

  /**
   * Get the ratio of the sum of the node areas to the root area.
   * 
   * @return
   */
  float getAreaRatio();

  int getInsertionCount();

  void drawTree(DebugDraw draw);

}