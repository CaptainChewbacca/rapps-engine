
package com.phys.collision.shapes;

public enum ShapeType {
	CIRCLE, EDGE, POLYGON, CHAIN
}
