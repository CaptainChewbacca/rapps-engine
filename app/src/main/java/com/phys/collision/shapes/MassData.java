
package com.phys.collision.shapes;

import com.phys.common.Vec2;


public class MassData {
	/** The mass of the shape, usually in kilograms. */
	public float mass;
	/** The position of the shape's centroid relative to the shape's origin. */
	public final Vec2 center;
	/** The rotational inertia of the shape about the local origin. */
	public float I;
	
	/**
	 * Blank mass data
	 */
	public MassData() {
		mass = I = 0f;
		center = new Vec2();
	}
	
	/**
	 * Copies from the given mass data
	 * 
	 * @param md
	 *            mass data to copy from
	 */
	public MassData(MassData md) {
		mass = md.mass;
		I = md.I;
		center = md.center.clone();
	}
	
	public void set(MassData md) {
		mass = md.mass;
		I = md.I;
		center.set(md.center);
	}
	
	/** Return a copy of this object. */
	public MassData clone() {
		return new MassData(this);
	}
}
