
package com.phys.collision.shapes;

import com.phys.collision.AABB;
import com.phys.collision.RayCastInput;
import com.phys.collision.RayCastOutput;
import com.phys.common.Transform;
import com.phys.common.Vec2;


public abstract class Shape {

  public final ShapeType m_type;
  public float m_radius;

  public Shape(ShapeType type) {
    this.m_type = type;
  }

  /**
   * Get the type of this shape. You can use this to down cast to the concrete shape.
   * 
   * @return the shape type.
   */
  public ShapeType getType() {
    return m_type;
  }

  /**
   * The radius of the underlying shape. This can refer to different things depending on the shape
   * implementation
   * 
   * @return
   */
  public float getRadius() {
    return m_radius;
  }

  /**
   * Sets the radius of the underlying shape. This can refer to different things depending on the
   * implementation
   * 
   * @param radius
   */
  public void setRadius(float radius) {
    this.m_radius = radius;
  }

  /**
   * Get the number of child primitives
   * 
   * @return
   */
  public abstract int getChildCount();

  /**
   * Test a point for containment in this shape. This only works for convex shapes.
   * 
   * @param xf the shape world transform.
   * @param p a point in world coordinates.
   */
  public abstract boolean testPoint(final Transform xf, final Vec2 p);

  /**
   * Cast a ray against a child shape.
   * 
   * @param argOutput the ray-cast results.
   * @param argInput the ray-cast input parameters.
   * @param argTransform the transform to be applied to the shape.
   * @param argChildIndex the child shape index
   * @return if hit
   */
  public abstract boolean raycast(RayCastOutput output, RayCastInput input, Transform transform,
      int childIndex);


  /**
   * Given a transform, compute the associated axis aligned bounding box for a child shape.
   * 
   * @param argAabb returns the axis aligned box.
   * @param argXf the world transform of the shape.
   */
  public abstract void computeAABB(final AABB aabb, final Transform xf, int childIndex);

  /**
   * Compute the mass properties of this shape using its dimensions and density. The inertia tensor
   * is computed about the local origin.
   * 
   * @param massData returns the mass data for this shape.
   * @param density the density in kilograms per meter squared.
   */
  public abstract void computeMass(final MassData massData, final float density);

  /*
   * Compute the volume and centroid of this shape intersected with a half plane
   * 
   * @param normal the surface normal
   * 
   * @param offset the surface offset along normal
   * 
   * @param xf the shape transform
   * 
   * @param c returns the centroid
   * 
   * @return the total volume less than offset along normal
   * 
   * public abstract float computeSubmergedArea(Vec2 normal, float offset, Transform xf, Vec2 c);
   */

  public abstract Shape clone();
}
