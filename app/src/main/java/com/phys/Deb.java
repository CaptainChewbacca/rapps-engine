package com.phys;

import com.engine.Graphics.Camera;
import com.engine.Graphics.PrimDrawing;
import com.phys.callbacks.DebugDraw;
import com.phys.common.Color3f;
import com.phys.common.IViewportTransform;
import com.phys.common.Transform;
import com.phys.common.Vec2;

public class Deb extends DebugDraw {

	
	PrimDrawing pr;
	
	public Deb(IViewportTransform viewport) {
		super(viewport);
		
		pr = new PrimDrawing();
	}
	
	
	public void Begin(){ pr.Begin();}
	public void End(){ pr.End();}
	
	
	
	
	public void UpdateCam(Camera cam){
		pr.SetMatrix(cam.ViewProjectionMatrix());
	}
	

	@Override
	public void drawCircle(Vec2 arg0, float arg1, Color3f arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void drawPoint(Vec2 vec, float arg1, Color3f arg2) {
			  pr.Line(vec.x-1, vec.y-1, vec.x+1, vec.y+1);	
	}

	@Override
	public void drawSegment(Vec2 arg0, Vec2 arg1, Color3f arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void drawSolidCircle(Vec2 vec, float arg1, Vec2 vec1, Color3f arg3) {
		  pr.Circle(vec.x, vec.y, arg1);
	}

	@Override
	public void drawSolidPolygon(Vec2[] vec, int vol, Color3f arg2) {

		  for(int i =0; i<vol; i++){
			
			  pr.Line(vec[i].x, vec[i].y, vec[(i+1)%vol].x, vec[(i+1)%vol].y);
		  }	
	}

	@Override
	public void drawString(float arg0, float arg1, String arg2, Color3f arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void drawTransform(Transform arg0) {
		
		
	}

}
