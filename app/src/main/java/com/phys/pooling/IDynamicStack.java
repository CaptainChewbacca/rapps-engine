
package com.phys.pooling;

public interface IDynamicStack<E> {

	/**
	 * Pops an item off the stack
	 * @return
	 */
	public E pop();

	/**
	 * Pushes an item back on the stack
	 * @param argObject
	 */
	public void push(E argObject);

}
