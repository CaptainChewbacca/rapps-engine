
package com.phys.pooling;


public interface IOrderedStack<E> {

	public E pop();

	public E[] pop(int argNum);

	public void push(int argNum);

}
