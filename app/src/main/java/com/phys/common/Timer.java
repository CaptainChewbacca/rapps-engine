
package com.phys.common;


public class Timer {

  private long resetNanos;

  public Timer() {
    reset();
  }

  public void reset() {
    resetNanos = System.nanoTime();
  }

  public float getMilliseconds() {
    return (System.nanoTime() - resetNanos) / 1000 * 1f / 1000;
  }
}
