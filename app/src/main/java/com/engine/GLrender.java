package com.engine;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Color;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.PowerManager.WakeLock;
import android.provider.Settings;
import android.widget.TextView;

import com.engine.GLView.EglConfigInfo;
import com.engine.Info;
import com.engine.Graphics.TextureManager;
import com.engine.Sound.MainSound;
import com.game.MenuView;
import com.game.GameView;

import java.util.concurrent.TimeUnit;

public class GLRender implements GLSurfaceView.Renderer, OnLoadCompleteListener  {

	public static Info AppInfo;
    public static MainAct App;
    //public static MemoryManager MemMan;

    GameView Game;
    MenuView Menu;
    
    byte b = 0;
    
    int Width;
    int Height;
    float TouchX;
    float TouchY;

    long startTime;
    long startTimeM;
    public float Time = 0;
    public float DeltaTime = 0;
    long DeltaTimeOld = 0;
    int frames = 0;
    public int FPS = 0;
    
    public boolean Init = false;

    public MainSound MSound; 
    
    public GLRender(MainAct main) {
		App = main;


    	AppInfo = new Info();
    	AppInfo.SetContext(main.getApplicationContext());

    	
    	AppInfo.GetSettings().ToDef(1, 0);

    	
    	Game = new GameView(this);
    	Menu = new MenuView(this);
    	MSound = new MainSound(this);
    	
    	startTimeM = System.currentTimeMillis();
		startTime = System.nanoTime();

	}

    public void onDrawFrame(GL10 glUnused) { 	
    	Time = (System.currentTimeMillis() - startTimeM)/1000f;
        DeltaTime = (float)((System.nanoTime()-DeltaTimeOld));
        DeltaTime/=1000000000;
    	DeltaTimeOld = System.nanoTime();



    	for(int i = 0; i<9; i++){
    	      if(App.multiT[i].ID != -1){
    	    	  if(App.multiT[i].Down){
    	    		  onTouchDown(App.multiT[i].x, App.multiT[i].y, App.multiT[i].ID);
    	    		  App.multiT[i].Down=false;
    	    	  }
    	    	  else
    	    	  {
    	    	      onTouchDrag(App.multiT[i].x, App.multiT[i].y, App.multiT[i].ID);
    	    	  }
    	      }	  
    	      else
    	      {
    	    	  onTouchUp(i);
    	      }
    	      
    	}
    	

    	MSound.Update(b);
    	
    	if(b==1)
     	   Game.Render();
     	else
     	   Menu.Render();
    	
        frames++;
        if(System.nanoTime() - startTime >= 1000000000) {
        	FPS = frames;
            frames = 0;
            startTime = System.nanoTime();
        }

		App.setText("FPS: "+FPS+'\n'+"DeltaTime(ms): "+(int)(DeltaTime*1000));
    }
    
    
//Глючит многопоточный режим
    public void onTouchDown(float x, float y, int i)
    {
	    TouchX = App.multiT[i].x - Width/2;
	    TouchY = -App.multiT[i].y + Height/2;
    	if(b==1) Game.TouchDown(TouchX,TouchY,i);
    	else Menu.TouchDown(TouchX, TouchY, i);
    }
    
    public void onTouchUp(int i)
    {
    	if(b==1) Game.TouchUp(i);
    	else Menu.TouchUp(i);
    }
    
    public void onTouchDrag(float x, float y, int i)
    {
	    TouchX = App.multiT[i].x - Width/2;
	    TouchY = -App.multiT[i].y + Height/2;
    	if(b==1) Game.TouchDrag(TouchX,TouchY,i);
    	else Menu.TouchDrag(TouchX, TouchY, i);
    }
    


    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
    	Width=width;
    	Height=height;

    	if(!Init){
        	if(b==1){
                Game.Create(width, height);
            	//App.Service.inMenu(false);
        	}
           	else{
                Menu.Create(width, height);
            	//App.Service.inMenu(true);
           	}
    	    Init = true;
    	}
    	else
    	{
    		if(b==1)
    			Game.iPause=true;
    	}
    }
    

    
    
    
    public void ToGame(int mode){

    	b = 1;

    	MSound.Clear();
    	MSound.Start(this);
    	//TextureManager.DeleteAll();

    	Game.Create(Width, Height);
    	Menu = new MenuView(this);

    	//MSound.Start(this);
    	//App.Service.inMenu(false);
		GLES20.glReleaseShaderCompiler();
    }
    
    public void ToMenu(){

    	b = 0;
    	
    	MSound.Clear();
    	MSound.Start(this);
		//TextureManager.DeleteAll();
    	//MemoryManager.Delete();
    	
    	Menu.Create(Width, Height);
    	Game = new GameView(this);
    	
    	//App.Service.inMenu(true);
		GLES20.glReleaseShaderCompiler();
    }

    @Override
	public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
    	MSound.GetID(sampleId).Load = true;
    }

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		TextureManager.Init(40);
		TextureManager.LoadDirectory("image");
	}


}