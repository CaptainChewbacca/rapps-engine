//V 1.12


package com.engine.Graphics;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import com.engine.Graphics.Shader;
import com.engine.Graphics.Drawing;

import android.opengl.GLES20;


public class SpriteDrawing extends Drawing {
	private FloatBuffer mTriangleVertices;
    
	public SpriteDrawing()
	{
		
        String vertexShaderCode=

		"uniform mat4 u_MVP;"+
        "uniform mat4 u_modelView;"+ 
        "attribute vec3 a_vertex;"+
        "attribute vec2 a_texcoord;"+
         
        
        "varying vec3 v_vertex;"+
        "varying vec2 v_texcoord0;"+

        "void main() {"+
             "v_vertex=a_vertex;"+

             "v_texcoord0=a_texcoord;"+
             //"v_texcoord0.t=a_texcoord.z;"+
             "gl_Position = u_MVP * u_modelView * vec4(a_vertex,1.0);"+
        "}"; 

        String fragmentShaderCode=
        "precision lowp float;"+

        "uniform vec4 u_color;"+  
        "uniform sampler2D u_texture0;"+
        "varying vec2 v_texcoord0;"+
        
        "void main() {"+

             "vec4 textureColor0=texture2D(u_texture0, v_texcoord0);"+
             
             "gl_FragColor = textureColor0*u_color;"+
        "}"; 
        
		
		
		Shad = new Shader(vertexShaderCode, fragmentShaderCode);
		
        IDvertex = GLES20.glGetAttribLocation(Shad.Program, "a_vertex");
        IDmodelview = GLES20.glGetUniformLocation(Shad.Program, "u_modelView");
        IDtexcoord = GLES20.glGetAttribLocation(Shad.Program, "a_texcoord");
        IDcolor = GLES20.glGetUniformLocation(Shad.Program, "u_color");
        
        Mat = new float[16];
        
        

	    float quadv[] =
	           { 0, 0,
	             0, 1,
	             1, 1,
	             1, 0};	
			
	    mTriangleVertices = ByteBuffer.allocateDirect(quadv.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
	    mTriangleVertices.put(quadv).position(0);
	}
	
	public void FlipY(boolean f){
		if(f){
		    float quadv[] =
		           { 0, 1,
		             0, 0,
		             1, 0,
		             1, 1};	
				
		    mTriangleVertices = ByteBuffer.allocateDirect(quadv.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		    mTriangleVertices.put(quadv).position(0);	
		}
		else
		{
		    float quadv[] =
		           { 0, 0,
		             0, 1,
		             1, 1,
		             1, 0};	
				
		    mTriangleVertices = ByteBuffer.allocateDirect(quadv.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		    mTriangleVertices.put(quadv).position(0);
		}
	}
	
	
	
	public void BeginSprite(){
		   mTriangleVertices.position(0);
		   GLES20.glVertexAttribPointer(IDtexcoord, 2, GLES20.GL_FLOAT, false, 8, mTriangleVertices);
		   GLES20.glEnableVertexAttribArray(IDtexcoord);
		   GLES20.glEnableVertexAttribArray(IDvertex);


		   SetColor(1, 1, 1, 1);
	}
	
	public void SetColor(float r, float g, float b, float a){
		GLES20.glUniform4f(IDcolor, r,g,b,a);
	}
	
}
