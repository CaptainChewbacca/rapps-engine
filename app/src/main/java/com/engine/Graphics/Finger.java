package com.engine.Graphics;

public class Finger {
    public int ID;		
	
    public float x;
    public float y;
    
    
    public boolean Down;
    
    public Finger(int id, float x, float y){
        ID = id;
        this.x = x;
        this.y = y;
        
        Down = true;
    }
    
    public void Set(int id, float x, float y){
        this.x = x;
        this.y = y;
        ID = id;
    }
}
