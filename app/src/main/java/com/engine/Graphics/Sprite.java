
package com.engine.Graphics;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import com.engine.Object2D;

import android.graphics.Bitmap;
import android.opengl.GLES20;


public class Sprite extends Object2D {
    public FloatBuffer mTriangleVertices;

    public int Texture;
    
    public float Width;
    public float Height;


	public Sprite(String tex)
	{
        Texture = TextureManager.Get(tex, 0);
        float h = 0.5f;
        float w = 0.5f*TextureManager.GetWidth(Texture)/TextureManager.GetHeight(Texture);
        Width = w*2;
        Height = h*2;
        Create(w, h);
	}

	public Sprite(int w1, int h1)
	{
        float h = 0.5f;
        float w = 0.5f*w1/h1; 
        Width = w/h;
        Height = 1;    
        Create(w, h);
	}

	
	private void Create(float w, float h){
        float quadv[] =
            { -w,  h,
              -w, -h,
               w, -h,
               w,  h};
        mTriangleVertices = ByteBuffer.allocateDirect(quadv.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTriangleVertices.put(quadv).position(0);      

	}

	
	public void SetFilter(int m)
	{
		TextureManager.SetFilter(m, Texture);
	}
	
	
	
	public float GetWidth()
	{
		return Width;
	}
	
	public float GetHeight()
	{
		return Height;
	}
	
	
	public void Draw(Drawing dr)
	{
        TextureManager.Use(Texture, 0);
        GLES20.glVertexAttribPointer(dr.IDvertex, 2, GLES20.GL_FLOAT, false, 8, mTriangleVertices);
        GLES20.glUniformMatrix4fv(dr.IDmodelview, 1, false, GetFinalMatrix(), 0);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 4);
	}
		
	public void Draw(Drawing dr, int tex)
	{
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tex);
        GLES20.glVertexAttribPointer(dr.IDvertex, 2, GLES20.GL_FLOAT, false, 8, mTriangleVertices);
        GLES20.glUniformMatrix4fv(dr.IDmodelview, 1, false, GetFinalMatrix(), 0);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 4);  
	}

    public void DrawWT(Drawing dr)
    {
        GLES20.glVertexAttribPointer(dr.IDvertex, 2, GLES20.GL_FLOAT, false, 8, mTriangleVertices);
        GLES20.glUniformMatrix4fv(dr.IDmodelview, 1, false, GetFinalMatrix(), 0);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 4);
    }

}
