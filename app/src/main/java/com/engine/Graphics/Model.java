package com.engine.Graphics;

import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;



import com.engine.GLRender;
import com.engine.Object3D;

import android.opengl.GLES20;


public class Model extends Object3D {


    private FloatBuffer mTriangleVertices;
    private ShortBuffer mTriangleIndex;
    private FloatBuffer mTriangleTexCoord;
    
    private int Texture;
    
    private float Width;
    private float Height;
    
    

    
    ArrayList<Float> vertex;
    ArrayList<Float> coords;

    
    ArrayList<Float> vertex2;
    ArrayList<Float> vertex3;
    ArrayList<Short> index1;

    ArrayList<String> string;
    ArrayList<String> string2;
    
    boolean g1, g2, g3;
    
    
    public String COUNT = "";
    
    
	public Model(String tex, String model)
	{

		Texture = TextureManager.Get(tex, 0);
        float h = 0.5f;
        float w = 0.5f*TextureManager.GetWidth(Texture)/TextureManager.GetHeight(Texture);
        
        
        Width = w*2;
        Height = h*2;


      
        vertex = new ArrayList<Float>();
        coords = new ArrayList<Float>();
        
        
        vertex3 = new ArrayList<Float>();
        
        vertex2 = new ArrayList<Float>();
        index1 = new ArrayList<Short>();
        string = new ArrayList<String>();
        string2 = new ArrayList<String>();
        
     
        
		InputStream is;
		try {
			is = GLRender.AppInfo.GetContext().getAssets().open(model);
			int size = is.available();
			
			
			byte[] buffer = new byte[size];
			
			is.read(buffer);
			is.close();
			String text = new String(buffer);
			
			ArrayList<String> str = getLines(text);

		
			
			for(int i = 0; i<str.size(); i++){
				if(str.get(i).indexOf("vt") == 0){
					StringToCoords(str.get(i));
				}
				else if(str.get(i).indexOf("v") == 0){				
					StringToVertex(str.get(i));
				}
				
				if(str.get(i).indexOf("f") == 0){
					StringToIndex(str.get(i));
				}
			}
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		


		
		for(int i =0; i<string.size(); i++){
			float v1 = vertex.get(Integer.valueOf(string.get(i).split("/")[0])*3-3);
			float v2 = vertex.get(Integer.valueOf(string.get(i).split("/")[0])*3-2);
			float v3 = vertex.get(Integer.valueOf(string.get(i).split("/")[0])*3-1);
			
			float c1 = coords.get(Integer.valueOf(string.get(i).split("/")[1])*2-2);
			float c2 = coords.get(Integer.valueOf(string.get(i).split("/")[1])*2-1);

			
			vertex2.add(v1);
			vertex2.add(v2);
			vertex2.add(v3);
			vertex3.add(c1);
			vertex3.add(c2);
			
			//Normal \/
			/*float x = (vertex.get((Integer.valueOf(string.get(i).split("/")[0])*3-3))-vertex.get((Integer.valueOf(string.get((i+1)%string.size()).split("/")[0])*3-3)));
			float y = (vertex.get((Integer.valueOf(string.get(i).split("/")[0])*3-2))-vertex.get((Integer.valueOf(string.get((i+1)%string.size()).split("/")[0])*3-2)));
			float z = (vertex.get((Integer.valueOf(string.get(i).split("/")[0])*3-1))-vertex.get((Integer.valueOf(string.get((i+1)%string.size()).split("/")[0])*3-1)));
			
			float len = (float)Math.sqrt(x*x+y*y+z*z);
			x/=len;
			y/=len;
			z/=len;*/   

		}

		
	
		for(int i = 0; i<string2.size(); i++){
			
			for(int g = 0; g<string.size(); g++){
				if(string2.get(i).equals(string.get(g))){
					index1.add((short) g);
				}
			}
			
		}
		
		
		float[] v = new float[vertex2.size()];
		for (int i = 0; i < vertex2.size(); i++) {
		    Float f = vertex2.get(i);
		    v[i] = f/2; // Or whatever default you want.		    
		}
		
		
		
        mTriangleVertices = ByteBuffer.allocateDirect(v.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTriangleVertices.put(v).position(0);

		short[] in = new short[index1.size()];
		for (int i = 0; i < index1.size(); i++) {
		    Short f = index1.get(i);
		    in[i] = (short) (f);
		} 

			
		
        
        mTriangleIndex = ByteBuffer.allocateDirect(in.length * 4).order(ByteOrder.nativeOrder()).asShortBuffer();
        mTriangleIndex.put(in).position(0);
        
        
		float[] tc = new float[vertex3.size()];
		for (int i = 0; i < vertex3.size(); i++) {
		    Float f = vertex3.get(i);
		    tc[i] = f; // Or whatever default you want.		    
		}
		
        mTriangleTexCoord = ByteBuffer.allocateDirect(tc.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTriangleTexCoord.put(tc).position(0);

	}

	
	public void StringToVertex(String str){
		int r1 = str.indexOf(" ");
		int r2 = str.indexOf(" ", r1+1);
		int r3 = str.indexOf(" ", r2+1);

		vertex.add(Float.valueOf(str.substring(r1+1, r2)));
		vertex.add(Float.valueOf(str.substring(r2+1, r3)));
		vertex.add(Float.valueOf(str.substring(r3+1)));
	}
	
	
	public void StringToCoords(String str){
		int r1 = str.indexOf(" ");
		int r2 = str.indexOf(" ", r1+1);

		coords.add(Float.valueOf(str.substring(r1+1, r2)));
		coords.add(Float.valueOf(str.substring(r2+1)));
	}
	
	
	public void StringToIndex(String str){
	   if(str.indexOf("/") != -1){
			int r1 = str.indexOf(" ");
			int r2 = str.indexOf("/", r1+1);
			int r3 = str.indexOf(" ", r2+1);
			int r4 = str.indexOf("/", r3+1);
			int r5 = str.indexOf(" ", r4+1);

		    g1 = true;
		    g2 = true;
		    g3 = true;
		    
	
		    for(int i = 0; i<string.size(); i++){
		    	if(string.get(i).equals(str.substring(r1+1, r3))){
		    		g1 = false;
		    	}
		    	if(string.get(i).equals(str.substring(r3+1, r5))){
		    		g2 = false;
		    	}
		    	if(string.get(i).equals(str.substring(r5+1))){
		    		g3 = false;
		    	}
		    }
		    
		    if(g1 == true){ string.add(str.substring(r1+1, r3));}
		    if(g2 == true){ string.add(str.substring(r3+1, r5));}
		    if(g3 == true){ string.add(str.substring(r5+1));}
		    
		    string2.add(str.substring(r1+1, r3));
		    string2.add(str.substring(r3+1, r5));
		    string2.add(str.substring(r5+1));

	   }

	}
	

    public ArrayList<String> getLines(String s) throws IOException {
        StringReader sr = new StringReader(s);
        LineNumberReader lnr = new LineNumberReader(sr);
        ArrayList<String> rez = new ArrayList<String>();
        String str = lnr.readLine();

        while (str != null) {
            rez.add(str);
            str = lnr.readLine();
        }
        return rez;
    }
	
    

	
	public float GetWidth()
	{
		return Width;
	}
	
	public float GetHeight()
	{
		return Height;
	}
	

	
	public void Draw(Drawing dr)
	{
		TextureManager.Use(Texture, 0);
		
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER,GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER,GLES20.GL_LINEAR);
              		
        mTriangleVertices.position(0);
        GLES20.glVertexAttribPointer(dr.IDvertex, 3, GLES20.GL_FLOAT, false, 12, mTriangleVertices);
        GLES20.glEnableVertexAttribArray(dr.IDvertex);
        
        mTriangleTexCoord.position(0);
        GLES20.glVertexAttribPointer(dr.IDtexcoord, 2, GLES20.GL_FLOAT, false, 8, mTriangleTexCoord);
        GLES20.glEnableVertexAttribArray(dr.IDtexcoord);      

        GLES20.glUniformMatrix4fv(dr.IDmodelview, 1, false, GetFinalMatrix(), 0);
        
        mTriangleIndex.position(0);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, mTriangleIndex.capacity(), GLES20.GL_UNSIGNED_SHORT, mTriangleIndex);
	}
	

	

   
}
























/*        ArrayList<Float> tempVertices = new ArrayList<Float>();
        //ArrayList<Float> tempNormals = new ArrayList<Float>();
        ArrayList<Short> vertexIndices = new ArrayList<Short>();
        //ArrayList<Short> normalIndices = new ArrayList<Short>();

        try {
            AssetManager manager = GLRender.AppInfo.getContext().getAssets();
            BufferedReader reader = new BufferedReader(new InputStreamReader(manager.open("cube.obj")));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("v")) {
                    tempVertices.add(Float.valueOf(line.split(" ")[1])); //vx
                    tempVertices.add(Float.valueOf(line.split(" ")[2])); //vy
                    tempVertices.add(Float.valueOf(line.split(" ")[3])); //vz
                }
                //              else if (line.startsWith("vn")) {
                //                  tempNormals.add(Float.valueOf(line.split(" ")[1])); //nx
                //                  tempNormals.add(Float.valueOf(line.split(" ")[2])); //ny
                //                  tempNormals.add(Float.valueOf(line.split(" ")[3])); //nz
                //              }
                else if (line.startsWith("f")) {

                    /*
                    vertexIndices.add(Short.valueOf(tokens[1].split("/")[0])); //first point of a face
                    vertexIndices.add(Short.valueOf(tokens[2].split("/")[0])); //second point
                    vertexIndices.add(Short.valueOf(tokens[3].split("/")[0])); //third point
                    normalIndices.add(Short.valueOf(tokens[1].split("/")[2])); //first normal
                    normalIndices.add(Short.valueOf(tokens[2].split("/")[2])); //second normal
                    normalIndices.add(Short.valueOf(tokens[3].split("/")[2])); //third
                     */
                    //                  for (int i = 1; i <= 3; i++) {
                    //                      //String[] s = tokens[i].split("/");
                    //                      vertexIndices.add(Short.valueOf());
                    //                      //normalIndices.add(Short.valueOf(s[2]));
                    //                  }

                    /*vertexIndices.add(Short.valueOf(line.split(" ")[1]));
                    vertexIndices.add(Short.valueOf(line.split(" ")[2]));
                    vertexIndices.add(Short.valueOf(line.split(" ")[3]));
                }
            }

            float[] vertices = new float[tempVertices.size()];
            for (int i = 0; i < tempVertices.size(); i++) {
                Float f = tempVertices.get(i);
                vertices[i] = (f != null ? f : Float.NaN);
            }

            short[] indices = new short[vertexIndices.size()];
            for (int i = 0; i < vertexIndices.size(); i++) {
                Short s = vertexIndices.get(i);
                indices[i] = (s != null ? s : 1);
            }

            
            
            mTriangleVertices = ByteBuffer.allocateDirect(vertices.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
            mTriangleVertices.put(vertices).position(0);

            mTriangleIndex = ByteBuffer.allocateDirect(indices.length * 2).order(ByteOrder.nativeOrder()).asShortBuffer();
            mTriangleIndex.put(indices).position(0);


            COUNT = vertices.toString();
        }
        catch (Exception e) {
        }
        
        */
