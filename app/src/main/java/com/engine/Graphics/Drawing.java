package com.engine.Graphics;

import android.opengl.GLES20;

public class Drawing {
	
	public Shader Shad; 
	public float[] Mat;
	
    
    public int IDvertex;
    public int IDmodelview;
    public int IDtexcoord;
    public int IDnormal;
    public int IDcolor;
    
    public void SetSimpleShader(Shader sh) { Shad = sh; }
    public void SetMatrix(float[] pr) { Mat = pr; }
    
    public void Begin(float[] pr) {
    	Mat = pr;
		Shad.UseProgram();
		Shad.SetMatrix(Mat);
	}
    public void Begin() {
		Shad.UseProgram();
		Shad.SetMatrix(Mat);
	}


    public void End() { GLES20.glUseProgram(0); }
	Shader GetShader() { return Shad; }
	
	public void SetShader(Shader sh) {
		Shad = sh;
	}

}
