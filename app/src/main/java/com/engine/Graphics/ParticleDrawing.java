package com.engine.Graphics;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import com.engine.Graphics.Shader;
import com.engine.JNImanager;

import android.opengl.GLES20;



public class ParticleDrawing extends Drawing {

	public ParticleDrawing()
	{
		String vertexShaderCode=
				"uniform mat4 u_MVP;"+
				"uniform float u_CPIU;"+
				"attribute vec4 a_position;"+
				"attribute vec4 a_color;"+

				"varying vec4 v_color;" +

				"void main() {"+
						"v_color=a_color;"+
						"gl_Position =  u_MVP * vec4(a_position.xyz, 1.0);"+
						"gl_PointSize = u_CPIU * a_position.w;"+
				"}";

		String fragmentShaderCode=
				"precision lowp float;"+

				"uniform sampler2D u_texture0;"+
				"varying vec4 v_color;" +

				"void main() {"+
						"gl_FragColor = texture2D(u_texture0, gl_PointCoord)*v_color;"+
				"}";



		Shad = new Shader(vertexShaderCode, fragmentShaderCode);


		IDnormal = GLES20.glGetUniformLocation(Shad.Program, "u_CPIU"); //не нормали, просто переменная из дравинга
		IDvertex = GLES20.glGetAttribLocation(Shad.Program, "a_position");
        IDcolor = GLES20.glGetAttribLocation(Shad.Program, "a_color");

        Mat = new float[16];


		JNImanager.Init(IDvertex, IDcolor);
	}

	public void SetCamPixelInUnit(float cup){
		GLES20.glUniform1f(IDnormal, cup);
	}
}
