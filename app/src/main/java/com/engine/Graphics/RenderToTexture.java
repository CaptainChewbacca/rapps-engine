package com.engine.Graphics;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import android.opengl.GLES20;

public class RenderToTexture {

    int[] fb, renderTex; 
    int texW = 100; 
    int texH = 100; 
    IntBuffer texBuffer;

    
	public RenderToTexture(int w, int h){
		texW = w*2;
		texH = h*2;
		
		fb = new int[1];
		renderTex = new int[1];
		
		GLES20.glGenFramebuffers(1, fb, 0);
		GLES20.glGenTextures(1, renderTex, 0);


		int[] buf = new int[texW * texH];
		texBuffer = ByteBuffer.allocateDirect(buf.length * 4).order(ByteOrder.nativeOrder()).asIntBuffer();

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, renderTex[0]);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_WRAP_S,GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_WRAP_T,GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MIN_FILTER,GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MAG_FILTER,GLES20.GL_LINEAR);

		GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGB, texW, texH, 0, GLES20.GL_RGB, GLES20.GL_UNSIGNED_BYTE, texBuffer);

	}
	
	
	
	public void Delete(){
		GLES20.glDeleteTextures(1, renderTex, 0);
	}
	
	
	
	public int[] GetTexture(){
		return renderTex;
	}
	
	
	public void Begin(){
    	GLES20.glViewport(0, 0, this.texW, this.texH);

    	GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, fb[0]);
    	GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, renderTex[0], 0);

    	GLES20.glClearColor(.91f, .91f, .91f, .91f);
    	GLES20.glClear( GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
	}
	
	public void Begin(int x, int y){
    	GLES20.glViewport(x, y, this.texW, this.texH);

    	GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, fb[0]);
    	GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, renderTex[0], 0);

    	GLES20.glClearColor(.0f, .0f, .0f, .0f);
    	GLES20.glClear( GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
	}
	
	public void End(){
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
	}
	
	
}
