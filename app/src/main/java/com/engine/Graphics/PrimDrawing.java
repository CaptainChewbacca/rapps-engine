package com.engine.Graphics;


import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;



import android.opengl.GLES20;
import android.opengl.Matrix;


public class PrimDrawing extends Drawing {


	public PrimDrawing()
	{
		
        String vertexShaderCode=
        "uniform mat4 u_MVP;"+
        "uniform mat4 u_modelView;"+ 
        "attribute vec3 a_vertex;"+

        "varying vec3 v_vertex;"+

        "void main() {"+
             "v_vertex=a_vertex;"+

             "gl_Position = u_MVP * vec4(a_vertex,1.0);"+
             "gl_PointSize = 10.0;"+
        "}"; 

        String fragmentShaderCode=
        "precision lowp float;"+
        "uniform vec3 color;"+ 

        "void main() {"+      
             "gl_FragColor = vec4(color,1.0);"+
        "}"; 
        
		
		
		Shad=new Shader(vertexShaderCode, fragmentShaderCode);

		IDcolor = GLES20.glGetUniformLocation(Shad.Program, "color");
		IDvertex = GLES20.glGetAttribLocation(Shad.Program, "a_vertex");

		Mat = new float[16];

	}
	
	
	public void SetColor(float r, float g, float b){
		GLES20.glUniform3f(IDcolor, r,g,b);
	}
	

	

	
	public void Line(float x1, float y1, float x2, float y2){
		
        float vertexArray [] = {x1,y1, 1f, 1f, x2, y2};

        
        FloatBuffer mTriangleVertices = ByteBuffer.allocateDirect(vertexArray.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTriangleVertices.put(vertexArray).position(0);
        
        mTriangleVertices.position(0);
        GLES20.glVertexAttribPointer(IDvertex, 2, GLES20.GL_FLOAT, false, 16, mTriangleVertices);
        GLES20.glEnableVertexAttribArray(IDvertex);
        mTriangleVertices.position(0);

        GLES20.glDrawArrays(GLES20.GL_LINES, 0, 2);  	
	}
	
	
	
	public void Point(float x1, float y1){
		
        float vertexArray [] = {x1,y1};

        
        FloatBuffer mTriangleVertices = ByteBuffer.allocateDirect(vertexArray.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTriangleVertices.put(vertexArray).position(0);
        
        mTriangleVertices.position(0);
        GLES20.glVertexAttribPointer(IDvertex, 2, GLES20.GL_FLOAT, false, 16, mTriangleVertices);
        GLES20.glEnableVertexAttribArray(IDvertex);
        mTriangleVertices.position(0);

        GLES20.glDrawArrays(GLES20.GL_POINTS, 0, 1);  	
	}


	public void Circle(float x, float y, float r){
        int vertexCount = 36;
        float radius = r;
        float center_x = x;
        float center_y = y;
		
        //create a buffer for vertex data
        float buffer[] = new float[vertexCount*2]; // (x,y) for each vertex
        int idx = 0;

        //center vertex for triangle fan
        buffer[idx++] = center_x;
        buffer[idx++] = center_y;

        //outer vertices of the circle
        int outerVertexCount = vertexCount-1;

        
        for (int i = 0; i < outerVertexCount; ++i){
            float percent = (i / (float) (outerVertexCount-2));
            float rad = (float) (percent * 2*Math.PI);

            //vertex position
            float outer_x = center_x + radius * (float) (Math.cos(rad));
            float outer_y = center_y + radius * (float) (Math.sin(rad));

            buffer[idx++] = outer_x;    
            buffer[idx++] = outer_y; 
        }
        
        FloatBuffer mTriangleVertices = ByteBuffer.allocateDirect(buffer.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTriangleVertices.put(buffer).position(0);
        
        mTriangleVertices.position(0);
        GLES20.glVertexAttribPointer(IDvertex, 2, GLES20.GL_FLOAT, false, 8, mTriangleVertices);
        GLES20.glEnableVertexAttribArray(IDvertex);
        mTriangleVertices.position(0);

        GLES20.glDrawArrays(GLES20.GL_LINES, 0, outerVertexCount);
	}

}
