package com.engine.Graphics;


import android.opengl.Matrix;

public class Camera {


    public float[] ViewMatrix;
    public float[] ProjectionMatrix;
    public float[] ViewProjectionMatrix;


    
    public float Width = 0;
    public float Height = 0;

    public float X = 0;
    public float Y = 0;
    public float Z = 1;
    public float A = 0;
    
    private float Angle = 0;
    
    public float Zoom = 1;
	
	public Camera(int width, int height, float zoom)
	{
		ViewMatrix=new float[16];
        ProjectionMatrix=new float[16];
		ViewProjectionMatrix=new float[16];

        Matrix.setLookAtM(ViewMatrix, 0,
        		0, 0, 1, 
        		0, 0, 0, 
        		0, 1, 0);



		Zoom = zoom;
		Width = (float)width/height;
		Height = 1;

        Matrix.orthoM(ProjectionMatrix, 0, -Width*zoom, Width*zoom, -Height*zoom, Height*zoom, 0.0f, 10.0f);
        Matrix.multiplyMM(ViewProjectionMatrix, 0, ProjectionMatrix, 0, ViewMatrix, 0);
	}
	
	
	
	
	public float GetWidthZoom(){
		return Width*Zoom;
	}
	public float GetHeightZoom(){
		return Height*Zoom;
	}
	

	
	public void SetZoom(float zoom)
	{
        Matrix.orthoM(ProjectionMatrix, 0, -Width*zoom, Width*zoom, -Height*zoom, Height*zoom, -100.0f, 100.0f);
        Matrix.multiplyMM(ViewProjectionMatrix, 0, ProjectionMatrix, 0, ViewMatrix, 0);

		Zoom = zoom;
	}
	
	public void Update()
	{
		Matrix.rotateM(ProjectionMatrix,0,Angle,0,0,1.0f);
        Matrix.multiplyMM(ViewProjectionMatrix, 0, ProjectionMatrix, 0, ViewMatrix, 0);
	}
	
	
	
	public void SetX(float x)
	{
		   X = x;
	       Matrix.setLookAtM(ViewMatrix, 0,
	        		x, Y, Z, 
	        		x, Y, 0, 
	        		0, 1, 0); 
	}
	
	public void SetY(float y)
	{
		   Y = y;
	       Matrix.setLookAtM(ViewMatrix, 0,
	        		X, y, Z, 
	        		X, y, 0, 
	        		0, 1, 0);
	           
	}
	public void SetZ(float z)
	{
		   Z=z;
	       Matrix.setLookAtM(ViewMatrix, 0,
	        		X, Y, z, 
	        		X, Y, 0, 
	        		0, 1, 0);
	}
	
	
	public void SetRotation(float angle){
		Angle = -angle*0.5f;
		A = angle;
		Matrix.rotateM(ProjectionMatrix,0,Angle,0,0,1.0f);
	}


	public float[] ViewProjectionMatrix()
	{
		return ViewProjectionMatrix;
	}


}
