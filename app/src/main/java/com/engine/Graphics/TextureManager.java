package com.engine.Graphics;

import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;

import com.engine.Functions;
import com.engine.GLRender;

/**
 * Created by acer on 28.06.2016.
 */
public class TextureManager {

    public final static int LINEAR = 0;
    public final static int NEAREST = 1;
    public final static int MIPMAP = 2;


    static int Tex[];
    static String TexPath[];
    static int TexN;


    private static int TexW[], TexH[];
    private static int OldUseTex;


    public static int TexOff;

    private static Bitmap bitmap;

    public static void Init(int texn){
        TexN = texn;
        Tex = new int[TexN]; TexW = new int[TexN]; TexH = new int[TexN];

        TexPath = new String[TexN];
        TexOff = 0;
        OldUseTex = 0;
        GLES20.glGenTextures(TexN, Tex, 0);
    }

    public static int GetWidth(int t){
        return TexW[t];
    }

    public static int GetHeight(int t){
        return TexH[t];
    }

    public static int Get(String b, int m){
        for(int i = 0; i<TexN; i++){
            if(b.compareTo(TexPath[i])==0) return i;
        }
        return -1;
    }

    public static void LoadDirectory(String p){
        String[] fileNames = GLRender.App.GetAssetPath(p);
        for(String name:fileNames){
            Create(p+"/"+name, 0);
        }
        //GLRender.App.getResources().getIdentifier()
    }

    public static void Create(String b, int m){
        bitmap = Functions.BitmapLoad1(b);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, Tex[TexOff]);
        TexPath[TexOff] = b.substring(b.lastIndexOf("/")+1, b.lastIndexOf("."));

        SetFilter(m, TexOff);
        SetTextureWrap(0, TexOff);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
        bitmap.recycle();


        TexW[TexOff] = bitmap.getWidth();
        TexH[TexOff] = bitmap.getHeight();

        TexOff++;
    }




    public static void Use(int t, int s){
        if(OldUseTex != t) {
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + s);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, Tex[t]);
            OldUseTex = t;
        }
    }


    public static void DeleteAll(){
        GLES20.glDeleteTextures(TexN, Tex, 0);
    }


    public static void SetFilter(int m, int t)
    {
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, Tex[t]);
        if(m == LINEAR)
        {
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MIN_FILTER,GLES20.GL_LINEAR);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MAG_FILTER,GLES20.GL_LINEAR);
        }else if(m == NEAREST)
        {
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MIN_FILTER,GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MAG_FILTER,GLES20.GL_NEAREST);
        }else if(m == MIPMAP)
        {
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MIN_FILTER,GLES20.GL_LINEAR_MIPMAP_LINEAR);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MAG_FILTER,GLES20.GL_LINEAR);
            GLES20.glGenerateMipmap(GLES20.GL_TEXTURE_2D);
        }

    }

    public static void SetTextureWrap(int m, int t)
    {
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, Tex[t]);
        if(m==0)
        {
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_WRAP_S,GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_WRAP_T,GLES20.GL_CLAMP_TO_EDGE);
        }else
        {
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_WRAP_S,GLES20.GL_REPEAT);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_WRAP_T,GLES20.GL_REPEAT);
        }

    }
}
