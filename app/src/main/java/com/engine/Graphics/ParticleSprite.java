package com.engine.Graphics;

import android.opengl.GLES20;

import com.engine.JNImanager;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;


/*

class VecP2{
	float X, Y;
	VecP2(){
		X=0;Y=0;
	}
	VecP2(float x, float y){
		X=x;Y=y;
	}	
	
	void set(float x, float y){
		X=x;Y=y;
	}	
}

class Particle{
	VecP2 Pos;
	
	VecP2 AngM;
	VecP2 Ang;
	boolean Active;
	
	float Vel;
	VecP2 VelM;
	float Size;
	VecP2 SizeM;
	
	float r,g,b,t;
	VecP2 rM,gM,bM,tM;
	
	
	Particle(){
		Active = false;
		Pos = new VecP2();
		Ang = new VecP2();
		
		AngM = new VecP2();
		VelM = new VecP2();
		SizeM = new VecP2();
		
		rM = new VecP2();
		gM = new VecP2();
		bM = new VecP2();
		tM = new VecP2();
	}
	

	float LUpdate = -1;
	float Tstart = 0;
	float Tlife = 1.5f;
	
}


class LineGraph{
	float Min, Min1, Min2, Max, Max1, Max2;
	int N;
	VecP2[] Value;
	
	LineGraph(float min, float max, int n, VecP2[] v){
		N = n;
		Min1 = min; Max1 = max;
		Min2 = min; Max2 = max;
		Value = v;
	}
	
	LineGraph(float min1, float min2, float max1, float max2, int n, VecP2[] v){
		N = n;
		Min1 = min1;
		Min2 = min2;
		Max1 = max1;
		Max2 = max2;
		
		Min = min1+(min2-min1)*QuickMath.Random1(); Max = max1+(max2-max1)*QuickMath.Random1();
		Value = v;
	}
	
	void NewMinMax(){
		Min = Min1+(Min2-Min1)*QuickMath.Random1();
		Max = Max1+(Max2-Max1)*QuickMath.Random1();
	}
	
	float getValue(float x){
		NewMinMax();
		
		float yout = 0;
		
		for(int i = 0; i<N; ++i){
			if(Value[i].X<=x){
				if(i+1 != N){
					if(Value[i+1].X>=x){
						float xn = x-Value[i].X;
						float xo = Value[i+1].X-Value[i].X;
						xn/=xo;
						yout = Min+(Max-Min)*(Value[i].Y+(Value[i+1].Y-Value[i].Y)*xn);
					}
				}else{
					yout = Min+(Max-Min)*Value[i].Y;
				}
			}
		}

		return yout;
	}
	
	float getValue(float x, float min, float max){
		Min = min;
		Max = max;
		
		float yout = 0;
		
		for(int i = 0; i<N; ++i){
			if(Value[i].X<=x){
				if(i+1 != N){
					if(Value[i+1].X>=x){
						float xn = x-Value[i].X;
						float xo = Value[i+1].X-Value[i].X;
						xn/=xo;
						yout = Min+(Max-Min)*(Value[i].Y+(Value[i+1].Y-Value[i].Y)*xn);
					}
				}else{
					yout = Min+(Max-Min)*Value[i].Y;
				}
			}
		}

		return yout;
	}
	
	
}

*/



public class ParticleSprite {

    
    private int Texture;
    
    float X, Y;

    private float Width;
    private float Height;


    int Max;
    int ID;

    
    public ParticleSprite(String tex, int m, float t)
	{
        Texture = TextureManager.Get(tex, 0);

        Max = 1;

        ID = JNImanager.AddParticleSprite(m, t);
    }


	
	public void Draw(ParticleDrawing dr, float time, float dt)
	{
        TextureManager.Use(Texture, 0);

        JNImanager.Update(time, dt, ID);
	}
	
	


   //////////////////////////////////////// 
	

    public void SetPosition(float x, float y){
        X = x;
        Y = y;
    }


	public void Delete(){
        JNImanager.DeleteParticleSprite(ID);
    }


}