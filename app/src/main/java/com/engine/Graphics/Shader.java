package com.engine.Graphics;

import java.nio.FloatBuffer;

import android.opengl.GLES20;

/*
 * GLES20.glReleaseShaderCompiler();
 * нужная команда, просто ARC с ней не работает
*/

public class Shader {

	public int Program;

	public int VertexShader;
	public int FragmentShader;
	public int IDmvp;
	
	public Shader(String vertexShaderCode, String fragmentShaderCode){
		CreateProgram(vertexShaderCode, fragmentShaderCode);
		IDmvp = GLES20.glGetUniformLocation(Program, "u_MVP");
	}  

	public void CreateProgram(String vertexShaderCode, String fragmentShaderCode){
		VertexShader = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
		GLES20.glShaderSource(VertexShader, vertexShaderCode);
		GLES20.glCompileShader(VertexShader);

		FragmentShader = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
		GLES20.glShaderSource(FragmentShader, fragmentShaderCode);
		GLES20.glCompileShader(FragmentShader);


		Program = GLES20.glCreateProgram();
		GLES20.glAttachShader(Program, VertexShader);
		GLES20.glAttachShader(Program, FragmentShader);
		GLES20.glLinkProgram(Program);
	      
		GLES20.glReleaseShaderCompiler();
	}
	
	
	public void SetShader(String vertexShaderCode, String fragmentShaderCode){
		GLES20.glDetachShader(Program, VertexShader);
		GLES20.glDetachShader(Program, FragmentShader);
		GLES20.glDeleteShader(VertexShader);
		GLES20.glDeleteShader(FragmentShader);
		GLES20.glDeleteProgram(Program);

		VertexShader = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
		GLES20.glShaderSource(VertexShader, vertexShaderCode);
		GLES20.glCompileShader(VertexShader);

		FragmentShader = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
		GLES20.glShaderSource(FragmentShader, fragmentShaderCode);
		GLES20.glCompileShader(FragmentShader);


		Program = GLES20.glCreateProgram();
		GLES20.glAttachShader(Program, VertexShader);
		GLES20.glAttachShader(Program, FragmentShader);
		GLES20.glLinkProgram(Program);
	}
	
	


	public void SetMatrix(float [] modelViewProjectionMatrix){
		GLES20.glUniformMatrix4fv(IDmvp, 1, false, modelViewProjectionMatrix, 0);
	}

	
	public void UseProgram(){
		GLES20.glUseProgram(Program);
	}

}