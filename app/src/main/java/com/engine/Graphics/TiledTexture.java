package com.engine.Graphics;

import java.io.InputStream;




import com.engine.Functions;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


//НЕ РАБОТАЕТ, пока что...


public class TiledTexture 
{
    private int []names;
    
    private Bitmap bitmap;
    private Bitmap tempbitmap;    
    
    public final static int LINEAR = 0;
    public final static int NEAREST = 1;
    public final static int MIPMAP = 2;
    
    private int mode = 0;
    int N;
    int Wt,Ht;
    int I;
    public int[] Tex;
     
    public TiledTexture(String bm, int m, int n) 
    {
    	N = n;	
        names = new int[N];       
        Tex = new int[N];
        GLES20.glGenTextures(N, names, 0);
        GLES20.glPixelStorei(GLES20.GL_UNPACK_ALIGNMENT, 1);       
        bitmap = Functions.BitmapLoad(bm);       
        mode = m;
        
        
        I=0;
    }
    
    int GetTexture(int x0, int y0, int w, int h){
        tempbitmap = Bitmap.createBitmap(bitmap, x0, y0, w, h);

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, names[I]);
        
        SetFilter(mode);
        SetTextureWrap(0);
        
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, tempbitmap, 0);
        
        tempbitmap.recycle();
        
        //Tex[I] = TextureManager.Get(names[I], mode);
        if(mode == MIPMAP){GLES20.glGenerateMipmap(GLES20.GL_TEXTURE_2D);}	
        
        return I++;
    }
    
    public void End(){
        bitmap.recycle();
    }
    

    public void SetFilter(int m)
    {
    	mode = m;
        if(mode == LINEAR)
        {
          GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MIN_FILTER,GLES20.GL_LINEAR);
          GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MAG_FILTER,GLES20.GL_LINEAR);
        }   
        if(mode == NEAREST)
        {
          GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MIN_FILTER,GLES20.GL_NEAREST);
          GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MAG_FILTER,GLES20.GL_NEAREST);
        }
        if(mode == MIPMAP)
        {
          GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MIN_FILTER,GLES20.GL_LINEAR_MIPMAP_LINEAR);
          GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_MAG_FILTER,GLES20.GL_LINEAR);
          GLES20.glGenerateMipmap(GLES20.GL_TEXTURE_2D);
        }
    }
    
    public void SetTextureWrap(int m)
    {
    	if(m==0)
    	{
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_WRAP_S,GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_WRAP_T,GLES20.GL_CLAMP_TO_EDGE);	
    	}
    	else
    	{
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_WRAP_S,GLES20.GL_REPEAT);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_WRAP_T,GLES20.GL_REPEAT);			
    	}  	
    }
    

    public void Use(int TexID, int i){
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + TexID);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, names[i]);
    }
    
    public void Delete(){
        GLES20.glDeleteTextures(N, names, 0);
    }
    
    
    public int GetHeight()
    {
    	return bitmap.getHeight();
    }
    
    public int GetWidth()
    {
    	return bitmap.getWidth();
    }   
    

    public int[] GetNames()
    {
        return names;
    }
}
