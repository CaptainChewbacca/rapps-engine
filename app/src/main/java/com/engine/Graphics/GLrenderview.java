package com.engine.Graphics;

public interface GLRenderView {
	
    public void Create(int width, int height);
    
    public void Render();
    
    public void Destroy();
    
    public void TouchDown(float x, float y, int i);
    
    public void TouchDrag(float x, float y, int i);
    
    public void TouchUp(int i);
}
