package com.engine.Graphics;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class Settings {

	public static final String APP_PREFERENCES = "settings";
	SharedPreferences Settings;
	
	public Settings(Context c){
		Settings = PreferenceManager.getDefaultSharedPreferences(c);
	}
	
	public void Save(Object o, int id){
		Editor z = Settings.edit();
	    z.putString(String.valueOf(id), String.valueOf(o));
	    z.commit();  
	}
	
	public void ToDef(int id, Object d){
		if(Settings.getString(String.valueOf(id), "null") == "null")
		{
			Editor z = Settings.edit();
		    z.putString(String.valueOf(id), String.valueOf(d));
		    z.commit();
		}
	}
	
	public String GetSave(int id){
		return Settings.getString(String.valueOf(id), "null");
	}
	
	public String GetSave(int id, Object def){
		return Settings.getString(String.valueOf(id), String.valueOf(def));
	}
}
