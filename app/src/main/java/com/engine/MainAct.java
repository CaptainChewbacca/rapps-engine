package com.engine;


import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Handler;
import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.engine.Graphics.Finger;

import java.io.IOException;
import java.io.InputStream;

public class MainAct extends Activity implements OnTouchListener  {

	int upPI = 0;
	int downPI = 0;
	public boolean inTouch = false;
	public boolean inTouchDrag = false;
	public boolean inTouchUp = true;
	public boolean inPause = false;

    public int FPS = 60;

    private Handler RenderH = new Handler();

	
    public GLSurfaceView glSurfaceView;
    private GLRender glRender;

    public TextView textv;
    private WakeLock wakeLock;
    
    public Finger[] multiT;

    JNImanager JNI;

    public AssetManager Assets;



	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Assets = getAssets();


        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        textv = new TextView(this);
        LinearLayout.LayoutParams blp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        textv.setText("");
        textv.setTextSize(30);
        textv.setTextColor(Color.WHITE);


        multiT = new Finger[10];
        for(int i = 0; i<9; i++){
        	multiT[i] = new Finger(-1, -10000, -10000);
        }

        glSurfaceView = new GLSurfaceView(this);
        glSurfaceView.setEGLContextClientVersion(2);
        glRender = new GLRender(this);
        glSurfaceView.setRenderer(glRender);
        glSurfaceView.setOnTouchListener(this);
        glSurfaceView.setRenderMode(glSurfaceView.RENDERMODE_WHEN_DIRTY);
        glSurfaceView.setPreserveEGLContextOnPause(true);
        setContentView(glSurfaceView);
        
		PowerManager powerManager = (PowerManager)getSystemService(this.getApplicationContext().POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "wakelock");

        this.addContentView(textv,blp);


    }

	
    public void setText(final String s){
    	runOnUiThread(new Runnable() {
            public void run() {
            	textv.setText(s);
            }
        });
    }

    public AssetFileDescriptor GetAssetFileDescriptor(final String p){
        try{
            return Assets.openFd(p);
        }catch (IOException e) {}
        return null;
    }

    public InputStream GetAssetFile(final String p){
        try{
            return Assets.open(p);
        }catch (IOException e) {}
        return null;
    }

    public String[] GetAssetPath(final String p){
        try
        {
            return Assets.list(p);
        }catch (IOException e) {}
        return null;
    }



    void Render(){
        RenderH.removeCallbacks(RenderRun);
        if(!inPause) {
            RenderH.postDelayed(RenderRun, 1000 / FPS);
            glSurfaceView.requestRender();
        }
    }
    private final Runnable RenderRun = new Runnable() {
        public void run() {
            Render();
        }
    };

    

    @Override
    protected void onPause() {
        wakeLock.release();
        glSurfaceView.onPause();
        inPause = true;
        super.onPause();
    }
    @Override
    protected void onResume() {
        super.onResume();
        glSurfaceView.onResume();
        inPause = false;
        Render();
        wakeLock.acquire();
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        //glSurfaceView.onResume();
        wakeLock.acquire();
    }
    
  
   

    @Override
    protected void onStop(){
        super.onStop();
    }
    
    @Override
    protected void onDestroy(){
    	super.onDestroy();
    }


	@Override
	public boolean onTouch(View v, MotionEvent event) {

	    int actionMask = event.getActionMasked();
	    int pI = event.getActionIndex();
	    int pC = event.getPointerCount();
	      
	    switch (actionMask) {
	    case MotionEvent.ACTION_DOWN:
	      inTouch = true;
	      multiT[pC-1] = new Finger(event.getPointerId(pI), event.getX(pI), event.getY(pI));  
	    case MotionEvent.ACTION_POINTER_DOWN:
	      downPI = pI;
	      multiT[pC-1] = new Finger(event.getPointerId(pI), event.getX(pI), event.getY(pI));
	      break;
	    case MotionEvent.ACTION_UP:
	      inTouchDrag = false;
	      inTouch = false;
	      for(int i = 0; i<9; i++){
	        multiT[i] = new Finger(-1, -10000, -10000);
	      }
	    case MotionEvent.ACTION_POINTER_UP:
	        upPI = pI;
		    multiT[pI] = new Finger(-1, -10000, -10000);
	        break;
	    case MotionEvent.ACTION_MOVE:
	        for (int i = 0; i < pC; i++) {
	    	     multiT[event.getPointerId(i)].Set(event.getPointerId(i), event.getX(i), event.getY(i));
            }
	    }

	    return true;
	}  
}

