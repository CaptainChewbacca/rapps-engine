package com.engine;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Functions {
	
	public static FloatBuffer newFloatBuffer(float[] verticesData) {
		FloatBuffer floatBuffer;
		floatBuffer = ByteBuffer.allocateDirect(verticesData.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		floatBuffer.put(verticesData).position(0);
		return floatBuffer;
	}

    public static Bitmap BitmapLoad(String p){
        final Bitmap b;
        int res_id = GLRender.App.getResources().getIdentifier(GLRender.App.getApplicationContext().getPackageName()+":raw/"+p, null, null);

        InputStream is =  GLRender.App.getResources().openRawResource(res_id);
        b = BitmapFactory.decodeStream(is);
        return b;
    }
    public static Bitmap BitmapLoad1(String p){
        final Bitmap b;
        //int res_id = GLRender.App.getResources().getIdentifier(GLRender.App.getApplicationContext().getPackageName()+":raw/"+Name, null, null);

        InputStream is =  GLRender.App.GetAssetFile(p);//GLRender.App.getResources().openRawResource(res_id);
        b = BitmapFactory.decodeStream(is);
        return b;
    }


    public static String TextFileLoad(String p){
        try{
            InputStream is = GLRender.App.GetAssetFile(p);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer);
        }catch (IOException e) {

        }
        return null;
    }
}
