package com.engine;

import android.opengl.Matrix;

/**
 * Created by acer on 07.06.2016.
 */
public class Object2D {

    public float SX,SY,X,Y,A;

    public float OffsetZ = -5;


    public float[] FinalMtx = new float[16];


    public Object2D(){
        SetSprite(0,0,1,1,0);
    }


    public float[] GetFinalMatrix(){
        return FinalMtx;
    }


    public void SetRectPosition(float x1,float y1, float x2, float y2){
        float sizeX = x2-x1;
        float sizeY = y2-y1;
        float posX = (x1+x2)/2.0f;
        float posY = (x1+x2)/2.0f;
        SetSprite(posX, posY, sizeX, sizeY, 0.0f);

        SX=sizeX;SY=sizeY;X=posX;Y=posY;A=0;
    }



    public void SetSize(float sy){
        SetSprite(X,Y, sy,sy,A);
        SX = sy;
        SY = sy;
    }

    public void SetSize(float sx,float sy){
        SetSprite(X,Y, sx,sy,A);
        SX = sx;
        SY = sy;
    }
    public void SetPosition(float x, float y){
        SetSprite(x,y, SX,SY,A);
        X = x;
        Y = y;
    }
    public void SetRotation(float a){
        SetSprite(X,Y, SX,SY,a);
        A = a;
    }

    public void SetSprite(float x, float y,float sx,float sy,float a){
        reset();

        translate(x, y);
        scale(sx,sy);
        rotate(a);

        SX=sx;SY=sy;X=x;Y=y;A=a;
    }

    private void translate(float x, float y){
        Matrix.translateM(FinalMtx,0,x,y, OffsetZ);
    }
    private void scale(float x, float y){
        Matrix.scaleM(FinalMtx,0,x,y,1.0f);
    }
    private void rotate(float d){
        Matrix.rotateM(FinalMtx,0,d,0,0,1.0f);
    }
    private void reset(){
        Matrix.setIdentityM(FinalMtx,0);
    }

}
