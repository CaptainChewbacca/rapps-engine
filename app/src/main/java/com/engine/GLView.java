package com.engine;



import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.graphics.PixelFormat;
import android.opengl.GLDebugHelper;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.Writer;
import java.util.ArrayList;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGL11;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;


public class GLView extends SurfaceView implements SurfaceHolder.Callback {
   private final static String TAG = "GlSurfaceView";
   private static final int ANDROID_SDK_VERSION = Build.VERSION.SDK_INT;
   private final static boolean LOG_PAUSE_RESUME = false;
   private final static boolean LOG_SWAP_TIME = false;
   private final static boolean LOG_ATTACH_DETACH = false;
   private final static boolean LOG_THREADS = false;
   private final static boolean LOG_SURFACE = false;
   private final static boolean LOG_RENDERER = false;
   private final static boolean LOG_RENDERER_DRAW_FRAME = false;
   private final static boolean LOG_EGL = false;

   private final static boolean DRAW_TWICE_AFTER_SIZE_CHANGED = true;

   
   public final static int RENDERMODE_WHEN_DIRTY = 0;
   public final static int RENDERMODE_CONTINUOUSLY = 1;

   public final static int DEBUG_CHECK_GL_ERROR = 1;
   public final static int DEBUG_LOG_GL_CALLS = 2;

   private final Context context;


   public GLView(final Context context) {
       super(context);
       this.context = context;
       init();
   }

   public GLView(final Context context, final AttributeSet attrs) {
       super(context, attrs);
       this.context = context;
       init();
   }

   private void init() {
       final SurfaceHolder holder = getHolder();
       holder.addCallback(this);
       if (ANDROID_SDK_VERSION <= 8) {
           holder.setFormat(PixelFormat.RGB_565);
       }
   }


   public void setGLWrapper(final GLWrapper glWrapper) {
       mGLWrapper = glWrapper;
   }


   public void setDebugFlags(final int debugFlags) {
       mDebugFlags = debugFlags;
   }


   public int getDebugFlags() {
       return mDebugFlags;
   }


   public void setPreserveEGLContextOnPause(final boolean preserveOnPause) {
       mPreserveEGLContextOnPause = preserveOnPause;
   }


   public boolean getPreserveEGLContextOnPause() {
       return mPreserveEGLContextOnPause;
   }


   public void setRenderer(final Renderer renderer) {
       checkRenderThreadState();
       if (mEGLConfigChooser == null) {
           mEGLConfigChooser = new SimpleComponentEGLConfigChooser(5, 6, 5, 0, true);
       }
       if (mEGLContextFactory == null) {
           mEGLContextFactory = new DefaultContextFactory();
       }
       if (mEGLWindowSurfaceFactory == null) {
           mEGLWindowSurfaceFactory = new DefaultWindowSurfaceFactory();
       }
       mRenderer = renderer;
       mGLThread = new GLThread(renderer);
       mGLThread.start();
   }


   public void setEGLContextFactory(final EGLContextFactory factory) {
       checkRenderThreadState();
       mEGLContextFactory = factory;
   }


   public void setEGLWindowSurfaceFactory(final EGLWindowSurfaceFactory factory) {
       checkRenderThreadState();
       mEGLWindowSurfaceFactory = factory;
   }


   public void setEGLConfigChooser(final EGLConfigChooser configChooser) {
       checkRenderThreadState();
       mEGLConfigChooser = configChooser;
   }


   public void setEGLConfigChooser(final boolean needDepth) {
       setEGLConfigChooser(new SimpleComponentEGLConfigChooser(5, 6, 5, 0, needDepth));
   }


   public void setEGLConfigChooser(final int redSize, final int greenSize, final int blueSize,
           final int alphaSize, final boolean needDepth) {
       setEGLConfigChooser(new SimpleComponentEGLConfigChooser(redSize, greenSize, blueSize,
               alphaSize, needDepth));
   }


   public void setEGLConfigChooserStrict(final int redSize, final int greenSize,
           final int blueSize, final int alphaSize, final int depthSize,
           final int stencilSize) {
       setEGLConfigChooser(new ComponentSizeChooser(redSize, greenSize, blueSize, alphaSize,
               depthSize, stencilSize));
   }


   public void setEGLContextClientVersion(final int version) {
       checkRenderThreadState();
       mEGLContextClientVersion = version;
   }


   public void setRenderMode(final int renderMode) {
       mGLThread.setRenderMode(renderMode);
   }


   public int getRenderMode() {
       return mGLThread.getRenderMode();
   }


   public void requestRender() {
       mGLThread.requestRender();
   }


   @Override
   public void surfaceCreated(final SurfaceHolder holder) {
       mGLThread.surfaceCreated();
   }


   @Override
   public void surfaceDestroyed(final SurfaceHolder holder) {
       mGLThread.surfaceDestroyed();
   }


   @Override
   public void surfaceChanged(final SurfaceHolder holder, final int format, final int w,
           final int h) {
       mGLThread.onWindowResize(w, h);
   }


   public void onPause() {
       mGLThread.onPause();
   }

   public void onResume() {
       mGLThread.onResume();
   }


   public void queueEvent(final Runnable r) {
       mGLThread.queueEvent(r);
   }


   @Override
   public void onWindowFocusChanged(final boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       mGLThread.onWindowFocusChanged(hasFocus);
   }


   @Override
   protected void onAttachedToWindow() {
       super.onAttachedToWindow();
       if (LOG_ATTACH_DETACH) {
           Log.d(TAG, "onAttachedToWindow reattach =" + mDetached);
       }
       if (mDetached && mRenderer != null) {
           int renderMode = RENDERMODE_CONTINUOUSLY;
           if (mGLThread != null) {
               renderMode = mGLThread.getRenderMode();
           }
           mGLThread = new GLThread(mRenderer);
           if (renderMode != RENDERMODE_CONTINUOUSLY) {
               mGLThread.setRenderMode(renderMode);
           }
           mGLThread.start();
       }
       mDetached = false;
   }


   @Override
   protected void onDetachedFromWindow() {
       if (LOG_ATTACH_DETACH) {
           Log.d(TAG, "onDetachedFromWindow");
       }
       if (mGLThread != null) {
           mGLThread.requestExitAndWait();
       }
       mDetached = true;
       super.onDetachedFromWindow();
   }


   public void waitForGlThreadExit() {
       if (mGLThread != null)
           mGLThread.waitForExit();
   }


   public boolean isAlive() {
       return mGLThread != null && mGLThread.isAlive();
   }


   public interface GLWrapper {
       GL wrap(GL gl);
   }


   public interface Renderer {
       void onSurfaceCreated(GL10 gl, EglConfigInfo config);

       void onSurfaceChanged(GL10 gl, int width, int height);

       void onDrawFrame(GL10 gl) throws InterruptedException;

       void logLastSwapDuration(long nanoTimeBeforeEglSwap, long nanoTimeAfterEglSwap);

       void onExit();
   }


   public interface EGLContextFactory {
       EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig);

       void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context);
   }

   private class DefaultContextFactory implements EGLContextFactory {
       private final int EGL_CONTEXT_CLIENT_VERSION = 0x3098;

       @Override
       public EGLContext createContext(final EGL10 egl, final EGLDisplay display,
               final EGLConfig config) {
           final int[] attrib_list = {
                   EGL_CONTEXT_CLIENT_VERSION, mEGLContextClientVersion, EGL10.EGL_NONE
           };

           return egl.eglCreateContext(display, config, EGL10.EGL_NO_CONTEXT,
                   mEGLContextClientVersion != 0 ? attrib_list : null);
       }

       @Override
       public void destroyContext(final EGL10 egl, final EGLDisplay display,
               final EGLContext context) {
           if (!egl.eglDestroyContext(display, context)) {
               Log.e("DefaultContextFactory", "display:" + display + " context: " + context);
               if (LOG_THREADS) {
                   Log.i("DefaultContextFactory", "tid=" + Thread.currentThread().getId());
               }
           }
       }
   }


   public interface EGLWindowSurfaceFactory {
       EGLSurface createWindowSurface(EGL10 egl, EGLDisplay display, EGLConfig config,
               Object nativeWindow);

       void destroySurface(EGL10 egl, EGLDisplay display, EGLSurface surface);
   }

   private static class DefaultWindowSurfaceFactory implements EGLWindowSurfaceFactory {

       @Override
       public EGLSurface createWindowSurface(final EGL10 egl, final EGLDisplay display,
               final EGLConfig config, final Object nativeWindow) {
           EGLSurface result = null;
           try {
               result = egl.eglCreateWindowSurface(display, config, nativeWindow, null);
           } catch (final IllegalArgumentException e) {

               Log.e(TAG, "eglCreateWindowSurface", e);
           }
           return result;
       }

       @Override
       public void destroySurface(final EGL10 egl, final EGLDisplay display,
               final EGLSurface surface) {
           egl.eglDestroySurface(display, surface);
       }
   }


   public interface EGLConfigChooser {
       EGLConfig chooseConfig(EGL10 egl, EGLDisplay display);
   }

   private abstract class BaseConfigChooser implements EGLConfigChooser {
       public BaseConfigChooser(final int[] configSpec) {
           mConfigSpec = filterConfigSpec(configSpec);
       }

       @Override
       public EGLConfig chooseConfig(final EGL10 egl, final EGLDisplay display) {
           final int[] num_config = new int[1];
           if (!egl.eglChooseConfig(display, mConfigSpec, null, 0, num_config)) {
               throw new IllegalArgumentException("eglChooseConfig failed");
           }

           final int numConfigs = num_config[0];

           if (numConfigs <= 0) {
               throw new IllegalArgumentException("No configs match configSpec");
           }

           final EGLConfig[] configs = new EGLConfig[numConfigs];
           if (!egl.eglChooseConfig(display, mConfigSpec, configs, numConfigs, num_config)) {
               throw new IllegalArgumentException("eglChooseConfig#2 failed");
           }
           final EGLConfig config = chooseConfig(egl, display, configs);
           if (config == null) {
               throw new IllegalArgumentException("No config chosen");
           }
           return config;
       }

       abstract EGLConfig chooseConfig(EGL10 egl, EGLDisplay display, EGLConfig[] configs);

       protected int[] mConfigSpec;

       private int[] filterConfigSpec(final int[] configSpec) {
           if (mEGLContextClientVersion != 2) {
               return configSpec;
           }
           /*
            * We know none of the subclasses define EGL_RENDERABLE_TYPE. And we
            * know the configSpec is well formed.
            */
           final int len = configSpec.length;
           final int[] newConfigSpec = new int[len + 2];
           System.arraycopy(configSpec, 0, newConfigSpec, 0, len - 1);
           newConfigSpec[len - 1] = EGL10.EGL_RENDERABLE_TYPE;
           newConfigSpec[len] = 4; /* EGL_OPENGL_ES2_BIT */
           newConfigSpec[len + 1] = EGL10.EGL_NONE;
           return newConfigSpec;
       }
   }

   /**
    * Choose a configuration with exactly the specified r,g,b,a sizes, and at
    * least the specified depth and stencil sizes.
    */
   private class ComponentSizeChooser extends BaseConfigChooser {
       public ComponentSizeChooser(final int redSize, final int greenSize, final int blueSize,
               final int alphaSize, final int depthSize,
               final int stencilSize) {
           super(new int[] {
                   EGL10.EGL_RED_SIZE, redSize, EGL10.EGL_GREEN_SIZE, greenSize,
                   EGL10.EGL_BLUE_SIZE, blueSize, EGL10.EGL_ALPHA_SIZE,
                   alphaSize, EGL10.EGL_DEPTH_SIZE, depthSize, EGL10.EGL_STENCIL_SIZE,
                   stencilSize, EGL10.EGL_NONE
           });
           mValue = new int[1];
           mRedSize = redSize;
           mGreenSize = greenSize;
           mBlueSize = blueSize;
           mAlphaSize = alphaSize;
           mDepthSize = depthSize;
           mStencilSize = stencilSize;
       }

       @Override
       public EGLConfig chooseConfig(final EGL10 egl, final EGLDisplay display,
               final EGLConfig[] configs) {
           EGLConfig closestConfig = null;
           int closestDistance = Integer.MAX_VALUE;
           if (LOG_EGL)
               Log.d(TAG, "Searching for Config with rgba=" + mRedSize + "," + mGreenSize + ","
                       + mBlueSize + "," + mAlphaSize + ", depth="
                       + mDepthSize + ",stencil=" + mStencilSize
                       + " (alpha distance weighted 100 as much as rgb distance)");
           for (final EGLConfig config : configs) {
               // Depth refers to Depth Buffer Size (z-buffer).
               final int d = findConfigAttrib(egl, display, config, EGL10.EGL_DEPTH_SIZE, 0);
               final int s = findConfigAttrib(egl, display, config, EGL10.EGL_STENCIL_SIZE, 0);
               if (d >= mDepthSize && s >= mStencilSize) {
                   final int r = findConfigAttrib(egl, display, config, EGL10.EGL_RED_SIZE, 0);
                   final int g = findConfigAttrib(egl, display, config, EGL10.EGL_GREEN_SIZE, 0);
                   final int b = findConfigAttrib(egl, display, config, EGL10.EGL_BLUE_SIZE, 0);
                   final int a = findConfigAttrib(egl, display, config, EGL10.EGL_ALPHA_SIZE, 0);
                   final int distance = Math.abs(r - mRedSize) + Math.abs(g - mGreenSize)
                           + Math.abs(b - mBlueSize) + Math.abs(a - mAlphaSize);
                   if (LOG_EGL)
                       Log.d(TAG, "Config with distance " + distance + ", rgba=" + r + "," + g
                               + "," + b + "," + a + ", depth=" + d + ", stencil="
                               + s);
                   if (distance < closestDistance) {
                       if (LOG_EGL)
                           Log.d(TAG, "Best Config so far: (distance " + distance + "), rgba=" + r
                                   + "," + g + "," + b + "," + a + ", depth=" + d
                                   + ", stencil=" + s);
                       closestDistance = distance;
                       closestConfig = config;
                   }
               }
           }
           if (ANDROID_SDK_VERSION <= 8 && closestConfig != null) {
               // setFormat is done by SurfaceView in SDK 2.3 and newer.
               final int format;
               final int r = findConfigAttrib(egl, display, closestConfig, EGL10.EGL_RED_SIZE, 0);
               final int g = findConfigAttrib(egl, display, closestConfig, EGL10.EGL_GREEN_SIZE, 0);
               final int b = findConfigAttrib(egl, display, closestConfig, EGL10.EGL_BLUE_SIZE, 0);
               final int a = findConfigAttrib(egl, display, closestConfig, EGL10.EGL_ALPHA_SIZE, 0);
               if (a == 0) {
                   if (r == 8 && g == 8 && b == 8) {
                       format = PixelFormat.RGB_888;
                   } else {
                       format = PixelFormat.RGB_565;
                   }
               } else {
                   if (r == 8 && g == 8 && b == 8) {
                       format = PixelFormat.RGBA_8888;
                   } else if (r == 5 && g == 5 && b == 5) {
                       format = PixelFormat.RGBA_5551;
                   } else {
                       format = a > 1 ? PixelFormat.TRANSLUCENT : PixelFormat.TRANSPARENT;
                   }
               }
               getHolder().setFormat(format);
           }
           return closestConfig;
       }

       private int findConfigAttrib(final EGL10 egl, final EGLDisplay display,
               final EGLConfig config, final int attribute, final int defaultValue) {

           if (egl.eglGetConfigAttrib(display, config, attribute, mValue)) {
               return mValue[0];
           }
           return defaultValue;
       }

       private final int[] mValue;
       // Subclasses can adjust these values:
       protected int mRedSize;
       protected int mGreenSize;
       protected int mBlueSize;
       protected int mAlphaSize;
       protected int mDepthSize;
       protected int mStencilSize;
       protected EglConfigInfo mChosenEglConfigInfo;
   }

   /**
    * This class will choose a surface as close to RGB_565 as possible with or
    * without a depth buffer. If no RGB_565 surface is available it will fall
    * back on lower resolution surfaces like 555 or 4444.
    */
   public class SimpleComponentEGLConfigChooser extends ComponentSizeChooser {
       public SimpleComponentEGLConfigChooser(final int r, final int g, final int b, final int a,
               final boolean withDepthBuffer) {
           super(4, 4, 4, a, withDepthBuffer ? 16 : 0, 0);
           /*
            * The ComponentSizeChooser.chooseConfig() will choose the config
            * closest to the values of mRedSize, mGreenSize, mBlueSize,
            * mALphaSizeAdjust on the time of creation of the surface (which is
            * later than this config instance is created). The component
            * chooser chooses amongst the set of available configs on the
            * device for which bitsizes are equal or larger than those
            * specified in the super-call. This way we'll accept a 4444 or 555
            * buffer if there's no 565 buffer available.
            */
           mRedSize = r;
           mGreenSize = g;
           mBlueSize = b;
       }
   }

   /**
    * An EGL helper class.
    */

   private class EglHelper {
       public EglHelper() {

       }

       /**
        * Initialize EGL for a given configuration spec.
        *
        * @param configSpec
        * @throws StopGlThreadException
        */
       public void start() throws StopGlThreadException {
           if (LOG_EGL) {
               Log.w("EglHelper", "start() tid=" + Thread.currentThread().getId());
           }
           /* Get an EGL instance */
           mEgl = (EGL10) EGLContext.getEGL();
           if (mEgl == null) {
               throwEglException("egl was null");
           }
           /* Get to the default display. */
           mEglDisplay = mEgl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
           if (mEglDisplay == null) {
               throwEglException("eglDisplay was null");
           }

           if (mEglDisplay == EGL10.EGL_NO_DISPLAY) {
               throwEglException("eglGetDisplay failed");
           }

           /* We can now initialize EGL for that display */
           final int[] version = new int[2];
           if (!mEgl.eglInitialize(mEglDisplay, version)) {
               throwEglException("eglInitialize failed");
           }
           mEglConfig = mEGLConfigChooser.chooseConfig(mEgl, mEglDisplay);
           if (mEglConfig == null) {
               throwEglException("eglConfig was null");
           }

           /*
            * Create an EGL context. We want to do this as rarely as we can,
            * because an EGL context is a somewhat heavy object.
            */
           mEglContext = mEGLContextFactory.createContext(mEgl, mEglDisplay, mEglConfig);
           if (mEglContext == null || mEglContext == EGL10.EGL_NO_CONTEXT) {
               mEglContext = null;
               throwEglException("createContext");
           }
           if (LOG_EGL) {
               Log.w("EglHelper", "createContext " + mEglContext + " tid="
                       + Thread.currentThread().getId());
           }

           mEglSurface = null;
       }

       /*
        * React to the creation of a new surface by creating and returning an
        * OpenGL interface that renders to that surface.
        */
       public GL createSurface(final SurfaceHolder holder) throws StopGlThreadException {
           if (LOG_EGL) {
               Log.w("EglHelper", "createSurface()  tid=" + Thread.currentThread().getId());
           }
           /* Check preconditions. */
           if (mEgl == null) {
               throw new RuntimeException("egl not initialized");
           }
           if (mEglDisplay == null) {
               throw new RuntimeException("eglDisplay not initialized");
           }
           if (mEglConfig == null) {
               throw new RuntimeException("mEglConfig not initialized");
           }
           /* The window size has changed, so we need to create a new surface. */
           if (mEglSurface != null && mEglSurface != EGL10.EGL_NO_SURFACE) {

               /* Unbind and destroy the old EGL surface, if there is one. */
               mEgl.eglMakeCurrent(mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE,
                       EGL10.EGL_NO_CONTEXT);
               mEGLWindowSurfaceFactory.destroySurface(mEgl, mEglDisplay, mEglSurface);
           }

           /* Create an EGL surface we can render into. */
           mEglSurface = mEGLWindowSurfaceFactory.createWindowSurface(mEgl, mEglDisplay,
                   mEglConfig, holder);

           if (mEglSurface == null || mEglSurface == EGL10.EGL_NO_SURFACE) {
               final int error = mEgl.eglGetError();
               if (error == EGL10.EGL_BAD_NATIVE_WINDOW) {
                   reportEglException("createWindowSurface returned EGL_BAD_NATIVE_WINDOW.");
               }
               return null;
           }

           /*
            * Before we can issue GL commands, we need to make sure the context
            * is current and bound to a surface.
            */
           if (!mEgl.eglMakeCurrent(mEglDisplay, mEglSurface, mEglSurface, mEglContext)) {
               throwEglException("eglMakeCurrent");
           }

           GL gl = mEglContext.getGL();
           if (mGLWrapper != null) {
               gl = mGLWrapper.wrap(gl);
           }

           if ((mDebugFlags & (DEBUG_CHECK_GL_ERROR | DEBUG_LOG_GL_CALLS)) != 0) {
               int configFlags = 0;
               Writer log = null;
               if ((mDebugFlags & DEBUG_CHECK_GL_ERROR) != 0) {
                   configFlags |= GLDebugHelper.CONFIG_CHECK_GL_ERROR;
               }
               if ((mDebugFlags & DEBUG_LOG_GL_CALLS) != 0) {
                   log = new LogWriter();
               }
               gl = GLDebugHelper.wrap(gl, configFlags, log);
           }
           return gl;
       }

       public void purgeBuffers() {
           mEgl.eglMakeCurrent(mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE,
                   EGL10.EGL_NO_CONTEXT);
           mEgl.eglMakeCurrent(mEglDisplay, mEglSurface, mEglSurface, mEglContext);
       }

       /**
        * Display the current render surface.
        *
        * @return false if the context has been lost.
        * @throws StopGlThreadException
        */
       public boolean swap() throws StopGlThreadException {
           if (!mEgl.eglSwapBuffers(mEglDisplay, mEglSurface)) {

               /*
                * Check for EGL_CONTEXT_LOST, which means the context and all
                * associated data were lost (For instance because the device
                * went to sleep). We need to sleep until we get a new surface.
                */
               final int error = mEgl.eglGetError();
               switch (error) {
                   case EGL11.EGL_CONTEXT_LOST:
                       return false;
                   case EGL10.EGL_BAD_NATIVE_WINDOW:
                       // The native window is bad, probably because the
                       // window manager has closed it. Ignore this error,
                       // on the expectation that the application will be
                       // closed soon.
                       Log.e("EglHelper", "eglSwapBuffers returned EGL_BAD_NATIVE_WINDOW. tid="
                               + Thread.currentThread().getId());
                       break;
                   default:
                       throwEglException("eglSwapBuffers", error);
               }
           }
           return true;
       }

       public void destroySurface() {
           if (LOG_EGL) {
               Log.w("EglHelper", "destroySurface()  tid=" + Thread.currentThread().getId());
           }
           if (mEglSurface != null && mEglSurface != EGL10.EGL_NO_SURFACE) {
               mEgl.eglMakeCurrent(mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE,
                       EGL10.EGL_NO_CONTEXT);
               mEGLWindowSurfaceFactory.destroySurface(mEgl, mEglDisplay, mEglSurface);
               mEglSurface = null;
           }
       }

       public void finish() {
           if (LOG_EGL) {
               Log.w("EglHelper", "finish() tid=" + Thread.currentThread().getId());
           }
           if (mEglContext != null) {
               mEGLContextFactory.destroyContext(mEgl, mEglDisplay, mEglContext);
               mEglContext = null;
           }
           if (mEglDisplay != null) {
               // Disabled hack - so we always do terminate the display, just
               // as in original version

               // Removed the teminate display call due to bugreports on
               // cupcake and donut
               // with exception: eglMakeCurrent failed: 12294
               // http://forum.xda-developers.com/archive/index.php/t-644710.html
               // That was related to Windows mobile gl crashing, so it may
               // have nothing
               // to do with our problem, but it solved their no 12294 bug.
               // if (ANDROID_SDK_VERSION >= 5) {
               // On Tattoo we must terminate the display or the app sometimes
               // hang it appears
               mEgl.eglTerminate(mEglDisplay);
               // }
               mEglDisplay = null;
           }
       }

       private void throwEglException(final String function) throws StopGlThreadException {
           final String errorCode = mEgl != null ? "" + mEgl.eglGetError() : "(mEgl was null)";
           final boolean handled = handleSurfaceException(context, function, errorCode);
           if (handled) {
               throw new StopGlThreadException(function + " failed: " + errorCode);
           } else {
               throw new RuntimeException(function + " failed: " + errorCode);
           }
       }

       private void reportEglException(final String errorMsg) {
           Log.e(TAG, errorMsg);
       }

       private void throwEglException(final String function, final int error)
               throws StopGlThreadException {
           final String errorCode = "" + error;
           final boolean handled = handleSurfaceException(context, function, errorCode);
           if (handled) {
               throw new StopGlThreadException(function + " failed: " + errorCode);
           } else {
               throw new RuntimeException(function + " failed: " + errorCode);
           }
       }

       EGL10 mEgl;
       EGLDisplay mEglDisplay;
       EGLSurface mEglSurface;
       EGLConfig mEglConfig;
       EGLContext mEglContext;

   }

   /**
    * A generic GL Thread. Takes care of initializing EGL and GL. Delegates to
    * a Renderer instance to do the actual drawing. Can be configured to render
    * continuously or on request. All potentially blocking synchronization is
    * done through the sGLThreadManager object. This avoids multiple-lock
    * ordering issues.
    */
   private class GLThread extends Thread {
       public GLThread(final Renderer renderer) {
           super();
           mWidth = 0;
           mHeight = 0;
           mRequestRender = true;
           mRenderMode = RENDERMODE_CONTINUOUSLY;
           mRenderer = renderer;
       }

       @Override
       public void run() {
           setName("GLThread " + getId());
           if (LOG_THREADS)
               Log.i(TAG, "GLThread.run(): starting tid=" + getId());

           try {
               guardedRun();
           } catch (final InterruptedException e) {
               // fall thru and exit normally
           } catch (final StopGlThreadException e) {
               // fall thru and exit normally
           } finally {
               sGLThreadManager.threadExiting(this);
           }
           if (LOG_THREADS)
               Log.i(TAG, "GLThread.run(): GlThread stopped running");
       }

       /*
        * This private method should only be called inside a
        * synchronized(sGLThreadManager) block.
        */
       private void stopEglSurfaceLocked() {
           if (mHaveEglSurface) {
               mHaveEglSurface = false;
               mEglHelper.destroySurface();
           }
       }

       /*
        * This private method should only be called inside a
        * synchronized(sGLThreadManager) block.
        */
       private void stopEglContextLocked() {
           if (mHaveEglContext) {
               mEglHelper.finish();
               mHaveEglContext = false;
               sGLThreadManager.releaseEglContextLocked(this);
           }
       }

       private void guardedRun() throws InterruptedException, StopGlThreadException {
           mEglHelper = new EglHelper();
           mHaveEglContext = false;
           mHaveEglSurface = false;
           try {
               GL10 gl = null;
               boolean createEglContext = false;
               boolean createEglSurface = false;
               boolean lostEglContext = false;
               boolean sizeChanged = false;
               boolean wantRenderNotification = false;
               boolean doRenderNotification = false;
               boolean askedToReleaseEglContext = false;
               int w = 0;
               int h = 0;
               Runnable event = null;

               while (true) {
                   synchronized (sGLThreadManager) {
                       while (true) {
                           if (mShouldExit) {
                               return;
                           }

                           if (!mEventQueue.isEmpty()) {
                               event = mEventQueue.remove(0);
                               break;
                           }

                           // Update the pause state.
                           if (mPaused != mRequestPaused) {
                               mPaused = mRequestPaused;
                               sGLThreadManager.notifyAll();
                               if (LOG_PAUSE_RESUME) {
                                   Log.i("GLThread", "mPaused is now " + mPaused + " tid="
                                           + getId());
                               }
                           }

                           // Do we need to give up the EGL context?
                           if (mShouldReleaseEglContext) {
                               if (LOG_SURFACE) {
                                   Log.i("GLThread", "releasing EGL context because asked to tid="
                                           + getId());
                               }
                               stopEglSurfaceLocked();
                               stopEglContextLocked();
                               mShouldReleaseEglContext = false;
                               askedToReleaseEglContext = true;
                           }

                           // Have we lost the EGL context?
                           if (lostEglContext) {
                               stopEglSurfaceLocked();
                               stopEglContextLocked();
                               lostEglContext = false;
                           }

                           // Do we need to release the EGL surface?
                           if (mHaveEglSurface && mPaused) {
                               if (LOG_SURFACE)
                                   Log.i(TAG, "releasing EGL surface because paused tid="
                                           + getId());
                               if (LOG_SURFACE) {
                                   Log.i("GLThread", "releasing EGL surface because paused tid="
                                           + getId());
                               }
                               stopEglSurfaceLocked();
                               if (!mPreserveEGLContextOnPause
                                       || sGLThreadManager.shouldReleaseEGLContextWhenPausing()) {
                                   stopEglContextLocked();
                                   if (LOG_SURFACE) {
                                       Log.i("GLThread",
                                               "releasing EGL context because paused tid="
                                                       + getId()
                                                       + ", device supports preserve: "
                                                       + !sGLThreadManager
                                                               .shouldReleaseEGLContextWhenPausing());
                                   }
                               }
                               if (sGLThreadManager.shouldTerminateEGLWhenPausing()) {
                                   mEglHelper.finish();
                                   if (LOG_SURFACE) {
                                       Log.i("GLThread", "terminating EGL because paused tid="
                                               + getId());
                                   }
                               }
                           }

                           // Have we lost the surface view surface?
                           if (!mHasSurface && !mWaitingForSurface) {
                               if (LOG_SURFACE)
                                   Log.i(TAG,
                                           "GLThread.guardedRun(): noticed surfaceView surface lost tid="
                                                   + getId());
                               if (mHaveEglSurface) {
                                   stopEglSurfaceLocked();
                               }
                               mWaitingForSurface = true;
                               sGLThreadManager.notifyAll();
                           }

                           // Have we acquired the surface view surface?
                           if (mHasSurface && mWaitingForSurface) {
                               if (LOG_SURFACE)
                                   Log.i(TAG,
                                           "GLThread.guardedRun(): noticed surfaceView surface acquired tid="
                                                   + getId());
                               mWaitingForSurface = false;
                               sGLThreadManager.notifyAll();
                           }

                           if (doRenderNotification) {
                               if (LOG_SURFACE) {
                                   Log.i("GLThread", "sending render notification tid=" + getId());
                               }
                               wantRenderNotification = false;
                               doRenderNotification = false;
                               mRenderComplete = true;
                               sGLThreadManager.notifyAll();
                           }

                           // Ready to draw?
                           if (readyToDraw()) {

                               // If we don't have an EGL context, try to
                               // acquire one.
                               if (!mHaveEglContext) {
                                   if (askedToReleaseEglContext) {
                                       askedToReleaseEglContext = false;
                                   } else if (sGLThreadManager.tryAcquireEglContextLocked(this)) {
                                       try {
                                           mEglHelper.start();
                                       } catch (final RuntimeException t) {
                                           sGLThreadManager.releaseEglContextLocked(this);
                                           throw t;
                                       }
                                       mHaveEglContext = true;
                                       createEglContext = true;

                                       sGLThreadManager.notifyAll();
                                       if (LOG_SURFACE) {
                                           Log.w("GLThread", "egl create context");
                                       }
                                   }
                               }

                               if (mHaveEglContext && !mHaveEglSurface) {
                                   mHaveEglSurface = true;
                                   createEglSurface = true;
                                   sizeChanged = true;
                               }

                               if (mHaveEglSurface) {
                                   if (mSizeChanged) {
                                       sizeChanged = true;
                                       w = mWidth;
                                       h = mHeight;
                                       wantRenderNotification = true;
                                       if (LOG_SURFACE) {
                                           Log.i("GLThread",
                                                   "noticing that we want render notification tid="
                                                           + getId());
                                       }

                                       if (DRAW_TWICE_AFTER_SIZE_CHANGED) {
                                           // We keep mRequestRender true so
                                           // that we draw twice after the size
                                           // changes.
                                           // (Once because of mSizeChanged,
                                           // the second time because of
                                           // mRequestRender.)
                                           // This forces the updated graphics
                                           // onto the screen.
                                       } else {
                                           mRequestRender = false;
                                       }
                                       mSizeChanged = false;
                                   } else {
                                       mRequestRender = false;
                                   }
                                   sGLThreadManager.notifyAll();
                                   break;
                               }
                           }

                           // By design, this is the only place in a GLThread
                           // thread where we wait().
                           if (LOG_THREADS)
                               Log.i(TAG, "GLThread.guardedRun(): waiting tid=" + getId()
                                       + ", rendermode continuous="
                                       + (mRenderMode == RENDERMODE_CONTINUOUSLY));
                           sGLThreadManager.wait();
                       }
                   } // end of synchronized(sGLThreadManager)

                   if (event != null) {
                       event.run();
                       event = null;
                       continue;
                   }

                   /*
                    * The mHasFocus is applied on two levels. It is used it
                    * readyToDraw to avoid continuous draws if mHasFocus=false,
                    * but to allow one draw even without focus (to get stuff
                    * drawn behind dialogs for instance, and perhaps also on
                    * resume). The mHasFocus is also applied here for small SDK
                    * versions to avoid or reduce incorrect portrait mode
                    * clipping. On G1 clipping will become wrong after screen
                    * has been off if we ommit this mHasFocus check, and on G1
                    * the problem will remain until user has gone to home
                    * screen and back (thus destroying and recreating the
                    * context and surface)
                    */
                   if (mHasFocus || ANDROID_SDK_VERSION >= 5) {
                       if (createEglSurface) {
                           if (LOG_SURFACE) {
                               Log.w("GLThread", "GLThread.guardedRun(): egl createSurface");
                           }
                           gl = (GL10) mEglHelper.createSurface(getHolder());
                           if (gl == null) {
                               // Couldn't create a surface. Quit quietly.
                               break;
                           }
                           sGLThreadManager.checkGLDriver(gl);
                           createEglSurface = false;
                           // Here the previous GLSurfaceView always called
                           // mRenderer.onSurfaceCreated
                       }

                       if (createEglContext) {
                           if (LOG_RENDERER) {
                               Log.w("GLThread", "GLThread.guardedRun(): onSurfaceCreated");
                           }
                           mRenderer.onSurfaceCreated(gl, new EglConfigInfo(mEglHelper));
                           createEglContext = false;
                       }
                       if (sizeChanged) {
                           if (LOG_RENDERER) {
                               Log.w("GLThread", "GLThread.guardedRun(): onSurfaceChanged(" + w
                                       + ", " + h + ")");
                           }
                           mEglHelper.purgeBuffers();
                           mRenderer.onSurfaceChanged(gl, w, h);
                           sizeChanged = false;
                       }
                       if (LOG_RENDERER_DRAW_FRAME) {
                           Log.w(TAG, "GLThread.guardedRun(): onDrawFrame tid=" + getId());
                       }
                       mRenderer.onDrawFrame(gl);
                       long time;
                       if (LOG_SWAP_TIME)
                           time = System.nanoTime();
                       if (!mEglHelper.swap()) {
                           if (LOG_SURFACE)
                               Log.i(TAG, "egl surface lost tid=" + getId());
                           lostEglContext = true;
                       }
                       if (LOG_SWAP_TIME)
                           mRenderer.logLastSwapDuration(time, System.nanoTime());
                   }
                   if (wantRenderNotification) {
                       doRenderNotification = true;
                   }
               }
           } finally {
               mRenderer.onExit();
               /* clean-up everything... */
               synchronized (sGLThreadManager) {
                   stopEglSurfaceLocked();
                   stopEglContextLocked();
               }
           }
       }

       public boolean ableToDraw() {
           return mHaveEglContext && mHaveEglSurface && readyToDraw();
       }

       private boolean readyToDraw() {
           // Added mHasFocus to fix Android < 5 G1 Clipping problem, see
           // comments around if-block at line 1575.
           return !mPaused
                   && mHasSurface
                   && mWidth > 0
                   && mHeight > 0
                   && (mRequestRender || mRenderMode == RENDERMODE_CONTINUOUSLY
                           && (mHasFocus || ANDROID_SDK_VERSION >= 5));
       }

       public void setRenderMode(final int renderMode) {
           if (!(RENDERMODE_WHEN_DIRTY <= renderMode && renderMode <= RENDERMODE_CONTINUOUSLY)) {
               throw new IllegalArgumentException("renderMode");
           }
           synchronized (sGLThreadManager) {
               mRenderMode = renderMode;
               sGLThreadManager.notifyAll();
           }
       }

       public int getRenderMode() {
           synchronized (sGLThreadManager) {
               return mRenderMode;
           }
       }

       public void requestRender() {
           synchronized (sGLThreadManager) {
               mRequestRender = true;
               sGLThreadManager.notifyAll();
           }
       }

       public void surfaceCreated() {
           synchronized (sGLThreadManager) {
               if (LOG_THREADS) {
                   Log.i(TAG, "GLThread.surfaceCreated() tid=" + getId());
               }
               mHasSurface = true;
               sGLThreadManager.notifyAll();
               while (mWaitingForSurface && !mExited) {
                   try {
                       sGLThreadManager.wait();
                   } catch (final InterruptedException e) {
                       Thread.currentThread().interrupt();
                   }
               }
           }
       }

       public void surfaceDestroyed() {
           synchronized (sGLThreadManager) {
               if (LOG_THREADS) {
                   Log.i(TAG, "GLThread.surfaceDestroyed() tid=" + getId());
               }
               mHasSurface = false;
               sGLThreadManager.notifyAll();
               while (!mWaitingForSurface && !mExited) {
                   try {
                       sGLThreadManager.wait();
                   } catch (final InterruptedException e) {
                       Thread.currentThread().interrupt();
                   }
               }
           }
       }

       public void onPause() {
           synchronized (sGLThreadManager) {
               if (LOG_PAUSE_RESUME) {
                   Log.i("GLThread", "onPause tid=" + getId());
               }
               mRequestPaused = true;
               sGLThreadManager.notifyAll();
               while (!mExited && !mPaused) {
                   if (LOG_PAUSE_RESUME) {
                       Log.i("Main thread", "onPause waiting for mPaused.");
                   }
                   try {
                       sGLThreadManager.wait();
                   } catch (final InterruptedException ex) {
                       Thread.currentThread().interrupt();
                   }
               }
           }
       }

       public void onResume() {
           synchronized (sGLThreadManager) {
               if (LOG_PAUSE_RESUME) {
                   Log.i("GLThread", "onResume tid=" + getId());
               }
               mRequestPaused = false;
               mRequestRender = true;
               mRenderComplete = false;
               sGLThreadManager.notifyAll();
               while (!mExited && mPaused && !mRenderComplete) {
                   if (LOG_PAUSE_RESUME) {
                       Log.i("Main thread", "onResume waiting for !mPaused.");
                   }
                   try {
                       sGLThreadManager.wait();
                   } catch (final InterruptedException ex) {
                       Thread.currentThread().interrupt();
                   }
               }
           }
       }

       public void onWindowResize(final int w, final int h) {
           synchronized (sGLThreadManager) {
               mWidth = w;
               mHeight = h;
               mSizeChanged = true;
               mRequestRender = true;
               mRenderComplete = false;
               sGLThreadManager.notifyAll();

               // Wait for thread to react to resize and render a frame
               while (!mExited && !mPaused && !mRenderComplete && mGLThread != null
                       && mGLThread.ableToDraw()) {
                   if (LOG_SURFACE) {
                       Log.i("GlSurface Main thread", "GLThread.onWindowResize(" + w + "," + h
                               + ") waiting for render complete.");
                   }
                   try {
                       sGLThreadManager.wait();
                   } catch (final InterruptedException ex) {
                       Thread.currentThread().interrupt();
                   }
               }
           }
       }

       // On some Qualcomm devices (such as the HTC Magic running Android 1.6),
       // there's a bug in the graphics driver that will cause glViewport() to
       // do the wrong thing in a very specific situation. When the screen is
       // rotated, if a surface is created in one layout (say, portrait view)
       // and then rotated to another, subsequent calls to glViewport are
       // clipped.
       // So, if the window is, say, 320x480 when the surface is created, and
       // then the rotation occurs and glViewport() is called with the new
       // size of 480x320, devices with the buggy driver will clip the viewport
       // to the old width (which means 320x320...ugh!). This is fixed in
       // Android 2.1 Qualcomm devices (like Nexus One) and doesn't affect
       // non-Qualcomm devices (like the Motorola DROID).
       //
       // Unfortunately, under Android 1.6 this exact case occurs when the
       // screen is put to sleep and then wakes up again. The lock screen
       // comes up in portrait mode, but at the same time the window surface
       // is also created in the backgrounded game. When the lock screen is
       // closed
       // and the game comes forward, the window is fixed to the correct size
       // which causes the bug to occur.

       // The solution used here is to simply never render when the window
       // surface
       // does not have the focus. When the lock screen (or menu) is up,
       // rendering
       // will stop. This resolves the driver bug (as the egl surface won't be
       // created
       // until after the screen size has been fixed), and is generally good
       // practice
       // since you don't want to be doing a lot of CPU intensive work when the
       // lock
       // screen is up (to preserve battery life).

       // Added to fix Android < 5 G1 Clipping problem, see comments around
       // if-block at line 1575.
       public void onWindowFocusChanged(final boolean hasFocus) {
           synchronized (sGLThreadManager) {
               mHasFocus = hasFocus;
               // Request a render here, because mHasFocus=false now overrides
               // continous mode drawing
               mRequestRender = true;
               sGLThreadManager.notifyAll();
           }
           if (LOG_SURFACE) {
               Log.i(TAG, "GLThread.onWindowFocusChanged(): Focus "
                       + (mHasFocus ? "gained" : "lost"));
           }

       }

       public void requestExitAndWait() {
           // don't call this from GLThread thread or it is a guaranteed
           // deadlock!
           synchronized (sGLThreadManager) {
               mShouldExit = true;
               sGLThreadManager.notifyAll();
               while (!mExited) {
                   try {
                       sGLThreadManager.wait();
                   } catch (final InterruptedException ex) {
                       Thread.currentThread().interrupt();
                   }
               }
           }
       }

       public void waitForExit() {
           // don't call this from GLThread thread or it is a guaranteed
           // deadlock!
           synchronized (sGLThreadManager) {
               while (!mExited) {
                   try {
                       sGLThreadManager.wait();
                   } catch (final InterruptedException ex) {
                       Thread.currentThread().interrupt();
                   }
               }
           }
       }

       public void requestReleaseEglContextLocked() {
           mShouldReleaseEglContext = true;
           sGLThreadManager.notifyAll();
       }

       /**
        * Queue an "event" to be run on the GL rendering thread.
        *
        * @param r the runnable to be run on the GL rendering thread.
        */
       public void queueEvent(final Runnable r) {
           if (r == null) {
               throw new IllegalArgumentException("r must not be null");
           }
           synchronized (sGLThreadManager) {
               mEventQueue.add(r);
               sGLThreadManager.notifyAll();
           }
       }

       // Once the thread is started, all accesses to the following member
       // variables are protected by the sGLThreadManager monitor
       private boolean mShouldExit;
       private boolean mExited;
       private boolean mRequestPaused;
       private boolean mPaused;
       private boolean mHasSurface;
       private boolean mWaitingForSurface;
       private boolean mHaveEglContext;
       private boolean mHaveEglSurface;
       private boolean mShouldReleaseEglContext;
       private int mWidth;
       private int mHeight;
       private int mRenderMode;
       private boolean mRequestRender;
       private boolean mRenderComplete;
       private final ArrayList<Runnable> mEventQueue = new ArrayList<Runnable>();
       private boolean mHasFocus;

       // End of member variables protected by the sGLThreadManager monitor.

       private final Renderer mRenderer;
       private EglHelper mEglHelper;
   }

   static class LogWriter extends Writer {

       @Override
       public void close() {
           flushBuilder();
       }

       @Override
       public void flush() {
           flushBuilder();
       }

       @Override
       public void write(final char[] buf, final int offset, final int count) {
           for (int i = 0; i < count; i++) {
               final char c = buf[offset + i];
               if (c == '\n') {
                   flushBuilder();
               } else {
                   mBuilder.append(c);
               }
           }
       }

       private void flushBuilder() {
           if (mBuilder.length() > 0) {
               Log.v(TAG, mBuilder.toString());
               mBuilder.delete(0, mBuilder.length());
           }
       }

       private final StringBuilder mBuilder = new StringBuilder();
   }

   private void checkRenderThreadState() {
       if (mGLThread != null) {
           throw new IllegalStateException(
                   "setRenderer has already been called for this instance.");
       }
   }

   private static class GLThreadManager {

       public synchronized void threadExiting(final GLThread thread) {
           if (LOG_THREADS) {
               Log.i(TAG, "GLThreadManager: exiting tid=" + thread.getId());
           }
           thread.mExited = true;
           if (mEglOwner == thread) {
               mEglOwner = null;
           }
           notifyAll();
       }

       /*
        * Tries once to acquire the right to use an EGL context. Does not
        * block. Requires that we are already in the sGLThreadManager monitor
        * when this is called.
        * @return true if the right to use an EGL context was acquired.
        */
       public boolean tryAcquireEglContextLocked(final GLThread thread) {
           if (mEglOwner == thread || mEglOwner == null) {
               mEglOwner = thread;
               notifyAll();
               return true;
           }
           checkGLESVersion();
           if (mMultipleGLESContextsAllowed) {
               return true;
           }
           // Notify the owning thread that it should release the context.
           // TODO: implement a fairness policy. Currently
           // if the owning thread is drawing continuously it will just
           // reacquire the EGL context.
           if (mEglOwner != null) {
               mEglOwner.requestReleaseEglContextLocked();
           }
           return false;
       }

       /*
        * Releases the EGL context. Requires that we are already in the
        * sGLThreadManager monitor when this is called.
        */
       public void releaseEglContextLocked(final GLThread thread) {
           if (mEglOwner == thread) {
               mEglOwner = null;
           }
           notifyAll();
       }

       public synchronized boolean shouldReleaseEGLContextWhenPausing() {
           // Release the EGL context when pausing even if
           // the hardware supports multiple EGL contexts.
           // Otherwise the device could run out of EGL contexts.
           return mLimitedGLESContexts;
       }

       public synchronized boolean shouldTerminateEGLWhenPausing() {
           checkGLESVersion();
           return !mMultipleGLESContextsAllowed;
       }

       public synchronized void checkGLDriver(final GL10 gl) {
           if (!mGLESDriverCheckComplete) {
               checkGLESVersion();
               final String renderer = gl.glGetString(GL10.GL_RENDERER);
               if (mGLESVersion < kGLES_20) {
                   mMultipleGLESContextsAllowed = !renderer.startsWith(kMSM7K_RENDERER_PREFIX);
                   notifyAll();
               }
               // Regarding adreno limited contexts:
               // https://developer.qualcomm.com/forum/qdevnet-forums/mobile-gaming-graphics-optimization-adreno/12173
               mLimitedGLESContexts = !mMultipleGLESContextsAllowed
                       || renderer.startsWith(kADRENO);
               if (LOG_SURFACE) {
                   Log.w(TAG, "checkGLDriver renderer = \"" + renderer
                           + "\" multipleContextsAllowed = " + mMultipleGLESContextsAllowed
                           + " mLimitedGLESContexts = " + mLimitedGLESContexts);
               }
               mGLESDriverCheckComplete = true;
           }
       }

       private void checkGLESVersion() {
           if (!mGLESVersionCheckComplete) {
               if (ANDROID_SDK_VERSION >= 4) {
                   try {
                       mGLESVersion = ConfigurationInfo.class.getField("GL_ES_VERSION_UNDEFINED")
                               .getInt(null);
                   } catch (final Exception e) {
                       mGLESVersion = 0; // The constant is zero
                   }
                   if (mGLESVersion >= kGLES_20) {
                       mMultipleGLESContextsAllowed = true;
                   }
               } else {
                   mGLESVersion = 0;
               }
               if (LOG_SURFACE) {
                   Log.w(TAG, "checkGLESVersion mGLESVersion =" + " " + mGLESVersion
                           + " mMultipleGLESContextsAllowed = "
                           + mMultipleGLESContextsAllowed);
               }
               mGLESVersionCheckComplete = true;
           }
       }

       private boolean mGLESVersionCheckComplete;
       private int mGLESVersion;
       private boolean mGLESDriverCheckComplete;
       private boolean mMultipleGLESContextsAllowed;
       private boolean mLimitedGLESContexts;
       private static final int kGLES_20 = 0x20000;
       private static final String kMSM7K_RENDERER_PREFIX = "Q3Dimension MSM7500 ";
       private static final String kADRENO = "Adreno";
       private GLThread mEglOwner;
   }

   public boolean handleSurfaceException(final Context context, final String function,
           final String errorCode) {
       return false;
   }

   public static class StopGlThreadException extends Exception {
       private static final long serialVersionUID = 1L;

       public StopGlThreadException(final String detailMessage) {
           super(detailMessage);
       }
   }

   public class EglConfigInfo {
       public final int mRedSize;
       public final int mGreenSize;
       public final int mBlueSize;
       public final int mAlphaSize;
       public final int mDepthSize;
       public final int mStencilSize;

       private int findConfigAttrib(final EGL10 egl, final EGLDisplay display,
               final EGLConfig config, final int attribute, final int defaultValue) {
           final int[] value = new int[1];
           if (egl.eglGetConfigAttrib(display, config, attribute, value)) {
               return value[0];
           }
           return defaultValue;
       }

       private EglConfigInfo(final EglHelper eglHelper) {
           this(eglHelper.mEgl, eglHelper.mEglDisplay, eglHelper.mEglConfig);
       }

       private EglConfigInfo(final EGL10 egl, final EGLDisplay display, final EGLConfig config) {
           final int d = findConfigAttrib(egl, display, config, EGL10.EGL_DEPTH_SIZE, 0);
           final int s = findConfigAttrib(egl, display, config, EGL10.EGL_STENCIL_SIZE, 0);
           final int r = findConfigAttrib(egl, display, config, EGL10.EGL_RED_SIZE, 0);
           final int g = findConfigAttrib(egl, display, config, EGL10.EGL_GREEN_SIZE, 0);
           final int b = findConfigAttrib(egl, display, config, EGL10.EGL_BLUE_SIZE, 0);
           final int a = findConfigAttrib(egl, display, config, EGL10.EGL_ALPHA_SIZE, 0);

           this.mRedSize = r;
           this.mGreenSize = g;
           this.mBlueSize = b;
           this.mAlphaSize = a;
           this.mDepthSize = d;
           this.mStencilSize = s;
       }

       public boolean isRgb888() {
           return mRedSize == 8 && mGreenSize == 8 && mBlueSize == 8;
       }

       public boolean isRgb565() {
           return mRedSize == 5 && mGreenSize == 6 && mBlueSize == 5;
       }

       public boolean isAlpha8() {
           return mAlphaSize == 8;
       }

       public boolean isAlpha0() {
           return mAlphaSize == 0;
       }

       @Override
       public String toString() {
           return "RGBADS=" + mRedSize + mGreenSize + mBlueSize + mAlphaSize + mDepthSize
                   + mStencilSize;
       }
   }

   private static final GLThreadManager sGLThreadManager = new GLThreadManager();
   private boolean mSizeChanged = true;

   private GLThread mGLThread;
   private Renderer mRenderer;
   private boolean mDetached;
   private EGLConfigChooser mEGLConfigChooser;
   private EGLContextFactory mEGLContextFactory;
   private EGLWindowSurfaceFactory mEGLWindowSurfaceFactory;
   private GLWrapper mGLWrapper;
   private int mDebugFlags;
   private int mEGLContextClientVersion;
   private boolean mPreserveEGLContextOnPause;
}
