package com.engine;

public class JNImanager {

    static {
        System.loadLibrary("jni");
    }

    public static native void Init(int idp, int idc);
    public static native int AddParticleSprite(int m, float t);
    public static native void DeleteParticleSprite(int id);
    public static native void Update(float time, float dt, int id);
    public static native float getDebug();
    public static native void InitSound();
}
