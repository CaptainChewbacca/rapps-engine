package com.engine;

public class QuickMath {

	static public final float PI = 3.1415927f;
	static public final float DegToRad = 0.017453f;
	static public final float RadToDeg = 57.295779f;
	
	
	static long RandomIteration = 1;
	
	public static float Random(){
		long x = RandomIteration;
		++RandomIteration;
	    x = (x<<13) ^ x;
	    return ( 1.0f - ( (x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f);    
	}
	
	public static float Random1(){
		long x = RandomIteration;
		++RandomIteration;
	    x = (x<<13) ^ x;
	    return 0.5f*( 2.0f - ( (x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f);    
	}


	
}
