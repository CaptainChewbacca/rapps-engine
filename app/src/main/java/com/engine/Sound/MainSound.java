package com.engine.Sound;


import java.util.ArrayList;


import com.engine.GLRender;
import com.engine.Info;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;


public class MainSound {
	public SoundPool soundPool;


	public ArrayList<Sound> Sounds;
	
	public MainSound(OnLoadCompleteListener listener){
		soundPool = new SoundPool(16, AudioManager.STREAM_MUSIC, 100);
		soundPool.setOnLoadCompleteListener(listener);
		Sounds = new ArrayList<Sound>();	
	}
	

	public void AddSound(Sound s){
		Sounds.add(s);
		s.SetMS(this);
		s.KEY = soundPool.load(GLRender.App.GetAssetFileDescriptor(s.Path), 0);
	}
	

	public void Update(byte b){
		for(int i = 0; i<Sounds.size(); ++i){
			if(Sounds.get(i).Load && Sounds.get(i).GameSound!=b){
				soundPool.stop(Sounds.get(i).KEY);
				soundPool.unload(Sounds.get(i).KEY);
				Sounds.remove(i);
			}
			else{
				if(Sounds.get(i).PlayS == 2 && Sounds.get(i).Load){
					soundPool.pause(Sounds.get(i).KEY);
					Sounds.get(i).PlayS=0;	
				}
				
				if(Sounds.get(i).PlayS == 1 && Sounds.get(i).Load)
				{
					if(Sounds.get(i).StartP==false){
						Sounds.get(i).KEY = soundPool.play(Sounds.get(i).KEY, Sounds.get(i).Volume,
							Sounds.get(i).Volume, 1, Sounds.get(i).loop, Sounds.get(i).Speed);
					  Sounds.get(i).StartP=true;
					}
					else
					{
					  soundPool.resume(Sounds.get(i).KEY);
					}
					Sounds.get(i).PlayS=3;
				}
			}
		}
	}
	
	public Sound GetID(int sID){
		for(int i = 0; i<Sounds.size(); ++i){
			if(Sounds.get(i).KEY==sID){
				return Sounds.get(i);
			}
		}
		return null;
	}
	

	public void StopSound(int ID){
		Sound s = Sounds.get(ID);
		s.PlayS = 2;
	}
	
	public void PlaySound(int ID) {
		Sound s = Sounds.get(ID);
		s.PlayS = 1;
    }
	
	
	public void Start(OnLoadCompleteListener listener){
		soundPool = new SoundPool(16, AudioManager.STREAM_MUSIC, 100);
		soundPool.setOnLoadCompleteListener(listener);
		Sounds = new ArrayList<Sound>();	
	}
	
	public void Clear(){
		soundPool.release();
		Sounds.clear();
	}
}
