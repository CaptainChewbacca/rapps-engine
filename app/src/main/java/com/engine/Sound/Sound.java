package com.engine.Sound;

import android.content.Context;
import android.content.res.AssetManager;


public class Sound {
	public String Path;
	public float Volume = 0.5f;
	public int KEY = 0;
	public byte loop = 0;
    public boolean Load = false;
    public byte PlayS = 0;
    public byte GameSound = 0;
    public float Speed = 1;
    private static MainSound MS;
    public boolean StartP = false;
	
	public Sound(String name){
		Path = name;
	}
	
	public void SetMS(MainSound ms){
		MS=ms;
	}
	
	public void SetSpeed(float s){
		Speed = s;
		MS.soundPool.setRate(KEY, s);
	}
	
	public void SetLoop(byte l){
		loop = l;
		MS.soundPool.setLoop(KEY, loop);
	}
	
	
	public void SetVolume(float v){
		Volume = v;
		MS.soundPool.setVolume(KEY, v, v);
	}
	
	public void PlaySound(){
		if(Load)
		 MS.soundPool.play(KEY, Volume, Volume, 1, loop, Speed);
	}


}
