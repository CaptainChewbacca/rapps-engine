package com.engine;

import android.opengl.Matrix;

/**
 * Created by acer on 07.06.2016.
 */
public class Object3D {

    public float SX,SY,SZ,X,Y,Z,AX,AY,AZ;



    public float[] VertMtx = new float[16];



    public Object3D(){
        Matrix.setIdentityM(VertMtx,0);
        SetSprite(0,0,0, 1,1,1, 0,0,0);
    }

    public float[] GetFinalMatrix(){
        return VertMtx;
    }


    public void SetSize(float sx,float sy, float sz){
        SetSprite(X,Y,Z, sx,sy,sz, AX,AY,AZ);
    }
    public void SetPosition(float x, float y, float z){
        SetSprite(x,y,z, SX,SY,SZ, AX,AY,AZ);
    }
    public void SetRotation(float ax, float ay, float az){
        SetSprite(X,Y,Z, SX,SY,SZ, ax,ay,az);
    }

    void SetSprite(float x, float y,float z, float sx, float sy,float sz, float ax, float ay,float az){
        reset();

        translate(x, y, z);
        scale(sx,sy,sz);


        rotateZ(az);
        rotateX(ax);
        rotateY(ay);

        SX=sx;SY=sy;SZ=sz;X=x;Y=y;Z=z;AX=ax;AY=ay;AZ=az;
    }

    private void translate(float x, float y, float z){
        Matrix.translateM(VertMtx,0,x,y,z);
    }
    private void scale(float x, float y, float z){
        Matrix.scaleM(VertMtx,0,x,y,z);
    }



    private void rotateY(float d){
        AY=d;
        Matrix.rotateM(VertMtx,0,d,0,1,0);

    }

    private void rotateX(float d){
        AX=d;
        Matrix.rotateM(VertMtx,0,d,1,0,0);
    }

    private void rotateZ(float d){
        AZ=d;
        Matrix.rotateM(VertMtx,0,d,0,0,1);
    }



    private void reset(){
        Matrix.setIdentityM(VertMtx,0);
    }


}
