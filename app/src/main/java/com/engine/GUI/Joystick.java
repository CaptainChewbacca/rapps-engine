package com.engine.GUI;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.GLES20;

import com.engine.Graphics.Drawing;
import com.engine.Graphics.Sprite;
import com.engine.Graphics.TextureManager;
import com.engine.Object2D;

public class Joystick extends Object2D implements TouchGUI{

    private FloatBuffer mTriangleVertices;
    
    int Texture;
    int i=50;
    Sprite Back;
    
    float X,X1 = 0;
    float Y,Y1 = 0;


    
    float Size = 0;
    float SizeX = 0;

    
    private boolean inTouch = false;
    
    
    
    public int IDp = -2;

    public float Xf, Yf = 0;

	float JPX, JPY;

    
	public Joystick(String tex, float x, float y, float s)
	{
		
        Texture = TextureManager.Get(tex, 0);

        Size = s;
        SizeX = Size/TextureManager.GetHeight(Texture);
     
        float quadv[] =
            { 	-TextureManager.GetWidth(Texture)*SizeX*0.7f,  Size*0.7f,
              	-TextureManager.GetWidth(Texture)*SizeX*0.7f, -Size*0.7f,
				TextureManager.GetWidth(Texture)*SizeX*0.7f, -Size*0.7f,
				TextureManager.GetWidth(Texture)*SizeX*0.7f,  Size*0.7f};
        
        
        X = x; Y = y;  

        X1 = TextureManager.GetWidth(Texture)*SizeX/2; Y1=Size/2;
        
        SetPosition(X,Y);

        
        mTriangleVertices = ByteBuffer.allocateDirect(quadv.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTriangleVertices.put(quadv).position(0);
        
        Back = new Sprite("joy");
        Back.SetSize(Size*4,Size*4);
        Back.OffsetZ = 0;
        Back.SetPosition(X,Y);
	}

	
	@Override
	public void Draw(Drawing dr)
	{
		Back.Draw(dr);

		UpdateP(0, 0);

		TextureManager.Use(Texture, 0);
        mTriangleVertices.position(0);
        GLES20.glVertexAttribPointer(dr.IDvertex, 2, GLES20.GL_FLOAT, false, 8, mTriangleVertices);
        GLES20.glEnableVertexAttribArray(dr.IDvertex);     
        GLES20.glUniformMatrix4fv(dr.IDmodelview, 1, false, GetFinalMatrix(), 0);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 4);  
    }
	
	
    public void UpdateP(float ax, float ay){
    	if(!inTouch){
    	   SetPosition(X+ax*Size*2, Y+ay*Size*2);
    	}
    	else
    	{
        	SetPosition(X+Xf*Size*2, Y+Yf*Size*2);
    	}
    }
	
	
	
    boolean FirstTouch = true;
    
    @Override
    public boolean InTouch(){
    	return inTouch;
    }
    
    @Override
	public void TouchDown(float x, float y, int z){	
		float Xg = x-X;
		float Yg = y-Y;
		
		
        float len = (float)Math.sqrt(Xg*Xg+Yg*Yg);
		if(len<Size*2+Size/2){
			inTouch = true;
			if(IDp==-2){IDp = z;}
		}
		

		
		if(inTouch & IDp == z){
			if(len>Size*2){
				 Xf = (Xg/len);
				 Yf = (Yg/len);
			}
			else if(len<Size/5){
				 Xf = (Xg/(len*10f));
				 Yf = (Yg/(len*10f));
			}
			else{
				 Xf = (Xg/(Size*2));
				 Yf = (Yg/(Size*2));
			}
		}
		
		
	}
    @Override
	public void TouchDrag(float x, float y, int z){	
		float Xg = x-X;
		float Yg = y-Y;
		
        float len = (float)Math.sqrt(Xg*Xg+Yg*Yg);
		if(len<Size*2+Size/2){
			inTouch = true;
			if(IDp==-2){IDp = z;}
		}
		
		if(inTouch & IDp == z){
			if(len>Size*2){
				 Xf = (Xg/len);
				 Yf = (Yg/len);
			}
			else if(len<Size/5){
				 Xf = (Xg/(len*10f));
				 Yf = (Yg/(len*10f));
			}
			else{
				 Xf = (Xg/(Size*2));
				 Yf = (Yg/(Size*2));
			}
		}
	}
	
    @Override
	public void TouchUp(int i){
		if(IDp == i || i==-1){
			inTouch = false;
			IDp = -2;
			//setPosition(X,Y);
			FirstTouch = true;
		    Xf = 0;
		    Yf = 0;
		}
	}


	public static FloatBuffer newFloatBuffer(float[] verticesData) {
		FloatBuffer floatBuffer;
		floatBuffer = ByteBuffer.allocateDirect(verticesData.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		floatBuffer.put(verticesData).position(0);
		return floatBuffer;
	}
}
