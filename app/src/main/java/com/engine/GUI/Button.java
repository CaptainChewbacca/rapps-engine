package com.engine.GUI;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.GLES20;

import com.engine.GLRender;
import com.engine.Graphics.Drawing;
import com.engine.Graphics.TextureManager;
import com.engine.Object2D;

public class Button extends Object2D implements TouchGUI {

	private FloatBuffer mTriangleVertices;

	int Texture;


	public float X = 0;
	public float Y = 0;
	public float Xf = 0;
	public float Yf = 0;
	public float OffsetZ=0f;

	public float SizeY = 0;
	float SizeX = 0;


	private boolean inTouch = false;
	public boolean VISIBLE = true;



	int IDp = -2;
	int ID1 = -2;


	TouchListener Touch;
	int Orient=1;


	public Button(String tex, float x, float y, float s, int orintation)
	{
		Texture = TextureManager.Get(tex, 0);

		SizeY = s;
		SizeX = TextureManager.GetWidth(Texture)*SizeY/TextureManager.GetHeight(Texture);

		float quadv[] =
				{ 	-SizeX,  SizeY,
					-SizeX, -SizeY,
					SizeX, -SizeY,
					SizeX,  SizeY };


		X = x; Y = y;
		Xf = x; Yf = y;
		Orient=orintation;

		SetOrientation(Orient);


		SetPosition(Xf,Yf);


		mTriangleVertices = ByteBuffer.allocateDirect(quadv.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		mTriangleVertices.put(quadv).position(0);
	}

	@Override
	public void Draw(Drawing dr)
	{
		if(VISIBLE){
			TextureManager.Use(Texture, 0);
			mTriangleVertices.position(0);
			GLES20.glVertexAttribPointer(dr.IDvertex, 2, GLES20.GL_FLOAT, false, 8, mTriangleVertices);
			GLES20.glEnableVertexAttribArray(dr.IDvertex);
			GLES20.glUniformMatrix4fv(dr.IDmodelview, 1, false, GetFinalMatrix(), 0);
			GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 4);
		}
	}

	public void SetButtonPosition(float x, float y){
		X = x; Y = y;
		Xf = x; Yf = y;
		SetOrientation(Orient);
		SetPosition(Xf,Yf);
	}


	public void SetTouchListener(TouchListener ts){
		Touch = ts;
	}


	public void SetOrientation(int i){
		Xf+=(-2*(i/3)+1)*SizeX;
		Yf+=(-2*((i%4)/2)+1)*SizeY;

		/* Orient == 1 -> Xf+=SizeX Yf+=SizeY;
		   Orient == 2 -> Xf+=SizeX Yf-=SizeY;
		   Orient == 3 -> Xf-=SizeX Yf-=SizeY;
		   Orient == 4 -> Xf-=SizeX Yf+=SizeY;
		 */
	}



	boolean FirstTouch = true;

	@Override
	public boolean InTouch(){
		return inTouch;
	}

	@Override
	public void TouchDown(float x, float y, int z){
		if(VISIBLE){
			if(x<Xf+SizeX && x>Xf-SizeX && y<Yf+SizeY && y>Yf-SizeY){
				if(FirstTouch){
					if(IDp==-2) IDp = GLRender.App.multiT[z].ID;

					if(IDp == GLRender.App.multiT[z].ID){
						Touch.TouchDown(x, y);
						inTouch = true;
					}
				}
				FirstTouch = false;
			}
			else
			{
				if(IDp == GLRender.App.multiT[z].ID){
					Touch.TouchUp(z);
					inTouch = false;
				}
			}
		}

	}

	@Override
	public void TouchDrag(float x, float y, int z){
		if(VISIBLE){
			if(IDp == GLRender.App.multiT[z].ID){
				if(x<Xf+SizeX && x>Xf-SizeX && y<Yf+SizeY && y>Yf-SizeY){
					Touch.TouchDrag(x, y);
					inTouch = true;
				}
				else
				{
					Touch.TouchUp(z);
					inTouch = false;
				}
			}
		}
	}

	@Override
	public void TouchUp(int i){
		if(VISIBLE){
			if(IDp == i || i==-1){
				Touch.TouchUp(i);
				inTouch = false;
				IDp = -2;
				ID1 = -2;
				FirstTouch = true;
			}
		}
	}

}
