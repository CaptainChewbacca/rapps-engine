package com.engine.GUI;

import android.opengl.GLES20;

import com.engine.Functions;
import com.engine.Graphics.Drawing;
import com.engine.Graphics.TextureManager;
import com.engine.Object2D;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;



class Glyph{
    public int id, x, y, width, height, xoffset, yoffset, xadvance;
    public float Ratio, XADV, XOFF, YOFF, SIZE;

    float[] VertexTextureCoord;

    public Glyph(int i, int x, int y, int w, int h, int xo, int yo, int xa, int b, int wt, int ht){
        id=i;this.x=x;this.y=y;width=w;height=h;xoffset=xo;yoffset=yo;xadvance=xa;


        SIZE = h/((float)b);
        Ratio = w/((float)h);
        XADV = xa/((float)b);
        XOFF = xo/((float)b);
        YOFF = yo/((float)b);

        float X = x/(float)wt;
        float Y = y/(float)ht;
        float W = w/(float)wt;
        float H = h/(float)ht;

        VertexTextureCoord = new float[16];
        //Текстурные координаты (не меняются):
        VertexTextureCoord[2] = X;
        VertexTextureCoord[3] = Y;
        VertexTextureCoord[6] = X;
        VertexTextureCoord[7] = Y+H;
        VertexTextureCoord[10] = X+W;
        VertexTextureCoord[11] = Y+H;
        VertexTextureCoord[14] = X+W;
        VertexTextureCoord[15] = Y;
    }

    public void SetPositon(float x, float y){
        VertexTextureCoord[0] = x;
        VertexTextureCoord[1] = y;
        VertexTextureCoord[4] = x;
        VertexTextureCoord[5] = -1*SIZE + y;
        VertexTextureCoord[8] = Ratio*SIZE + x;
        VertexTextureCoord[9] = -1*SIZE + y;
        VertexTextureCoord[12] = Ratio*SIZE + x;
        VertexTextureCoord[13] = y;
    }

}



public class Text extends Object2D{

    private Glyph[] GLYPH;

    private FloatBuffer VertexTextureCoord;
    private ShortBuffer IndexBuffer;

    private int Texture;
    private int Width,Height;


    private int bufferoffset;

    int StrH = 1;
    int StrL = 20;

    private String text;

    public Text(String p, String t){
        text="";
        SetFNT(p, t);
    }


    public void SetFNT(String p, String t){
        Texture = TextureManager.Get(t, 0);
        //TextureManager.SetFilter(3, Texture);

        Width = TextureManager.GetWidth(Texture);
        Height = TextureManager.GetHeight(Texture);

        String FNT = Functions.TextFileLoad(p);
        int base = Integer.valueOf(FNT.substring(FNT.indexOf("base=")+5, FNT.indexOf("scaleW=")-1));
        int start = FNT.indexOf("chars count=")+12;
        int count = Integer.valueOf(FNT.substring(start, FNT.indexOf("\n", start)-1));
        GLYPH = new Glyph[count];

        for(int i = 0; i<count; i++) {
            String S = FNT.substring(FNT.indexOf("char ", start) + 1, FNT.indexOf("chnl=", FNT.indexOf("char ", start) + 1));
            int id = Integer.parseInt(S.substring(S.indexOf("id=") + 3, S.indexOf(" ", S.indexOf("id=") + 3)));
            int x = Integer.parseInt(S.substring(S.indexOf("x=") + 2, S.indexOf(" ", S.indexOf("x=") + 2)));
            int y = Integer.parseInt(S.substring(S.indexOf("y=") + 2, S.indexOf(" ", S.indexOf("y=") + 2)));
            int width = Integer.parseInt(S.substring(S.indexOf("width=") + 6, S.indexOf(" ", S.indexOf("width=") + 6)));
            int height = Integer.parseInt(S.substring(S.indexOf("height=") + 7, S.indexOf(" ", S.indexOf("height=") + 7)));
            int xoffset = Integer.parseInt(S.substring(S.indexOf("xoffset=") + 8, S.indexOf(" ", S.indexOf("xoffset=") + 8)));
            int yoffset = Integer.parseInt(S.substring(S.indexOf("yoffset=") + 8, S.indexOf(" ", S.indexOf("yoffset=") + 8)));
            int xadvance = Integer.parseInt(S.substring(S.indexOf("xadvance=") + 9, S.indexOf(" ", S.indexOf("xadvance=") + 9)));

            GLYPH[i] = new Glyph(id,x,y,width,height,xoffset,yoffset,xadvance, base, Width, Height);
            start = FNT.indexOf("chnl=", FNT.indexOf("char ", start) + 1);
        }
    }


    public void SetText(String t){
        if(t.compareTo(text)!=0) {
            text = t;
            char[] ct = t.toCharArray();

            float X = 0, Y = 0;
            UpdateBuffer(ct.length);
            for (int i = 0; i < ct.length; i++) {
                int c = -1;
                for (int z = 0; z < GLYPH.length; z++) {
                    if ((int) ct[i] == GLYPH[z].id) c = z;
                }
                if (c != -1) {
                    if (i > 0 && (ct[i - 1] == '\n' || (X + GLYPH[c].XADV > StrL && ct[i - 1] == 32 && StrL > 0))) {
                        X = 0; Y -= StrH;
                    }

                    GLYPH[c].SetPositon(X + GLYPH[c].XOFF, Y - GLYPH[c].YOFF);
                    AddToBuffer(GLYPH[c].VertexTextureCoord);

                    X += GLYPH[c].XADV;
                }
            }
        }
    }
    public String GetText(){ return text; }

    public void Draw(Drawing dr){
        if(text.length()>0) {
            TextureManager.Use(Texture, 0);

            GLES20.glUniformMatrix4fv(dr.IDmodelview, 1, false, GetFinalMatrix(), 0);

            VertexTextureCoord.position(0);
            GLES20.glVertexAttribPointer(dr.IDvertex, 2, GLES20.GL_FLOAT, false, 16, VertexTextureCoord);
            GLES20.glEnableVertexAttribArray(dr.IDvertex);
            VertexTextureCoord.position(2);
            GLES20.glVertexAttribPointer(dr.IDtexcoord, 2, GLES20.GL_FLOAT, false, 16, VertexTextureCoord);
            GLES20.glEnableVertexAttribArray(dr.IDtexcoord);

            IndexBuffer.position(0);

            GLES20.glDrawElements(GLES20.GL_TRIANGLES, IndexBuffer.capacity(), GLES20.GL_UNSIGNED_SHORT, IndexBuffer);
        }
    }

    public void UpdateBuffer(int n){
        bufferoffset = 0;
        VertexTextureCoord = ByteBuffer.allocateDirect(n * 16 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        VertexTextureCoord.position(0);

        IndexBuffer = ByteBuffer.allocateDirect(n * 6 * 4).order(ByteOrder.nativeOrder()).asShortBuffer();
        IndexBuffer.position(0);
    }

    public void AddToBuffer(float[] vt){
        VertexTextureCoord.position(bufferoffset*16);
        VertexTextureCoord.put(vt);

        int oset = bufferoffset*4;
        short[] Index = {(short)(0+oset), (short)(1+oset),
                (short)(2+oset), (short)(0+oset),
                (short)(2+oset), (short)(3+oset)}; //indexes: {0, 1, 2, 0, 2, 3};

        IndexBuffer.position(bufferoffset*6);
        IndexBuffer.put(Index);

        bufferoffset++;
    }
}
