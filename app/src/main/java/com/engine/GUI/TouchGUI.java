package com.engine.GUI;

import com.engine.Graphics.Drawing;

public interface TouchGUI {
	public boolean InTouch();
	public void TouchDown(float x, float y, int z);
	public void TouchDrag(float x, float y, int z);
	public void TouchUp(int i);
	public void Draw(Drawing dr);
}
