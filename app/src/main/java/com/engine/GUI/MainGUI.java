package com.engine.GUI;

import java.util.ArrayList;

import com.engine.Graphics.Drawing;


public class MainGUI {
	   ArrayList<TouchGUI> List;
	   
	   public MainGUI(){
		   List = new ArrayList<TouchGUI>();
	   }
	   
	   
	   public void Draw(Drawing sp){
		   for(int i = 0; i<List.size(); i++){
			   List.get(i).Draw(sp);
		   }
	   }
	   
	   
	   public boolean TouchDown(float x, float y, int z){
		   boolean t = false;
		   for(int i = 0; i<List.size(); i++){
			     List.get(i).TouchDown(x, y, z);
			     if(List.get(i).InTouch())
			        t = true;
		   }  
		   return t;
	   }
	   
	   
	   
	   public void TouchDrag(float x, float y, int z){
		   for(int i = 0; i<List.size(); i++){
			     List.get(i).TouchDrag(x, y, z);
		   }  
	   }
	   
	   public void TouchUp(int g){
		   for(int i = 0; i<List.size(); i++){
			   List.get(i).TouchUp(g);
		   } 
	   }
	   
	   
	   public void Add(TouchGUI s){
		   List.add(s);
	   }
		
	   public void Remove(TouchGUI s){
		   List.remove(s);
	   }
}
