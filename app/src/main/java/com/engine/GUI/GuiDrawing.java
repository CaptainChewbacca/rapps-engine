//V 1.12

package com.engine.GUI;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import com.engine.Graphics.Drawing;
import com.engine.Graphics.Shader;

import android.opengl.GLES20;
import android.opengl.Matrix;


public class GuiDrawing extends Drawing {

    public float[] modelMatrix;
    public float[] viewMatrix;
    public float[] modelViewMatrix;
    public float[] projectionMatrix;
    
    
	private FloatBuffer mTriangleVertices;
    
	public GuiDrawing(int w, int h)
	{
		
        String vertexShaderCode=
        "uniform mat4 u_MVP;"+
        "uniform mat4 u_modelView;"+ 
        "attribute vec3 a_vertex;"+
        "attribute vec2 a_texcoord;"+
        
        "varying vec3 v_vertex;"+
        "varying vec2 v_texcoord0;"+

        "void main() {"+
             "v_vertex=a_vertex;"+

             "v_texcoord0=a_texcoord;"+
             //"v_texcoord0.t=a_texcoord.z;"+
             "gl_Position = u_MVP * u_modelView * vec4(a_vertex,1.0);"+
        "}"; 

        String fragmentShaderCode=
        "precision mediump float;"+

        "uniform vec4 u_color;"+  
        "uniform sampler2D u_texture0;"+
        "varying vec2 v_texcoord0;"+
        
        "void main() {"+

             "lowp vec4 textureColor0=texture2D(u_texture0, v_texcoord0);"+
             
             "gl_FragColor = textureColor0*u_color;"+
        "}"; 
        
		
		
		Shad=new Shader(vertexShaderCode, fragmentShaderCode);
		
        IDvertex = GLES20.glGetAttribLocation(Shad.Program, "a_vertex");
        IDmodelview = GLES20.glGetUniformLocation(Shad.Program, "u_modelView");
        IDtexcoord = GLES20.glGetAttribLocation(Shad.Program, "a_texcoord");
        IDcolor = GLES20.glGetUniformLocation(Shad.Program, "u_color");
        
        
        
        modelMatrix=new float[16];
        viewMatrix=new float[16];
        modelViewMatrix=new float[16];
        projectionMatrix=new float[16];
        Mat=new float[16];
        
        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.setLookAtM(viewMatrix, 0, 
        		0, 0, 1, 
        		0, 0, 0, 
        		0, 1, 0);
        Matrix.multiplyMM(modelViewMatrix, 0, viewMatrix, 0, modelMatrix, 0);
        Matrix.orthoM(projectionMatrix, 0, -w/2, w/2, -h/2, h/2, 0.1f, 10.0f);
        Matrix.multiplyMM(Mat, 0, projectionMatrix, 0, modelViewMatrix, 0);
        
        
	    float quadv[] =
	           { 0, 0,
	             0, 1,
	             1, 1,
	             1, 0};	
			
	    mTriangleVertices = ByteBuffer.allocateDirect(quadv.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
	    mTriangleVertices.put(quadv).position(0);
	}

	
	public void BeginGui(){
		   mTriangleVertices.position(0);
		   GLES20.glVertexAttribPointer(IDtexcoord, 2, GLES20.GL_FLOAT, false, 8, mTriangleVertices);
		   GLES20.glEnableVertexAttribArray(IDtexcoord);
		   
		   SetColor(1, 1, 1, 1);  
	}
	
	
	public void SetColor(float r, float g, float b, float a){
		GLES20.glUniform4f(IDcolor, r,g,b,a);
	}
	

}
