package com.engine.GUI;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.GLES20;

import com.engine.Graphics.Drawing;
import com.engine.Graphics.Sprite;
import com.engine.Graphics.TextureManager;
import com.engine.Object2D;

public class Slider extends Object2D implements TouchGUI{


    private FloatBuffer mTriangleVertices;
    
    int Texture;
    Sprite Back;
    int i=50;
    
    
    float X0,X1 = 0;
    float Y0,Y1 = 0;


    
    float Size = 0;
	float Xext;
    float SizeX = 0;
    
    
    public int Value = 0;

    private boolean inTouch = false;
    
    public int IDp = -2;

    public float Xf, Yf = 0;
    

    float W, H;
    
    
    
	public Slider(float x, float y, float s, float xe, int v, float w, float h)
	{
		
		W=w; H=h;

		Texture = TextureManager.Get("mslu", 0);

        Size = s;
		Xext = xe;
        SizeX = Size/TextureManager.GetHeight(Texture);
     
        float quadv[] =
            {  -TextureManager.GetWidth(Texture)*SizeX/2,  Size/2,
               -TextureManager.GetWidth(Texture)*SizeX/2, -Size/2,
				TextureManager.GetWidth(Texture)*SizeX/2, -Size/2,
				TextureManager.GetWidth(Texture)*SizeX/2,  Size/2};
        
        
        X0 = x; Y0 = y;

        X1 = TextureManager.GetWidth(Texture)*SizeX/2; Y1=Size/2;
        
        SetPosition(X0,Y0);

        
        mTriangleVertices = ByteBuffer.allocateDirect(quadv.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTriangleVertices.put(quadv).position(0);
        
        Back = new Sprite("mslb");
        Back.SetSize(Size*Xext,Size);
        Back.OffsetZ = 0;
        Back.SetPosition(X0,Y0);
        
        
        SetValue(v);

	}
	
	
	private void SetValue(int v){
    	Value = v;
    	SetPosition(X0+((Size*Xext)/100)*(Value-50), Y0);
    }
	
	private void Up(){
		Value = (int)(((X-X0)/(Size*Xext))*100)+50;
	}
	
	public float GetResultValue(){
		return (float)Value/70+0.5f;
	}
	
	
	@Override
	public void Draw(Drawing dr)
	{	
	    Back.Draw(dr);

		TextureManager.Use(Texture,0);
        mTriangleVertices.position(0);
        GLES20.glVertexAttribPointer(dr.IDvertex, 2, GLES20.GL_FLOAT, false, 8, mTriangleVertices);
        GLES20.glEnableVertexAttribArray(dr.IDvertex);
        GLES20.glUniformMatrix4fv(dr.IDmodelview, 1, false, GetFinalMatrix(), 0);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 4);  
    }
	
	
    boolean FirstTouch = true;

    @Override
    public boolean InTouch(){
    	return inTouch;
    }
    
    
    @Override
	public void TouchDown(float x, float y, int z){	
		
		if(Math.abs(x-X0)*2<Size*Xext && Math.abs(y-Y0)<Size/2){
			inTouch = true;
			if(IDp==-2){IDp = z;}
		}
		
		
		if(inTouch & IDp == z){
			if(Math.abs(x-X0)*2>Size*Xext){
				 SetPosition(X0+((x-X0)>0?Size*Xext*0.5f:-Size*Xext*0.5f), Y0);
			}
			else if(Math.abs(x-X0)*2<Size*Xext){
				 SetPosition(x, Y0);


			}else{
				 SetPosition(x, Y0);

			}
			
			Up();
		}
		
		
	}
    @Override
	public void TouchDrag(float x, float y, int z){	

		if(Math.abs(x-X0)*2<Size*Xext && Math.abs(y-Y0)<Size/2){
			inTouch = true;
			if(IDp==-2){IDp = z;}
		}
		
		
		if(inTouch & IDp == z){
			if(Math.abs(x-X0)*2>Size*Xext){
				 SetPosition(X0+((x-X0)>0?Size*Xext*0.5f:-Size*Xext*0.5f), Y0);
			}
			else if(Math.abs(x-X0)*2<Size*Xext){
				 SetPosition(x, Y0);


			}else{
				 SetPosition(x, Y0);

			}
			
			Up();
		}
	}
	
    @Override
	public void TouchUp(int i){
		if(IDp == i || i==-1){
			inTouch = false;
			IDp = -2;
			//setPosition(X,Y);
			FirstTouch = true;
		    Xf = 0;
		    Yf = 0;
		    Up();
		}
	}

}
